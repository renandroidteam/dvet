﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Configuration;

namespace DVETAdministrationWebAPI.Utilities
{
    public class ShortMessageServiceClient
    {
        string _smsApiUri = ConfigurationManager.AppSettings["SMS-API-URI"];

        public string SendSingleSms(string mobileNo, string message)

        {

            var username = ConfigurationManager.AppSettings["SMS-Username"];
            var password = ConfigurationManager.AppSettings["SMS-Password"];
            var senderId = ConfigurationManager.AppSettings["SMS-SenderID"];
            var secureKey = ConfigurationManager.AppSettings["SMS-SecureKey"];
            string _smsApiUri = ConfigurationManager.AppSettings["SMS-API-URI"];

            message = message.Replace('\n', ' ').Replace('\r', ' ').Replace("  ", " ");
            message = message.Trim(' ');

            //Latest Generated Secure Key
            Stream dataStream;
            HttpWebRequest request =
                (HttpWebRequest)WebRequest.Create(_smsApiUri);
            request.ProtocolVersion = HttpVersion.Version10;
            request.KeepAlive = false;
            request.ServicePoint.ConnectionLimit = 1;

            //((HttpWebRequest)request).UserAgent = ".NET Framework Example Client";
            ((HttpWebRequest)request).UserAgent = "Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)";

            request.Method = "POST";

            string encryptedPassword = EncryptedPasswod(password);
            string newsecureKey = HashGenerator(username, senderId, message, secureKey);
            string smsservicetype = "singlemsg"; //For single message.
            string query =
                $"username={HttpUtility.UrlEncode(username)}&password={HttpUtility.UrlEncode(encryptedPassword)}&smsservicetype={HttpUtility.UrlEncode(smsservicetype)}&content={HttpUtility.UrlEncode(message)}&mobileno={HttpUtility.UrlEncode(mobileNo)}&senderid={HttpUtility.UrlEncode(senderId)}&key={HttpUtility.UrlEncode(newsecureKey)}";



            byte[] byteArray = Encoding.ASCII.GetBytes(query);

            request.ContentType = "application/x-www-form-urlencoded";

            request.ContentLength = byteArray.Length;



            dataStream = request.GetRequestStream();

            dataStream.Write(byteArray, 0, byteArray.Length);

            dataStream.Close();

            WebResponse response = request.GetResponse();

            string status = ((HttpWebResponse)response).StatusDescription;

            dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            reader.Close();

            dataStream.Close();

            response.Close();
            return responseFromServer;

        }


        protected string EncryptedPasswod(string password)
        {

            byte[] encPwd = Encoding.UTF8.GetBytes(password);
            //static byte[] pwd = new byte[encPwd.Length];
            HashAlgorithm sha1 = HashAlgorithm.Create("SHA1");
            byte[] pp = sha1.ComputeHash(encPwd);
            // static string result = System.Text.Encoding.UTF8.GetString(pp);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in pp)
            {

                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();

        }

        /// <summary>
        /// Method to Generate hash code 
        /// </summary>
        /// <param name="secureKey">your last generated Secure_key

        protected string HashGenerator(string username, string senderId, string message, string secureKey)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(username).Append(senderId).Append(message).Append(secureKey);
            byte[] genkey = Encoding.UTF8.GetBytes(sb.ToString());
            //static byte[] pwd = new byte[encPwd.Length];
            HashAlgorithm sha1 = HashAlgorithm.Create("SHA512");
            byte[] secKey = sha1.ComputeHash(genkey);

            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < secKey.Length; i++)
            {
                sb1.Append(secKey[i].ToString("x2"));
            }
            return sb1.ToString();
        }
    }
}