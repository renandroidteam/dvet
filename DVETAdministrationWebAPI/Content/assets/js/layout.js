﻿$(document).ready(function () {
    //Counter
    $('.counter').each(function () {
        var $this = $(this),
            countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        },

        {

            duration: 5000,
            easing: 'linear',
            step: function () {
                $this.text(Math.floor(this.countNum));
            },
            complete: function () {
                $this.text(this.countNum);
                //alert('finished');
            }

        });
    });
    /* Tooltip */
    $('[data-toggle="tooltip"]').tooltip();

    /* Menu */

    $('.mk-sb-has-submenu').on('click', function () {
        $('.mk-sb-has-submenu').toggleClass('active');
    });

    //$('.mk-sbmenu-content>li>a').on('click', function () {
    //    $('ul.sub-menu').removeClass('show');
    //});

    //$('[data-toggle="collapse"]').click(function () {
    //    $('.collapse.in').collapse('hide')
    //});

    $('.mk-menu-toggle').on('click', function () {
        $('body').toggleClass('mk-sb-minimized');
        $('#sb_style').toggleClass('scrollbar');
    });

    /* Mobile view menu toogle */
    $('.mk-menu-toggle-mobile').on('click', function () {
        $('.mk-aside-left').css("left", "0px");
    });
    $('.mk-menu-close-mobile').on('click', function () {
        $('.mk-aside-left').css("left", "-255px");
    });
    //Data Table section
    $('#dvetDataTable').DataTable();

    //reject section
    $('#comment-section').hide();
    $('#reject').click(function () {
        $('#comment-section').toggle('slow');

    });
    $('.alert-target').click(function () {
        $.notify({
            // options
            message: 'Submitted Successfully',

        }, {
            // settings
            type: 'success',
            placement: {
                from: "bottom",
                align: "right",
                z_index: 9999999999999999999,
            }
        });
    });
    
    /******Trade Info*****/
    $('#wps, #wcc, #wtfo, #wtto').hide();
    $('#wpsvalue').change(function () {
        var val_hf = $('#wpsvalue').val();
        if (val_hf == 1) { $('#wps').show('slow'); } else { $('#wps').hide('slow'); }
    });
    $('#wccvalue').change(function () {
        var val_hf = $('#wccvalue').val();
        if (val_hf == 1) { $('#wcc').show('slow'); } else { $('#wcc').hide('slow'); }
    });
    $('#wtfovalue').change(function () {
        var val_hf = $('#wtfovalue').val();
        if (val_hf == 1) { $('#wtfo').show('slow'); } else { $('#wtfo').hide('slow'); }
    });
    $('#wttovalue').change(function () {
        var val_hf = $('#wttovalue').val();
        if (val_hf == 1) { $('#wtto').show('slow'); } else { $('#wtto').hide('slow'); }
    });

    /* Buliding Infrastructure */
    $('.owned-content, .private-rented-content').hide();

    $('.ownded').change(function () {
        var val_owned = $('.ownded').val();
        if (val_owned == 1) { $('.owned-content').show(); }
        else { $('.owned-content').hide(); }
    });

    $('.private-rented').change(function () {
        var val_private_rented = $('.private-rented').val();
        if (val_private_rented == 1) { $('.private-rented-content').show(); }
        else { $('.private-rented-content').hide(); }
    });

    /* Hostel Details */
    $('.hostel_facillity_content, .staff_qua_facility_content, .any_oth_occupants_content').hide();
    $('.hostel_facillity').change(function () {
        //alert($('.extra_curricular').val());
        var val_hfl = $('.hostel_facillity').val();
        if (val_hfl == 1) { $('.hostel_facillity_content').show(); }
        else { $('.hostel_facillity_content').hide(); }
    });
    $('.hostel_availability').change(function () {
        var val_ha = $('.hostel_availability').val();
        if (val_ha == 1) { $('#h_girls').show(); $('#h_boys').hide(); }
        if (val_ha == 2) { $('#h_boys').show(); $('#h_girls').hide(); }
        if (val_ha == 3) { $('#h_boys').show(); $('#h_girls').show(); }

    });
    $('.staff_qua_facility').change(function () {
        var val_staff_qua = $('.staff_qua_facility').val();
        if (val_staff_qua == 1) { $('.staff_qua_facility_content').show(); }
        else { $('.staff_qua_facility_content').hide(); }
    });
    $('.any_oth_occupants').change(function () {
        var val_other_occupants = $('.any_oth_occupants').val();
        if (val_other_occupants == 1) { $('.any_oth_occupants_content').show(); }
        else { $('.any_oth_occupants_content').hide(); }
    });


    $('.extra_curricular_content, .student_council_content').hide();
    $('.extra_curricular').change(function () {
        var val_hf = $('#ec').val();
        if (val_hf == 1) { $('.extra_curricular_content').show(); }
        else { $('.extra_curricular_content').hide(); }
    });
    $('.student_council').change(function () {
        var val_hf = $('#sc').val();
        if (val_hf == 1) { $('.student_council_content').show(); }
        else { $('.student_council_content').hide(); }
    });

    $.notify({
        // options
        icon: 'glyphicon glyphicon-warning-sign',
        title: 'Bootstrap notify',
        message: 'Turning standard Bootstrap alerts into "notify" like notifications',
        url: 'https://github.com/mouse0270/bootstrap-notify',
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "info",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
});

  
    
    