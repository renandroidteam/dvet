﻿/// <reference path="../../pages/exampaperrecord.html" />

function check(form) {

    if (form.username.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.password.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }
    var usrnm = form.username.value;
    var psw = form.password.value;
    var user = form.user.value;
    if (usrnm == "DvetAdmin" && psw == "DvetAdmin") {
        localStorage.setItem("username", usrnm);
        if (user == 1) { window.location.href = 'homepage.html'; }
        else if (user == 2) { window.location.href = 'homepage-principal.html'; }
        else if (user == 3) { window.location.href = 'homepage-DVEO.html'; }
        else if (user == 4) { window.location.href = 'homepage-RO.html'; }
        else if (user == 5) { window.location.href = 'homepage-HO.html'; }
        else {
            alert("Please select User Type");
        }
    }
    else {
        alert("Login failed - Please enter correct Username and Password")
    }
    
}
//$('document').ready(function () {
//    $('.libraryone').hide();

//});

function checklibrary(form) {
    if (form.lusername.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.lpassword.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }


    if (form.lusername.value == "DvetAdmin" && form.lpassword.value == "DvetAdmin") {
        //localStorage.setItem("username", usrnm);
        $('#loginlibrary').modal('hide');
        $('#librarylogin').hide();
        //$('.libraryone').show();
        window.location.href = 'addbook.html';
        return false;
    }

    else {
        alert("Login failed - Please enter correct Username and Password")
    }
}
function checkapprentice(form) {
    if (form.lusername.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.lpassword.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }

    if (form.lusername.value == "DvetAdmin" && form.lpassword.value == "DvetAdmin") {
        window.location.href = 'registration.html';
        return false;
    }
    else {
        alert("Login failed - Please enter correct Username and Password")
    }
}
function checkattendance(form) {
    if (form.lusername.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.lpassword.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }

    if (form.lusername.value == "DvetAdmin" && form.lpassword.value == "DvetAdmin") {
        window.location.href = 'attendance.html';
        return false;
    }
    else {
        alert("Login failed - Please enter correct Username and Password")
    }
}
function checkevaluationanswerpaperrecord(form) {
    if (form.lusername.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.lpassword.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }

    if (form.lusername.value == "DvetAdmin" && form.lpassword.value == "DvetAdmin") {
        window.location.href = 'evaluationanswerpaperrecord.html';
        return false;
    }
    else {
        alert("Login failed - Please enter correct Username and Password")
    }
}
function checkexampaperrecord(form) {
    if (form.lusername.value == "") {
        alert("Please enter a Username");
        $('#username').focus();
        return false;
    }
    if (form.lpassword.value == "") {
        alert("Please enter a Password");
        $('#password').focus();
        return false;
    }

    if (form.lusername.value == "DvetAdmin" && form.lpassword.value == "DvetAdmin") {
        window.location.href = 'exampaperrecord.html';
        return false;
    }
    else {
        alert("Login failed - Please enter correct Username and Password")
    }
}
//var credentials = ["DvetAdmin", "Librarian"];
//localStorage.setItem("username", JSON.stringify(credentials));
//var retrievedData = localStorage.getItem("username");
//var user_credentials = JSON.parse(retrievedData);
var locstr = localStorage.getItem("username");
//var locstrlibrary = localStorage.getItem("library");
function logout() {
    localStorage.clear();
    window.location.href = 'login.html';
}

