﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.Viewmodels.Admin
{
    public class InwordViewModel
    {
        public IList<Desk> listDeskdetails { get; set; }
        public IList<Employee> listEmployee { get; set; }
        public IList<Department> listDepartment { get; set; }
        public IList<LetterType> listType { get; set; }
        public IList<DeskOfficer> listOffice { get; set; }
        public IList<Status> listStatus { get; set; }
        public IList<NoteStatus> listNotes { get; set; }
        public LetterType listLetter { get; set; }
        public DeskOfficer listCentral { get; set; }
        public Desk listDesk { get; set; }
        public Department list { get; set; }
        public Employee listemp { get; set; }
        public NoteStatus note { get; set; }
        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }
}