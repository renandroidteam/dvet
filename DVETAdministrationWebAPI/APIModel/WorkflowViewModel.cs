﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.APIModel
{
    public class WorkflowViewModel
    {
        public List<WorkflowModel> lstWorkflowModel { get; set; }
        public List<WorkflowProcessModel> lstWorkflowModels { get; set; }
        public  string WorkflowStatus { get; set; }
        public string Status { get; set; }
        public string errorMsg { get; set; }

        public WorkflowProcessModel model { get; set; }


    }
}