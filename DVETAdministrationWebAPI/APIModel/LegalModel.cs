﻿using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class LegalModel
    {
       
        public Report report { get; set; }
        public IList<DashboardCaseCount> lstCaseCount { get; set; }
        public IList<CaseDetails> lstCaseDetails { get; set; }
        public IList<CaseComment> lstCaseComment { get; set; }
        public IList<CaseHistory> lstCaseHistory { get; set; }
        public IList<CaseHearing> lstCaseHearing { get; set; }
        public IList<RecentCases> lstRecentCases { get; set; }
        public IList<CaseApplicantsAndRespondents> lstApplicantsAndRespondents { get; set; }
        public CaseDetails caseDetails;
        public CaseComment caseComment { get; set; }
        public CaseHistory caseHistory { get; set; }
        public CaseParticipants caseParticipants;
        public IList<CaseParticipants> lstCaseParticipants { get; set; }
        public IList<CaseParticipantByEmail> lstCaseParticipantsByEmailId { get; set; }
        public IList<Report> lstReport { get; set; }
        public string status { get; set; }
        public string NewParticipantID { get; set; }
        public string CasePKIDOutput { get; set; }
        public string ErrorMessage { get; set; }
        public IList<AuthEmailId> lstEmail { get; set; }
    
       

       

    }  
}