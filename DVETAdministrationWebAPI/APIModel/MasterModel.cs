﻿using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class MasterModel
    {
        public IList<Master_CourtType> lstCourtType { get; set; }
        public IList<Master_AidType> lstAidType { get; set; }
        public IList<Master_CaseType> lstCaseType { get; set; }
        public IList<Master_Region> lstRegion { get; set; }
        public IList<Master_CaseClassification> lstCaseClassification { get; set; }
        public IList<Master_LegalStatus> lstLegalStatus { get; set; }
        public IList<Master_ParticipantType> lstParticipantType { get; set; }
        public IList<Master_Institute> lstInstitute { get; set; }
        public IList<Master_InstituteType> lstInstituteType { get; set; }
        public IList<Master_Department> lstDepartments { get; set; }
        public IList<Master_Employee> lstEmployees { get; set; }

        public IList<Master_Citizen_UserRoles> lstcitizenRoles { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Master_SMSModel
    {
        public string mobileNo { get; set; }
        public string msgBody { get; set; }
    }

    public class EmailContent
    {
        public string CarbonCopyAddresses { get; set; }
        public string MailToAddresses { get; set; }
        public string BroadcastCarbonCopyAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MailPriority MailPriority { get; set; }
        public List<string> Attachments { get; set; }
        public string MailFromAddress { get; set; }
    }

    public enum MailPriority
    {
        //     The email has normal priority.
        Normal = 0,

        //     The email has low priority.
        Low = 1,

        //     The email has high priority.
        High = 2
    }

}