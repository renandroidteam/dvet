﻿
using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class eOfficeModel
    {
        public IList<eOffice> lstdocumenttype { get; set; }
        public IList<eOfficeYears> lstyear { get; set; }
        public IList<eOfficeYears> lstTasks { get; set; }
        public IList<eOfficeTaskDetailsModel> lstTasksDetails { get; set; }
        public IList<GRDetails> lstGRDetails { get; set; }
        public string status { get; set; }
        public string ErrorMessage { get; set; }
        public string TaskId { get; set; }

    }
}