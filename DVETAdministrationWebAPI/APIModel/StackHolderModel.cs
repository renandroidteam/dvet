﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.APIModel
{
    public class StackHolderModel
    {

        public IList<StackHolderAddQuestions> addquestions { get; set; }
        public IList<StackHolderSurvey> addsurvey { get; set; }
        public IList<StackHolderSurvey> lstsurveydetila { get; set; }
        public IList<StackHolderFeedbackDetails> lstFeedBack { get; set; }
        public IList<StackHolderFeedBackSurvey> lstSurvey { get; set; }
        public IList<StackHoldersrespondantDetails> lstResponadant{ get; set; }
        
        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }
    

}