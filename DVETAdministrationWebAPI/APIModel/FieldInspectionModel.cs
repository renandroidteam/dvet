﻿using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
   public class FIModel
    {
        public IList<FIInspectionOrAudit> FIAddnewInspection { get; set; }
        public FIInspectionOrAudit lstInspection;
        public IList<FIInspectionOrAudit> lstAudit { get; set; }
        public IList<FIRegisterDesk> lstRegisterDesk { get; set; }
        public IList<FI_AnnexureC> lstAddAnnexureC { get; set; }
        //public FIInspectionOrAudit fiaddcase;
        public IList<FIInspectionOrAuditType> lstInspectionOrAuditTypes { get; set; }
        public IList<FI_EventType> lstEventTypes { get; set; }
        public string status { get; set; }
        public string ErrorMessage { get; set; }
        public IList<FIInspectionOrAudit> lstSearchDetails { get; set; }
        public IList<FIAuditParas> lstAuditParas { get; set; }
        public IList<FIAuditParasDetails> lstAuditParasDetails { get; set; }
        public IList<FIInspectionCommiteeData> lstInspectionCommiteeData { get; set; }
    }
}