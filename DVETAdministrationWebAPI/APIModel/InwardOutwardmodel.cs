﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.APIModel
{
    public class InwardOutwardmodel
    {
        public IList<Inwardmodel> lstentitymodels { get; set; }
        public IList<DeskOfficerResponseModel> lstentitymodel { get; set; }
        public IList<Addfiles> lstsearchViewmodel { get; set; }
        public IList<Inwardmodel> lstListofcasesmodel { get; set; }
        public IList<AddfileNotes> lstFiles { get; set; }
        public IList<AddFileDetails> lstFile { get; set; }
        public IList<Inwardmodel> lstdocuments { get; set; }
        public IList<NoteStatusDecision> lstNote { get; set; }
        public string status { get; set; }
        public string ErrorMessage { get; set; }
        public string CaseId { get; set; }
        public Inwardmodel model { get; set; }
    }
}