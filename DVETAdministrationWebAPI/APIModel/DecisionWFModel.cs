﻿
using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class DecisionWFModel
    {

        public IList<DecisionWF> lstCase { get; set; }

        public int WorkflowInstanceId { get; set; }

        public int RequestID { get; set; }

        public string Comments { get; set; }

        public string CreatedBY { get; set; }

        public string ActionCode { get; set; }

        public int ProcessID { get; set; }

        public string WFCode { get; set; }

        public int StateID { get; set; }

        public string RequestNo { get; set; }

        public string WorkflowDCode { get; set; }

        public int WFID { get; set; }

        public string AssignedTo { get; set; }

        public string StatusCode { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    
}