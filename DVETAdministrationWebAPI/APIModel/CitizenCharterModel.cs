﻿using DataModel.Master;
using System;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class CitizenCharterModel
    {

        public IList<Master_Institute> lstInstitute { get; set; }
        public IList<Master_InstituteType> lstInstituteType { get; set; }
        public IList<Master_Department> lstDepartment { get; set; }

        public IList<CoreGroupMembers> lstCoreGroupMembers { get; set; }
        public IList<TaskMemberForce> lstTaskMemberForce { get; set; }
        public IList<CitizenComment> lstCitizenComment { get; set; }
        public IList<Citizencharter> lstCitizenCharterUsers { get; set; }
        public IList<CitizenCharter> lstyear { get; set; }
        public IList<CitizenCharter_UserRole> lstUserRoles { get; set; }
        public IList<CitizenCharter> lstcc { get; set; }

      

        public Citizencharter CitizencharterUser { get; set; }
        public string CitizenUserRoles { get; set; }
        public int PKID_User { get; set; }
        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }
}