﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.APIModel
{
    public class WorkflowRequestViewModel
    {
        public int RequestId { get; set; }
        public string RequestNo { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowDCode { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string WfStatus { get; set; }

    }
}