﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVETAdministrationWebAPI.APIModel
{
    public class EmployeeModel
    {
        public IList<Master_Employee> lstEmployee { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }
}