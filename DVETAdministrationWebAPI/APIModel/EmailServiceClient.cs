﻿using System;
using System.Net.Mail;

namespace DVETAdministrationWebAPI.APIModel
{
    public class EmailServiceClient : IDisposable
    {
        private MailMessage msgMail = new MailMessage();

        public bool ComposeMail(EmailContent mo)
        {
            //HttpContext.Current.Response.Write(filePath);
            //rds.ReadXml(filePath);
            try
            {
                string[] to = mo.MailToAddresses.Split(';');
                foreach (var toAddress in to)
                {
                    msgMail.To.Add(toAddress);
                }

                if (!string.IsNullOrEmpty(mo.CarbonCopyAddresses))
                    {
                    string[] cc = mo.CarbonCopyAddresses.Split(';');
                    foreach (var ccAddress in cc)
                    {
                        msgMail.CC.Add(ccAddress);
                    }
                }
                if (!string.IsNullOrEmpty(mo.BroadcastCarbonCopyAddresses))
                {
                    string[] bcc = mo.BroadcastCarbonCopyAddresses.Split(';');
                    foreach (var bccAddress in bcc)
                    {
                        msgMail.Bcc.Add(bccAddress);
                    }
                }



            //     if (mo.MailFromAddress == null)
            //{
            //    MailAddress from = new MailAddress("dvetdonotreply@gmail.com");
            //    MailAddress receiver = new MailAddress(mo.MailToAddresses);
            //    MailMessage Mymessage = new MailMessage(from, receiver);
            //    Mymessage.Subject = mo.Subject.Trim();
            //    Mymessage.Body = mo.Body.Trim();
            //    MyServer.Send(Mymessage);
            //}
            //else
            //{
            //    MailAddress from = new MailAddress(mo.MailFromAddress);
            //    MailAddress receiver = new MailAddress(mo.MailToAddresses);
            //    MailMessage Mymessage = new MailMessage(from, receiver);
            //    Mymessage.Subject = mo.Subject.Trim();
            //    Mymessage.Body = mo.Body.Trim();
            //    MyServer.Send(Mymessage);
          //  }


                // this is now configured in mail config section
                //if (!string.IsNullOrEmpty(mo.SendersAddress) && !string.IsNullOrEmpty(mo.SendersDisplayName))
                //{
                //    msgMail.From = new MailAddress(mo.SendersAddress, mo.SendersDisplayName);
                //}

                //msgMail.Priority = mo.MailPriority;
                msgMail.Subject = mo.Subject;
                msgMail.Body = mo.Body;
                msgMail.IsBodyHtml = true;
                msgMail.BodyEncoding = System.Text.Encoding.UTF8;

                if (mo.Attachments != null)
                {
                    foreach (var attachment in mo.Attachments)
                    {
                        msgMail.Attachments.Add(new Attachment(attachment));
                    }
                }

                SmtpClient client = new SmtpClient();
                client.Send(msgMail);
                return true;
            }

            catch (Exception ex)
            {
                //return ex.Message;
                return false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            msgMail.Dispose();
        }

        #endregion IDisposable Members
    }
}