﻿using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class StudentManagementModel
    {
        public IList<StudentProfileDetails> lstStudentDetails { get; set; }
        public IList<StudentSemesterAttendance> lstSemesterAttendance { get; set; }
        public IList<StudentSemesterLeave> lstSemesterLeave { get; set; }
        public IList<StudentSemesterResult> lstSemesterResult { get; set; }
        public IList<StudentPenalty> lstSemesterPenalty { get; set; }
        public StudentProfileDetails model { get; set; }
        public StudentSemesterAttendance models { get; set; }
        public StudentSemesterLeave leaves { get; set; }
        public StudentSemesterResult result { get; set; }
        public StudentPenalty Penalty { get; set; }
        public IList<PlacementDetails> lstPlacementDetails { get; set; }
        public PlacementDetails placement { get; set; }
        public IList<ScholorshipDetails> lstScholorshipDetails { get; set; }
        public ScholorshipDetails scholor { get; set; }
        
        public string status { get; set; }
        public string ErrorMessage { get; set; }
        public string StudentId { get; set; }
    }
}