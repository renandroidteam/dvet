﻿using DataModel.Master;
using System.Collections.Generic;

namespace DVETAdministrationWebAPI.APIModel
{
    public class DecisionViewModel
    {
        public int TotalCases { get; set; }
        public int ClosedCases { get; set; }
        public int ReturnedCases { get; set; }
        public int AssignedCases { get; set; }

        public IList<DecisionCaseModel> lstDecisionCases { get; set; }

        public IList<Decision_CaseDetails> lstCaseDetails { get; set; }

        public IList<CaseOfficerDetails> lstofficer { get; set; }

        public IList<Decision_CaseFlowModel> lstCaseFlow { get; set; }

 
                
        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }
}