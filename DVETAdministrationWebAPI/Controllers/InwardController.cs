﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVETAdministrationWebAPI.Controllers
{
    public class InwardController : Controller
    {
        // GET: Inward
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Submit(Inwardmodel model)
        {
            InwardDB obj = new InwardDB();
            obj.IoAddCase(model);
            return View("Index");
        }

        public ActionResult BindDesk()
        {
            InwardDB objDb = new InwardDB();
            InwordViewModel viewModel = new InwordViewModel();
            viewModel.listDeskdetails = objDb.GetDesks().ToList();
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
    }
}