﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class eOfficeController : ApiController
    {

        [HttpGet]
        [ActionName("DocumentType")]
        public eOfficeModel GetDocumentType()
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objeOffice = new eOfficeDB();
                viewModel.lstdocumenttype= objeOffice.GetDocumentType();
                viewModel.lstyear = objeOffice.GetYear();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }

            return viewModel;
        }

        [HttpPost]
        [ActionName("AddCase")]
        public IHttpActionResult AddCase(eOfficeTaskDetailsModel entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                viewModel.TaskId = Convert.ToString(objStudentDetails.eOfficeTaskDetails(entity));

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("GetTaskDetails")]
        public IHttpActionResult GetTaskDetails(eOfficeTaskDetailsModel entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                viewModel.lstTasksDetails = objStudentDetails.GetTaskDetails();

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }


        [HttpPost]
        [ActionName("AddGrDetails")]
        public IHttpActionResult AddGrDetails(GRDetails entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                objStudentDetails.eOfficeGRDetails(entity);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("GetGRDetails")]
        public IHttpActionResult GetGRDetails(GRDetails entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                viewModel.lstGRDetails = objStudentDetails.GetGRDetails(entity);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }


        [HttpPost]
        [ActionName("UpdateGrDetails")]
        public IHttpActionResult UpdateGrDetails(GRDetails entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                objStudentDetails.UpdateGRDetails(entity);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }


        [HttpPost]
        [ActionName("GetEOfficeTaslDetails")]
        public IHttpActionResult GetEOfficeTaslDetails(GRDetails entity)
        {
            eOfficeModel viewModel = new eOfficeModel();
            try
            {
                eOfficeDB objStudentDetails = new eOfficeDB();
                viewModel.lstGRDetails = objStudentDetails.GetEOfficeTaskDetails(entity);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
    }
}
