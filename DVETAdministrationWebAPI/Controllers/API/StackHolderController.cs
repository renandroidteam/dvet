﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class StackHolderController : ApiController
    {
        [HttpPost]
        [ActionName("AddQuestions")]
        public IHttpActionResult AddQuestions(StackHolderAddQuestions ques)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.status = obj.AddQuestions(ques);
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("AddSurvey")]
        public IHttpActionResult AddSurvey(StackHolderSurvey ques)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.status = obj.AddSurvey(ques);
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
        [HttpPost]
        [ActionName("AddMultiQuestions")]
        public IHttpActionResult AddMultiQuestions(stackmodel ques)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                foreach (var item in ques.model)
                {
                    StackHolderDB obj = new StackHolderDB();

                    viewModel.status = obj.AddMultiQuestions(item);
                }
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("UpdateQuestions")]
        public IHttpActionResult UpdateQuestions(StackHolderAddQuestions ques)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.status = obj.UpdateQuestions(ques);
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("GetQuestions")]
        public IHttpActionResult GetQuestions(StackHolderAddQuestions SurveyId)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.addquestions = obj.GetQuestions(SurveyId.SurveyId);
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
        [HttpPost]
        [ActionName("GetSurvey")]
        public IHttpActionResult GetSurvey()
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.addsurvey = obj.GetSurvey();
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("DeleteQuestions")]
        public IHttpActionResult DeleteQuestions(StackHolderAddQuestions ques)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB obj = new StackHolderDB();

                viewModel.status = obj.DeleteQuestions(ques);
            }
            catch (Exception ex)
            {

                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("GetStakeHoldersSurveyDetails")]
        public IHttpActionResult GetStakeHoldersSurveyDeatisl()
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB objApplicantRepository = new StackHolderDB();

                viewModel.lstsurveydetila = objApplicantRepository.GetStakeHoldersSurveyDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(viewModel);

        }



        [HttpPost]
        [ActionName("GetStakeHoldersFeedBackDetails")]
        public IHttpActionResult GetStakeHoldersFeedBackDetails(StackHolderFeedbackDetails obj)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB objApplicantRepository = new StackHolderDB();

                viewModel.lstFeedBack = objApplicantRepository.GetStackHolderFeedbackDetails(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("GetStackHolderRespondantDetails")]
        public IHttpActionResult GetStackHolderRespondantDetails(StackHoldersrespondantDetails obj)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB objApplicantRepository = new StackHolderDB();

                viewModel.lstResponadant = objApplicantRepository.GetStackHolderRespondantDetails(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(viewModel);

        }


        [HttpPost]
        [ActionName("GetStackHolderRemarksDetails")]
        public IHttpActionResult GetStackHolderRemarksDetails(StackHoldersrespondantDetails obj)
        {
            StackHolderModel viewModel = new StackHolderModel();
            try
            {
                StackHolderDB objApplicantRepository = new StackHolderDB();

                viewModel.lstResponadant = objApplicantRepository.GetStackHolderRemarksDetails(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(viewModel);

        }
    }
}
