﻿using DataAccess.Master;
using DataModel.Master;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class EFWorkFlowController : ApiController
    {
        ITaskDL<TaskResponseModel, int> _repository;
        IWorkflowDL<WFinitiationModel, int> _repositoryWF;
        IWorkFlowComments<WFCommentsModel, int> _repositoryComments;
        public EFWorkFlowController(ITaskDL<TaskResponseModel, int> r, IWorkflowDL<WFinitiationModel, int> r1, IWorkFlowComments<WFCommentsModel, int> r2)
        {
            _repository = r;
            _repositoryWF = r1;
            _repositoryComments = r2;
        }
        public EFWorkFlowController()
        {
        }
       [HttpPost]
        [Route("api/EFWorkFlow/TaskDetails")]
        public IHttpActionResult TaskDetails(TaskRequestModel ViewModel)
        {
            TaskDL objTaskDL = new TaskDL();
            IList<TaskResponseModel> lstTask = new List<TaskResponseModel>();
            try
            {
                if (ViewModel != null)
                {
                    lstTask = objTaskDL.GetTaskDetails(ViewModel);
                }
            }
            catch
            {
            }
            return Ok(lstTask);
        }

        [Route("api/EFWorkFlow/WFIntiation")]
        [ResponseType(typeof(int))]
        public IHttpActionResult WFIntiation(WFinitiationModel ViewModel)
        {
            WorkflowDL objWorkflowDL = new WorkflowDL();
            try
            {
                if (ViewModel != null)
                {
                    objWorkflowDL.WFIntiation(ViewModel);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }


        [HttpPost]
        [Route("api/EFWorkFlow/WFAction")]
        public IHttpActionResult WFAction(WFServiceModel ViewModel)
        {
            WorkflowDL objWorkflowDL = new WorkflowDL();
            try
            {
                if (ViewModel != null)
                {
                    objWorkflowDL.WFAction(ViewModel);
                }
               // return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }
            return Ok();
        }
        [HttpPost]
        [Route("api/EFWorkFlow/GetWorkflowDetails")]
        public HttpResponseMessage GetWorkflowDetails(WorkflowInstanceDetailsModel model)
        {
            WorkflowDL Dl = new WorkflowDL();
            WorkflowInstanceDetailsModel obj = new WorkflowInstanceDetailsModel();
            var json = "";
            obj = Dl.GetWorkflowDetails(model);
            json = JsonConvert.SerializeObject(obj);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }
    }
}
