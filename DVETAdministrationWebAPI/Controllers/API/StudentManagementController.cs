﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;
using System.Net.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class StudentManagementController : ApiController
    {
        //Fetch case details based on search input on Legal-> Search
        


        [HttpPost]
        [ActionName("GetStudentProfileDetails")]
        public StudentManagementModel GetStudentProfileDetails(StudentProfileDetails obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.model = objStudentDetails.GetStudentManagementProfileDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetStudentAttendenceDetails")]
        public StudentManagementModel GetStudentAttendenceDetails(StudentSemesterAttendance obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.lstSemesterAttendance = objStudentDetails.GetStudentAttendenceDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetStudentLeaveDetails")]
        public StudentManagementModel GetStudentLeaveDetails(StudentSemesterLeave obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.lstSemesterLeave = objStudentDetails.GetStudentLeaveDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetStudentResultDetailsS")]
        public StudentManagementModel GetStudentResultDetailsS(StudentSemesterResult obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.lstSemesterResult = objStudentDetails.GetStudentResultDetailsS(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetStudentScholorshipDetails")]
        public StudentManagementModel GetStudentScholorshipDetails(ScholorshipDetails obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.scholor = objStudentDetails.GetStudentScholorshipDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetStudentPlacementDetails")]
        public StudentManagementModel GetStudentPlacementDetails(PlacementDetails obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.placement = objStudentDetails.GetStudentPlacementDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }



        [HttpPost]
        [ActionName("GetStudentPenaltyDetils")]
        public StudentManagementModel GetStudentPenaltyDetils(StudentPenalty obj)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.lstSemesterPenalty = objStudentDetails.GetStudentPenaltyDetils(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("AddStudentProfileDetails")]
        public IHttpActionResult AddCase(StudentProfileDetails entity)
        {
            StudentManagementModel viewModel = new StudentManagementModel();
            try
            {
                StudentManagementDB objStudentDetails = new StudentManagementDB();
                viewModel.StudentId = Convert.ToString(objStudentDetails.studentProfileDetails(entity));

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
    }
}
