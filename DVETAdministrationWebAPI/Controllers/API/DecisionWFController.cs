﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class DecisionWFController : ApiController

    {
        [HttpPost]
        [ActionName("WFProcess")]
        public DecisionWFModel WFProcess(DecisionWF process)
        {
            DecisionWFModel viewModel = new DecisionWFModel();
            try
            {
                DecisionWFDB decisionobj = new DecisionWFDB();
                bool result = decisionobj.WFProcess(process);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("WFIntiation")]
        public DecisionWFModel WFIntiation(DecisionCaseModel intiation)
        {
            DecisionWFModel viewModel = new DecisionWFModel();
            try
            {
                DecisionWFDB decisionobj = new DecisionWFDB();
                bool result = decisionobj.WFIntiation(intiation);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("WFTasklist")]
        public DecisionWFModel GetCaseDetails(DecisionWF casedetails)
        {

            DecisionWFModel viewModel = new DecisionWFModel();
            try
            {
                DecisionWFDB objdec = new DecisionWFDB();
                viewModel.lstCase = objdec.WFTasklist(casedetails);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        
    }
}
