﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;
using System.Net.Http;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class LegalController : ApiController
    {
        //Fetch all case count for Legal-> Dashboard
        [HttpGet]
        [ActionName("CaseCount")]
        public LegalModel GetLegalModel()
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseCount = objLegal.GetDashboardData();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetDashboard")]
        public LegalModel GetDashboard(AuthEmailId model)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseCount = objLegal.GetDashboard(model.EmailId,model.CreatedBy);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetHearingDashboard")]
        public LegalModel GetHearingDashboard(AuthEmailId model)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseCount = objLegal.GetHearingDashboard(model.EmailId, model.CreatedBy);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        //Fetch case details based on search input on Legal-> Search
        [HttpGet]
        [ActionName("GetCaseData")]
        public HttpResponseMessage GetCaseData([FromUri]CaseDetails entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseDetails = objLegal.GetCaseData(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Fetch recent cases based on search input on Legal-> Search
        [HttpGet]
        [ActionName("GetRecentCases")]
        public HttpResponseMessage GetRecentCases([FromUri]RecentCases entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstRecentCases = objLegal.GetRecentCases(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Add case details on Legal-> Add Case
        [HttpPost]
        [ActionName("AddCase")]
        public IHttpActionResult AddCase(CaseDetails entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.AddCase(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Update case details on Legal-> Edit Case
        [HttpPost]
        [ActionName("UpdateCase")]
        public IHttpActionResult UpdateCase(CaseDetails entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();

                viewModel.status = objLegal.UpdateCase(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Add case details on Legal-> Add Case
        [HttpPost]
        [ActionName("AddCaseComment")]
        public IHttpActionResult AddCaseComment(CaseComment entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.AddCaseComment(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Add case details on Legal-> Add Case
        [HttpPost]
        [ActionName("AddCaseHistory")]
        public IHttpActionResult AddCaseHistory(CaseHistory entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.AddCaseHistory(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Gets case comments
        [HttpGet]
        [ActionName("GetCaseComment")]
        public HttpResponseMessage GetCaseComment([FromUri]CaseComment entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseComment = objLegal.GetCaseComment(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Gets case history
        [HttpGet]
        [ActionName("GetCaseHistory")]
        public HttpResponseMessage GetCaseHistory([FromUri]CaseHistory entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseHistory = objLegal.GetCaseHistory(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Add case articipant on Legal-> Participants
        [HttpPost]
        [ActionName("AddCaseParticipants")]
        public IHttpActionResult AddCaseParticipants(CaseParticipants entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.NewParticipantID = Convert.ToString(objLegal.AddCaseParicipants(entity));
                viewModel.status = "Success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Add case articipant on Legal-> Participants
        [HttpPost]
        [ActionName("UpdateCaseParticipants")]
        public IHttpActionResult UpdateCaseParticipants(CaseParticipants entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.UpdateCaseParicipants(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Fetch case details based on search input on Legal-> Search
        [HttpGet]
        [ActionName("GetCaseParticipants")]
        public HttpResponseMessage GetCaseParticipants([FromUri]int CasePKID)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                CaseParticipants entity = new CaseParticipants();
                entity.CasePKID = CasePKID;
                LegalDB objCaseParticipant = new LegalDB();
                viewModel.lstCaseParticipants = objCaseParticipant.GetCaseParticipants(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Fetch all case count for Legal-> Dashboard
        [HttpGet]
        [ActionName("GetCaseParticipantByEmail")]
        public HttpResponseMessage GetCaseParticipantByEmail([FromUri]CaseParticipantByEmail entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseParticipantsByEmailId = objLegal.GetCaseParticipantsByEmailId(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Fetch case details based on search input on Legal-> Search
        [HttpGet]
        [ActionName("GetHearingParticipants")]
        public HttpResponseMessage GetHearingParticipants([FromUri]CaseParticipants entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objCaseParticipant = new LegalDB();
                viewModel.lstCaseParticipants = objCaseParticipant.GetHearingParticipants(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Add case hearing participants on Legal-> Participants
        [HttpPost]
        [ActionName("AddHearingParticipants")]
        public IHttpActionResult AddHearingParticipants(CaseParticipants Model)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objCaseParticipant = new LegalDB();
                 objCaseParticipant.AddHearingParicipants(Model);
                viewModel.status = "Success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            String HOST = ConfigurationManager.AppSettings["SMTPServer"].ToString();
            string BODY = "Dear " + Model.Name + "\n\n\n" +
                          "You are  added  as participant for Case No :  " + Model.CaseId + ".\n" +
                          "Thank You.";

            MailMessage message = new MailMessage();
            MailAddress Sender = new MailAddress(ConfigurationManager.AppSettings["from"].ToString());
            MailAddress receiver = new MailAddress(Model.HearingInvitedParticipants);
            SmtpClient smtp = new SmtpClient()
            {

                Host = ConfigurationManager.AppSettings["SMTPServer"],
                Port = 587,
                EnableSsl = true,
                Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"])

            };
            message.Subject = "DVET";
            message.From = Sender;
            message.To.Add(receiver);
            message.Body = BODY;
            message.IsBodyHtml = true;
            smtp.Send(message);

            string msgText = "Dear " + Model.Name + " You are  added  as \n" +
                             "participant for Legal Case No :  " + Model.HearingNumber + ".\n" +
                             "The hearing date is : " + Model.hearingDate + ".\r\n" +
                              "Thank You.";


            string sURL;
            sURL = "https://api-alerts.kaleyra.com/v4/?api_key=Ab5705f13eb3633fc45529e15c1f763f4&method=sms&message=" + msgText + "&to=" + Model.MobileNumber + "&sender=MAHAIT";

            try
            {
                using (WebClient client = new WebClient())
                {
                    string s = client.DownloadString(sURL);

                }

            }

            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //update case hearing participants on Legal-> Participants
        [HttpPost]
        [ActionName("UpdateHearingParticipants")]
        public IHttpActionResult UpdateHearingParticipants(CaseParticipants entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objCaseParticipant = new LegalDB();
                viewModel.status = objCaseParticipant.UpdateHearingParicipants(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //get case hearing Legal-> Hearing
        [HttpGet]
        [ActionName("GetCaseHearing")]
        public HttpResponseMessage GetCaseHearing([FromUri]CaseHearing entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseHearing = objLegal.GetCaseHearing(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //get case hearing Legal-> Hearing
        [HttpGet]
        [ActionName("GetCaseHearingNumbers")]
        public HttpResponseMessage GetCaseHearingNumbers([FromUri]CaseHearing entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseHearing = objLegal.GetCaseHearingNumbers(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        //Add applicant and respondent on Legal-> Applicants/Respondent
        [HttpPost]
        [ActionName("AddApplicantAndRespondent")]
        public IHttpActionResult AddApplicantAndRespondent(CaseApplicantsAndRespondents entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objCaseApplicantsAndRespondents = new LegalDB();
                viewModel.status = objCaseApplicantsAndRespondents.AddApplicantAndRespondent(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //Add applicant and respondent on Legal-> Applicants/Respondent
        [HttpPost]
        [ActionName("UpdateApplicantAndRespondent")]
        public IHttpActionResult UpdateApplicantAndRespondent(CaseApplicantsAndRespondents entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objCaseParticipant = new LegalDB();
                viewModel.status = objCaseParticipant.UpdateApplicantAndRespondent(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        //get case applicants/respondents Legal-> applicants/respondents
        [HttpGet]
        [ActionName("GetApplicantAndRespondent")]
        public HttpResponseMessage GetApplicantAndRespondent([FromUri]CaseApplicantsAndRespondents entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstApplicantsAndRespondents = objLegal.GetApplicantsAndRespondents(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }


        //get case hearing Legal-> Hearing
        [HttpPost]
        [ActionName("UpdateCaseHearing")]
        public HttpResponseMessage UpdateCaseHearing(CaseHearing entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                objLegal.UpdateCaseHearing(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }

        [HttpPost]
        [ActionName("SendHearingReminderEmail")]
        public string SendHearingReminderEmail()
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.SendEmailReminderToHearingParicipants();
                //viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            //return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
            return viewModel.status;

            //EmailServiceClient emailClient = new EmailServiceClient();
            //return Ok(emailClient.ComposeMail(model));
        }

        [HttpPost]
        [ActionName("SendHearingReminderSMS")]
        //public string  SendHearingReminderSMS(Master_SMSModel model)
        public string SendHearingReminderSMS()
        {
           // ShortMessageServiceClient shortMessageServiceClient = new ShortMessageServiceClient();


            //return Ok(shortMessageServiceClient.SendSingleSms(model.mobileNo, model.msgBody));
            //return "Success";

            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.status = objLegal.SendSMSReminderToHearingParicipants();
                //viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            //return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
            return viewModel.status;
        }

        [HttpPost]
        [ActionName("GetEmployeeByEmail")]

        public MasterModel GetEmployeeByEmail(Master_Employee email)
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                LegalDB objCitizenUser = new LegalDB();
                viewModel.lstEmployees = objCitizenUser.GetEmployeeByEmail(email.EmailId);
                viewModel.status = "Successfully fetch the employees.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
        [HttpGet]
        [ActionName("SearchCaseData")]
        public HttpResponseMessage SearchCaseData([FromUri]CaseDetails entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseDetails = objLegal.SearchCaseData(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }
        [HttpPost]
        [ActionName("Report")]

        public HttpResponseMessage Report(Report entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                LegalDB objLegal = new LegalDB();
                viewModel.lstReport = objLegal.Report(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }


        [HttpPost]
        [ActionName("SetCaseComment")]
        public LegalModel SetCaseComment(CaseComment entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstCaseComment = objLegal.SetCaseComment(entity);
                viewModel.status = "Successfully fetch the CaseComments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetAuthUsers")]
        public LegalModel GetAuthUsers(AuthEmailId entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstEmail = objLegal.GetAuthUsers(entity);
                viewModel.status = "Successfully fetch the CaseComments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("GetAuthTypes")]
        public LegalModel GetAuthTypes(AuthEmailId entity)
        {
            LegalModel viewModel = new LegalModel();
            try
            {
                LegalDB objLegal = new LegalDB();
                viewModel.lstEmail = objLegal.GetAuthTypes(entity);
                viewModel.status = "Successfully fetch the CaseComments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
    }
}