﻿using System;
using System.Collections.Generic;
using System.Linq;
using DVETAdministrationWebAPI.APIModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAccess.Master;
using DataModel.Master;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class WorkflowController : ApiController
    {
        [HttpPost]
        [ActionName("WorkflowIntiation")]
        public WorkflowViewModel WorkflowIntiation(WorkflowRequestViewModel viewModel)
        {
            WorkflowViewModel models = new WorkflowViewModel();

            try
            {

                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }

        [HttpPost]
        [ActionName("WorkflowTask")]
        public WorkflowViewModel WorkflowTask(WorkflowRequestViewModel viewModel)
        {
            WorkflowViewModel models = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {
                models.lstWorkflowModel = objRepository.GetWorkflowInwardtasklist(viewModel.CreatedBy,viewModel.WfStatus);
                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }


        [HttpPost]
        [ActionName("WorkflowProcess")]
        public WorkflowViewModel WorkflowProcess(WorkflowProcessViewModel viewModel)
        {
            WorkflowViewModel models = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {
                 objRepository.WorkflowProces(viewModel.WfProcessModel);

                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }

        [HttpPost]
        [ActionName("WorkflowProcesses")]
        public WorkflowViewModel WorkflowProcesses(WorkflowProcessViewModel viewModel)
        {
            WorkflowViewModel models = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {
                objRepository.WorkflowProcess(viewModel.WfProcessModel);

                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }

        [HttpPost]
        [ActionName("WorkflowDetails")]
        public WorkflowViewModel WorkflowDetails(WorkflowProcessModel WorkflowProcessModel)
        {
            WorkflowViewModel viewModel = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {

                viewModel.model = objRepository.GetAssignedDatabycasenumber(WorkflowProcessModel.CaseNumber);
                viewModel.Status = "Success";
            }

            catch (Exception ex)
            {
                viewModel.Status = "failure";
                viewModel.errorMsg = ex.InnerException.Message;
            }

            return viewModel;
        }



        [HttpPost]
        [ActionName("WorkflowApprovedTask")]
        public WorkflowViewModel WorkflowApprovedTask(WorkflowRequestViewModel viewModel)
        {
            WorkflowViewModel models = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {
                models.lstWorkflowModel = objRepository.GetWorkflowApprovedInwardtasklist(viewModel.CreatedBy, viewModel.WfStatus);
                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }




       [HttpPost]
        [ActionName("WorkflowProcessStatus")]
        public WorkflowViewModel WorkflowProcessStatus(WorkflowProcessViewModel viewModel)
         {
            WorkflowViewModel models = new WorkflowViewModel();
            WorkflowDB objRepository = new WorkflowDB();
            try
            {
                objRepository.WorkflowProcessStatus(viewModel.WfProcessModel);

                models.Status = "success";

            }
            catch (Exception ex)
            {
                models.Status = "failure";
                models.errorMsg = ex.InnerException.Message;
            }
            return models;
        }

    }
}
