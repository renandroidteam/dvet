﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class DecisionController : ApiController
    {

        [HttpPost]
        [ActionName("Decision_Dashboard")]
        public DecisionViewModel Decision_Dashboard()
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objeOffice = new DecisionDB();
                viewModel.lstDecisionCases = objeOffice.DashBoard();
                viewModel.TotalCases = viewModel.lstDecisionCases.Count;
                viewModel.AssignedCases= viewModel.lstDecisionCases.Count;
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("InsertCaseFlow")]
        public DecisionViewModel InsertCaseFlow(Decision_CaseFlowModel caseflow)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionobj = new DecisionDB();
                bool result = decisionobj.InsertCaseFlow(caseflow);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("InsertCaseDetails")]
        public DecisionViewModel InsertCaseDetails(decisionmodel casedetails)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                foreach (var item in casedetails.model)
                {
                    DecisionDB decisionobj = new DecisionDB();
                    bool result = decisionobj.InsertCaseDetails(item);
                }
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("InsertCase")]
        public DecisionViewModel InsertCase(DecisionCaseModel cased)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionobj = new DecisionDB();
                viewModel.status = decisionobj.InsertCase(cased);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetCaseDetails")]
        public DecisionViewModel GetCaseDetails(Decision_CaseDetails casedetails)
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstCaseDetails = objdec.GetCaseDetails(casedetails.CaseId);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("GetCaseFlow")]
        public DecisionViewModel GetCaseFlow(Decision_CaseFlowModel caseflow)
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstCaseFlow = objdec.GetCaseFlow(caseflow.CaseID);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetAllFlow")]
        public DecisionViewModel GetAllFlow()
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstCaseFlow = objdec.GetAllFlow();

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetFlow")]
        public DecisionViewModel GetFlow(DecisionCaseModel caseflow)
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstDecisionCases = objdec.GetFlow(caseflow.Emailid);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("GetofficerDetails")]
        public DecisionViewModel GetofficerDetails(CaseOfficerDetails officer)
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstofficer = objdec.GetofficerDetails();

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("GetInstituteDetails")]
        public DecisionViewModel GetInstituteDetails(CaseOfficerDetails officer)
        {

            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objdec = new DecisionDB();
                viewModel.lstofficer = objdec.GetInstituteDetails();

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("GetCaseHistory")]
        public DecisionViewModel GetCaseHistory(Decision_CaseFlowModel caseflow)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB objeOffice = new DecisionDB();
                viewModel.lstDecisionCases = objeOffice.GetCaseHistory(caseflow.FKID_Case);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("UpdateAssignedCase")]
        public DecisionViewModel UpdateAssignedCase(Decision_CaseFlowModel caseflow)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionObj = new DecisionDB();
                bool result = decisionObj.UpdateAssignedCase(caseflow);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("SearchCase")]
        public DecisionViewModel SearchCase(DecisionCaseModel searchcase)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionObj = new DecisionDB();
                viewModel.lstDecisionCases = decisionObj.SearchCase(searchcase);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetCase")]
        public DecisionViewModel GetCase(DecisionCaseModel getcase)
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionObj = new DecisionDB();
                viewModel.lstDecisionCases = decisionObj.GetCase(getcase);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("GetCaseID")]
        public DecisionViewModel GetCaseID()
        {
            DecisionViewModel viewModel = new DecisionViewModel();
            try
            {
                DecisionDB decisionObj = new DecisionDB();
                viewModel.lstDecisionCases = decisionObj.GetCaseID();

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
    }
}