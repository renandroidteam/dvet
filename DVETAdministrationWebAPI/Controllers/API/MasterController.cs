﻿using DataAccess.Master;
using DVETAdministrationWebAPI.APIModel;
using DataModel.Master;
using System;
using System.Web.Http;
using DVETAdministrationWebAPI.Viewmodels.Admin;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class MasterController : ApiController
    {
        [HttpGet]
        [ActionName("Master_Region")]
        public MasterModel Master_Region()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objRegionType = new MasterDB();
                viewModel.lstRegion = objRegionType.GetRegion();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_CourtType")]
        public MasterModel Master_CourtType()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objCourtType = new MasterDB();
                viewModel.lstCourtType = objCourtType.GeCourtType();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }

            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_AidType")]
        public MasterModel Master_AidType()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objAidType = new MasterDB();
                viewModel.lstAidType = objAidType.GetAidType();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_CaseType")]
        public MasterModel Master_CaseType()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objCaseType = new MasterDB();
                viewModel.lstCaseType = objCaseType.GetCaseType();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_CaseClassification")]
        public MasterModel Master_CaseClassification()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objCaseClassification = new MasterDB();
                viewModel.lstCaseClassification = objCaseClassification.GetCaseClassification();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_LegalStatus")]
        public MasterModel Master_LegalStatus()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objLegalStatus = new MasterDB();
                viewModel.lstLegalStatus = objLegalStatus.GetLegalStatus();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_ParticipantType")]
        public MasterModel Master_ParticipantType()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstParticipantType = objParticipantType.GetParticipantType();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("SendSMS")]
        public IHttpActionResult Master_SendSMS(Master_SMSModel model)
        {
            ShortMessageServiceClient shortMessageServiceClient = new ShortMessageServiceClient();

            return Ok(shortMessageServiceClient.SendSingleSms(model.mobileNo, model.msgBody));
        }

        [HttpPost]
        [ActionName("SendEmail")]
        public IHttpActionResult Master_SendEmail(EmailContent model)
        {
            EmailServiceClient emailClient = new EmailServiceClient();
            int? status = 1;
            emailClient.ComposeMail(model);
            return Ok(status);
            //return Ok(emailClient.ComposeMail(model));
        }

        [HttpGet]
        [ActionName("Master_Institutes")]
        public MasterModel GetInstitutes()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstInstitute = objParticipantType.GetInstitutes();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_InstituteTypes")]
        public MasterModel GetInstituteTypes()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstInstituteType = objParticipantType.GetInstitutesTypes();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_Department")]
        public MasterModel GetDepartments()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstDepartments = objParticipantType.GetDepartments();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_Employees")]
        public MasterModel GetEmployees()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstEmployees = objParticipantType.GetEmployees();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_CitizenUserRoles")]
        public MasterModel Master_CitizenUserRoles()
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objParticipantType = new MasterDB();
                viewModel.lstcitizenRoles = objParticipantType.GetCitizen_UserRoles();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetEmployeeByEmail")]

        public MasterModel GetEmployeeByEmail(Master_Employee email)
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                MasterDB objCitizenUser = new MasterDB();
                viewModel.lstEmployees = objCitizenUser.GetEmployeeByEmail(email.EmailId);
                viewModel.status = "Successfully fetch the employees.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }




        [HttpGet]
        [ActionName("Master_Desks")]
        public InwordViewModel Master_Desks()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();

                viewModel.listDeskdetails = objDb.GetDesks();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }



        [HttpGet]
        [ActionName("Master_Departments")]
        public InwordViewModel Master_Depaerment()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();

                viewModel.listDepartment = objDb.GetDepartments();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }



        [HttpPost]
        [ActionName("Master_EmployeesDetails")]
        public InwordViewModel Employee(Employee obj)
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();

                viewModel.listEmployee = objDb.GetEmployees(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("Master_Lettertypes")]
        public InwordViewModel Master_Lettertype()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();

                viewModel.listType = objDb.GetLetterTypes();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_CentralGovernmentOffices")]
        public InwordViewModel Master_CentralGovernmentOffices()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();

                viewModel.listOffice = objDb.GetCentralGovernmentOffices();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("Master_Status")]
        public InwordViewModel Master_Status()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();
                viewModel.listStatus = objDb.GetStatusDetails();
                viewModel.status = "Success";
            }

            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;

            }

            return viewModel;
        }
        [HttpGet]
        [ActionName("Note_tatus")]
        public InwordViewModel Note_tatus()
        {
            InwordViewModel viewModel = new InwordViewModel();
            try
            {
                InwardDB objDb = new InwardDB();
                viewModel.listNotes = objDb.GetNoteStatusDetails();
                viewModel.status = "Success";
            }

            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;

            }

            return viewModel;
        }
    }
}