﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static DataAccess.Master.InwardDB;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class InwardController : ApiController
    {
        [HttpPost]
        [ActionName("IoAddCase")]
        public IHttpActionResult AddCase(Inwardmodel entity)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB objLegal = new InwardDB();
                viewModel.CaseId = Convert.ToString(objLegal.IoAddCase(entity));

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
        //fetch the data
        [HttpPost]
        [ActionName("GetInwardData")]
        public InwardOutwardmodel GetInwardModel(Inwardmodel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB onjInward = new InwardDB();
                //onjInward.GetInwardData();
                // viewModel.lstentitymodels = onjInward.GetInwardData(Convert.ToInt32(obj.CaseID));
                viewModel.lstentitymodels = onjInward.GetInwardData(obj.Id);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        //fetch the data
        [HttpPost]
        [ActionName("Search")]
        public InwardOutwardmodel Search(Inwardmodel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            InwardDB _repository = new InwardDB();
            viewModel.lstentitymodels = _repository.SearchCase(obj);
            return viewModel;
        }

        //fetch the id data
        [HttpPost]
        [ActionName("GetInwarddatabycaseid")]
        public InwardOutwardmodel GetInwarddatabycaseid(Inwardmodel model)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            InwardDB _repos = new InwardDB();
            viewModel.model = _repos.GetInwardDatabycaseno(model.CaseNumber);
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetInwarddatabycasenumber")]
        public InwardOutwardmodel GetInwarddatabycasenumber(Inwardmodel model)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            InwardDB _repos = new InwardDB();
            viewModel.model = _repos.GetInwardDatabycasenumber(model.CaseNumber);
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetInwardMYPendingTask")]
        public InwardOutwardmodel GetInwardMYPendingTasks(Inwardmodel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB onjInward = new InwardDB();

                viewModel.lstentitymodels = onjInward.GetInwardMYPendingTask(obj.AssignedTo);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("AddDeskOfficerResponseDetails")]
        public IHttpActionResult AddDeskOfficeDetails(DeskOfficerResponseModel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                onjInward.AddDeskOfficerDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("GetDeskOfficerResponseDetails")]
        public InwardOutwardmodel GetDeskOfficerDetails(DeskOfficerResponseModel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB onjInward = new InwardDB();

                viewModel.lstentitymodel = onjInward.GetDeskOfficerResponseDetails(Convert.ToInt32(obj.CaseNo));
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("AddFileNotes")]
        public IHttpActionResult AddFileNotes(AddfileNotes obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                onjInward.AddFileNotes(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("GetFileNotes")]
        public InwardOutwardmodel GetFileNotes(AddfileNotes obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB onjInward = new InwardDB();

                viewModel.lstFiles = onjInward.GetFileNotesDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("AddFileDetails")]
        public IHttpActionResult AddFileDetails(AddFileDetails obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                onjInward.AddFileDetails(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("AddFileCaseMapping")]
        public IHttpActionResult AddFileCaseMapping(AddFileCaseMapping obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                onjInward.AddFileCaseMapping(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("GetFileDetails")]
        public InwardOutwardmodel GetFileDetails(AddFileDetails obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                viewModel.lstFile = onjInward.GetFileDetails(obj.CaseId);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;

        }


        [HttpPost]
        [ActionName("AddFiles")]
        public IHttpActionResult AddFiles(Addfiles obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                onjInward.AddFiles(obj);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);

        }

        [HttpPost]
        [ActionName("Viewfiles")]
        public InwardOutwardmodel Viewfiles(Addfiles obj)
        {
            //Addfiles obj1 = new Addfiles();
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            InwardDB _repository = new InwardDB();
            viewModel.lstsearchViewmodel = _repository.Viewfiles(obj);
            return viewModel;
        }

        [HttpPost]
        [ActionName("Listofcases")]
        public InwardOutwardmodel Listofcases(Inwardmodel obj)
        {
            //Addfiles obj1 = new Addfiles();
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            InwardDB _repository = new InwardDB();
            viewModel.lstListofcasesmodel = _repository.Listofcases(obj);
            return viewModel;
        }


        [HttpPost]
        [ActionName("IoAddNoteStatus")]
        public IHttpActionResult IoAddNoteStatus(NoteStatusDecision entity)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();
            try
            {
                InwardDB objLegal = new InwardDB();
                objLegal.IinsertNotesStatus(entity);

                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }



        
            [HttpPost]
            [ActionName("UpdateWorkflowStatus")]

            public WorkflowViewModel UpdateWorkflowStatus(WorkflowProcessModel viewModel)
            {
                WorkflowViewModel objView = new WorkflowViewModel();
                try
                {
                    InwardDB objwork = new InwardDB();
                    //objView.WorkflowStatus = 
                    objwork.UpdateWorkFlowStatus(viewModel);
                    objView.Status = "success";
                }
                catch (Exception ex)
                {
                    objView.errorMsg = "Error";
                    objView.errorMsg = ex.Message;
                }
                return objView;
            }



        [HttpPost]
        [ActionName("InsertWorkflowRemarks")]

        public WorkflowViewModel InsertWorkflowRemarks(WorkflowProcessModel viewModel)
        {
            WorkflowViewModel objView = new WorkflowViewModel();
            try
            {
                InwardDB objwork = new InwardDB();
                //objView.WorkflowStatus = 
                objwork.InsertWorkflowRemarks(viewModel);
                objView.Status = "success";
            }
            catch (Exception ex)
            {
                objView.errorMsg = "Error";
                objView.errorMsg = ex.Message;
            }
            return objView;
        }




        [HttpPost]
        [ActionName("UpdateWorkflowApprovedStatus")]

        public InwardOutwardmodel UpdateWorkflowApprovedStatus(NoteStatusDecision viewModel)
        {
            InwardOutwardmodel objView = new InwardOutwardmodel();
            try
            {
                InwardDB objwork = new InwardDB();
                //objView.WorkflowStatus = 
                objwork.UpdateWorkFlowApprovedStatus(viewModel);
                objView.status = "success";
            }
            catch (Exception ex)
            {
                objView.ErrorMessage = "Error";
                objView.ErrorMessage = ex.Message;
            }
            return objView;
        }



        [HttpPost]
        [ActionName("UpdateWorkflowRejectedStatus")]

        public InwardOutwardmodel UpdateWorkflowRejectedStatus(NoteStatusDecision viewModel)
        {
            InwardOutwardmodel objView = new InwardOutwardmodel();
            try
            {
                InwardDB objwork = new InwardDB();
                //objView.WorkflowStatus = 
                objwork.UpdateWorkFlowRejectedStatus(viewModel);
                objView.status = "success";
            }
            catch (Exception ex)
            {
                objView.ErrorMessage = "Error";
                objView.ErrorMessage = ex.Message;
            }
            return objView;
        }


        [HttpPost]
        [ActionName("GetDocumentDetails")]
        public InwardOutwardmodel GetDocumentDetails(Inwardmodel obj)
        {
            InwardOutwardmodel viewModel = new InwardOutwardmodel();

            try
            {
                InwardDB onjInward = new InwardDB();

                viewModel.lstdocuments = onjInward.GetDocumentDetials(obj.CaseNumber);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;

        }

    }
}
