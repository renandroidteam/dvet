﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Net.Http;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class CitizenCharterController : ApiController
    {
        [HttpPost]
        [ActionName("AddCitizenUser")]

        public CitizenCharterModel AddCitizenUser(Citizencharter citizencharterUser)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.AddUSer(citizencharterUser);
                viewModel.status = "Successfully added Citizen user";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("AddCitizenCharter")]
        public CitizenCharterModel AddCitizenCharter(CitizenCharter citizencommentsmodel)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.status = objCitizenUser.AddCitizenCharter(citizencommentsmodel);
                //viewModel.status = "Successfully added the comments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetCitizenCharter")]
        public CitizenCharterModel GetCitizenCharter()
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstcc = objCitizenUser.GetCitizenCharter();
                //viewModel.status = "Successfully added the comments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetYear")]
        public CitizenCharterModel GetYear()
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstyear = objCitizenUser.GetYear();
                //viewModel.status = "Successfully added the comments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
        [HttpPost]
        [ActionName("DeactivateCitizenUser")]

        public CitizenCharterModel DeactivateCitizenUser(Citizencharter citizencharterUser)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.DeactivateCitizenUser(citizencharterUser);
                viewModel.status = "Successfully deactivated Citizen user.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("AssignCitizenUserRole")]
        public CitizenCharterModel AssignCitizenUserRole(CitizenCharterModel citizencharterUser)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                bool result = objCitizenUser.AssignCitizenUserRole(citizencharterUser.PKID_User, citizencharterUser.CitizenUserRoles);
                viewModel.status = "Successfully assign the roles to the Citizen user.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("UnAssignCitizenUserRole")]
        public CitizenCharterModel UnAssignCitizenUserRole(CitizenCharterModel citizencharterUser)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                bool result = objCitizenUser.UnAssignCitizenUserRole(citizencharterUser.PKID_User, citizencharterUser.CitizenUserRoles);
                viewModel.status = "Successfully unassign the roles to the Citizen user.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("AddCitizenComments")]
        public CitizenCharterModel AddCitizenComments(CitizenCommentsModel citizencommentsmodel)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.status = objCitizenUser.AddCitizenComments(citizencommentsmodel);
                //viewModel.status = "Successfully added the comments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetCitizenComment")]
        public CitizenCharterModel GetCitizenComment(CitizenCommentsModel citizencommentsmodel)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstCitizenComment = objCitizenUser.GetCitizenComment(citizencommentsmodel);
                //viewModel.status = "Successfully added the comments.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
        

        [HttpGet]
        [ActionName("GetCoreGroupMembers")]
        public HttpResponseMessage GetCoreGroupMembers()
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                CitizenCharterDB objcoregroup = new CitizenCharterDB();
                viewModel.lstCoreGroupMembers = objcoregroup.GetCoreGroupMembers();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }


        [HttpGet]
        [ActionName("GetTaskMemberForce")]
        public HttpResponseMessage GetTaskMemberForce()
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                CitizenCharterDB objtask = new CitizenCharterDB();
                viewModel.lstTaskMemberForce = objtask.GetTaskMemberForce();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }


   


        [HttpGet]
        [ActionName("GetEmployeeDetails")]
        public HttpResponseMessage GetEmployeeDetails([FromUri]Master_Employee employee)
        {
            EmployeeModel viewModel = new EmployeeModel();
            try
            {
                //entity.CaseID = [FromUri]Case request
                CitizenCharterDB objcomment = new CitizenCharterDB();
                viewModel.lstEmployee = objcomment.GetEmployeeDetails(employee.PKID_Employee);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, viewModel);
        }


        [HttpPost]
        [ActionName("AddTaskForceMember")]

        public CitizenCharterModel AddTaskForceMember(TaskMemberForce member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.AddTaskForceMember(member);
                viewModel.status = "Successfully added Task Force member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("AddCoreGroupMember")]

        public CitizenCharterModel AddCoreGroupMember(CoreGroupMembers member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.AddCoreGroupMember(member);
                viewModel.status = "Successfully added core group member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }



        [HttpPost]
        [ActionName("UpdateTaskForceMember")]

        public CitizenCharterModel UpdateTaskForceMember(TaskMemberForce member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.UpdateTaskForceMember(member);
                viewModel.status = "Successfully added Task Force member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("UpdateCoreGroupMember")]

        public CitizenCharterModel UpdateCoreGroupMember(CoreGroupMembers member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.UpdateCoreGroupMember(member);
                viewModel.status = "Successfully updated core group member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("DeleteCoreGroupMember")]

        public CitizenCharterModel DeleteCoreGroupMember([FromUri]CoreGroupMembers member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.DeleteCoreGroupMember(member.PKID_CoreGroup);
                viewModel.status = "Successfully deleted core group member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("DeleteTaskForceMember")]

        public CitizenCharterModel DeleteTaskForceMember([FromUri]TaskMemberForce member)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                objCitizenUser.DeleteTaskForceMember(member.PKID_TaskForce);
                viewModel.status = "Successfully deleted Task force member";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }


        [HttpPost]
        [ActionName("GetEmployeeByDepartment")]

        public MasterModel GetEmployeeByDepartment(Master_Employee employee)
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstEmployees = objCitizenUser.GetEmployeeByDepartment(employee.DepartmentId);
                viewModel.status = "Successfully fetch the employees.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        

        [HttpPost]
        [ActionName("GetInstituteNames")]

        public MasterModel GetInstituteNames(Master_Institute institute)
        {
            MasterModel viewModel = new MasterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstInstitute = objCitizenUser.GetInstituteNames(institute.Type);
                viewModel.status = "Successfully fetch the Institutes.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("GetCitizenCharterUsers")]
        public CitizenCharterModel GetCitizenCharterUsers(Master_Employee employee)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstCitizenCharterUsers = objCitizenUser.GetCitizenCharterUsers(employee.DepartmentId);
                viewModel.status = "Successfully fetch the employees.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }


        [HttpGet]
        [ActionName("GetCitizenCharterUsersRoleByUserId")]
        public CitizenCharterModel GetCitizenCharterUsersRoleByUserId([FromUri]Citizencharter user)
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstUserRoles = objCitizenUser.GetCitizenCharterUsersRoleByUserId(user.PKID_User);
                viewModel.status = "Successfully fetch the employees roles.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("GetCitizenRoles")]
        public CitizenCharterModel GetCitizenRoles()
        {
            CitizenCharterModel viewModel = new CitizenCharterModel();
            try
            {
                CitizenCharterDB objCitizenUser = new CitizenCharterDB();
                viewModel.lstUserRoles = objCitizenUser.GetCitizenRoles();
                viewModel.status = "Successfully fetch the employees roles.";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.Message;
            }
            return viewModel;
        }
    }
}
