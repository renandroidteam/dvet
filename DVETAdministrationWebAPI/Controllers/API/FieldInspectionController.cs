﻿using DataAccess.Master;
using DataModel.Master;
using DVETAdministrationWebAPI.APIModel;
using System;
using System.Web.Http;

namespace DVETAdministrationWebAPI.Controllers.API
{
    public class FieldInspectionController : ApiController
    {
        //Fetch all inspection names  or Audit tyles
        [HttpGet]
        [ActionName("GetEventNames")]
        public FIModel GetLegalModel([FromUri]FIInspectionOrAuditType entity)
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objFI = new FieldInspectionDB();
                viewModel.lstInspectionOrAuditTypes = objFI.GetInspectionOrAuditTypes(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
        
        [HttpPost]
        [ActionName("AddAnnexureC")]
        public FIModel AddCase(FI_AnnexureC requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                //objPay.status = onjPay.add(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }

            return objPay;
        }

        /*
        [HttpPost]
        [ActionName("AddRegisterDesk")]
        public FIModel Addnew(FIRegisterDesk requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                //objPay.status = onjPay.Insertregisterdesk(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }

            return objPay;

        }
        */

        [HttpPost]
        [ActionName("AddOrUpdateRegisterDesk")]
        public IHttpActionResult UpdateCase(FIRegisterDesk entity)
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objLegal = new FieldInspectionDB();
                viewModel.status = objLegal.AddOrUpdateRegisterdesk(entity);
                //viewModel.status = "Success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }

        [HttpPost]
        [ActionName("AddOrUpdateAudit")]
        public FIModel Addnew(FIInspectionOrAudit requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                objPay.status = onjPay.AddOrUpdateAudit(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }
            return objPay;
        }

        [HttpPost]
        [ActionName("AddOrUpdateInspection")]
        public FIModel AddOrUpdateInspection(FIInspectionOrAudit requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                objPay.status = onjPay.AddOrUpdateInspection(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }

            return objPay;
        }
/*
        //Update case details on AddNew-> Edit Case
        [HttpPost]
        [ActionName("UpdateInspection")]
        public IHttpActionResult UpdateCase(FIInspection entity)
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objLegal = new FieldInspectionDB();
                //viewModel.status = objLegal.UpdateInspection(entity);
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return Ok(viewModel);
        }
        */
        [HttpGet]
        [ActionName("GetEventType")]
        public FIModel GetEventType()
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objRegionType = new FieldInspectionDB();
                viewModel.lstEventTypes = objRegionType.GetEventType();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpGet]
        [ActionName("GetSearchDetails")]
        public FIModel GetSearchDetails([FromUri]FIInspectionOrAudit entity)
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objRegionType = new FieldInspectionDB();
                viewModel.lstSearchDetails = objRegionType.GetSearchDetails(entity);
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }

        [HttpPost]
        [ActionName("AddOrUpdateAuditParas")]
        public FIModel AddOrUpdateAuditParas(FIAuditParas requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                objPay.status = onjPay.AddOrUpdateAuditParas(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }

            return objPay;
        }

        [HttpPost]
        [ActionName("AddOrUpdateAuditParasDetails")]
        public FIModel AddOrUpdateAuditParasDetails(FIAuditParasDetails requestView)
        {
            FIModel objPay = new FIModel();
            try
            {
                FieldInspectionDB onjPay = new FieldInspectionDB();
                objPay.status = onjPay.AddOrUpdateAuditParasDetails(requestView);
                //objPay.status = "Success";
            }
            catch (Exception ex)
            {
                objPay.ErrorMessage = "Error";
                objPay.ErrorMessage = ex.Message;
            }

            return objPay;
        }

        [HttpGet]
        [ActionName("GetInspectionCommiteeData")]
        public FIModel GetInspectionCommiteeData()
        {
            FIModel viewModel = new FIModel();
            try
            {
                FieldInspectionDB objRegionType = new FieldInspectionDB();
                viewModel.lstInspectionCommiteeData = objRegionType.GetInspectionCommiteeData();
                viewModel.status = "success";
            }
            catch (Exception ex)
            {
                viewModel.status = "failure";
                viewModel.ErrorMessage = ex.InnerException.Message;
            }
            return viewModel;
        }
    }
}
