﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel.Master;


namespace DVETAdministrationWebAPI.Models
{
    public class StackHolderMaster
    {
        public StackHolderFeedback stackholderfeedback { get; set; }

        public IList<StackHolderAddQuestions> stackques { get; set; }

        public IList<StackHolderUserType> UserType { get; set; }

        public StackHolderMaster()
            {
            stackholderfeedback=new StackHolderFeedback();
            stackques = new List<StackHolderAddQuestions>();
            UserType = new List<StackHolderUserType>();

        }
    }
}