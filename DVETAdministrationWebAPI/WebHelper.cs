﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel.Master;

namespace DVETAdministrationWebAPI
{
    public class WebHelper
    {
        public class DVETAdministrationWebAPI
        {
            public static UserModel UserContext
            {
                get
                {
                    return (UserModel)HttpContext.Current.Session["UserContext"];

                }
                set
                {
                    HttpContext.Current.Session["UserContext"] = value;

                }
            }
        }
    }
}