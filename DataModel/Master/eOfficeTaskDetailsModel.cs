﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
   public  class eOfficeTaskDetailsModel
    {
        public string UniqueCode { get; set; }

        public string Subject { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime TaskDate { get; set; }
        public string CreatedBy { get; set; }
        public int TaskId { get; set; }

    }

    public class GRDetails
    {

        public string year { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DocumentType { get; set; }
        public string UniqueCode { get; set; }
        public string CreatedBy { get; set; }
        public string Document { get; set; }
        public string Subject { get; set; }
        public string Desk { get; set; }
        public int GRId { get; set; }
        public int RequestId { get; set; }
        public int ProcessId { get; set; }
        public int StateId { get; set; }
        public bool IsActive { get; set; }
        public int WorkFlowInstanceId { get; set; }
    }
}
