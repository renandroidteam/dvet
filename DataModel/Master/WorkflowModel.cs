﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
   public class WorkflowModel
    {
        public int WFInstanceID { get; set; }
        public int RequestId { get; set; }
        public string RequestNo { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowDCode { get; set; }
        public string CreatedBy { get; set; }
        public string LetterType { get; set; }
        public string LetterSubject { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DeskCreatedDate { get; set; }
        public string AssignedTo { get; set; }
    }
}
