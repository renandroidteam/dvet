﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class TaskResponseModel
    {

        public int WFInstanceID { get; set; }
        public int RequestID { get; set; }
        public string CreatedBy { get; set; }
        public string Subject { get; set; }
        public string CApplication { get; set; }
        public string WorkflowStatus { get; set; }
        public string CreatedDate { get; set; }
        public string SubWFCode { get; set; }
        public string WFCode { get; set; }
        public string AssignedTo { get; set; }
    }
}
