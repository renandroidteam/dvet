﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
   public class UserAuthentication
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordPhrase { get; set; }
        public bool IsActive { get; set; }
        public DateTime PasswordChangedOn { get; set; }
        public string ActualUserName { get; set; }
        public string EmailId { get; set; }
        public string MobileNumber { get; set; }
        public int UserRoleId { get; set; }

        public string InstituteName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}
