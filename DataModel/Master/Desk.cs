﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class Desk
    {
        public int DeskId { get; set; }

        public string DeskName { get; set; }
    }
}
