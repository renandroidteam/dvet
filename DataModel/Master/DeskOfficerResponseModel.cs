﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class DeskOfficerResponseModel
    {
        public string AssignedTo { get; set; }
        public int CaseId { get; set; }
        public string CaseNo { get; set; }
        public DateTime SetDueDate { get; set; }
        public string Remarks { get; set; }
        public string Priority { get; set; }
        public string Comments { get; set; }
        public string DocumentPath { get; set; }
    }
}
