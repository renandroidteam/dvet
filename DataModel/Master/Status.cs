﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class Status
    {
        public int Id { get; set; }

        public string StatusCode { get; set; }

        public string StatusDesc { get; set; }

        public bool IsActive { get; set; }
    }
}
