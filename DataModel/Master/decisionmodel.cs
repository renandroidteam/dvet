﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class decisionmodel
    {
        public List<Decision_CaseDetails> model { get; set; }

        public decisionmodel()
        {
            model = new List<Decision_CaseDetails>();
        }
    }
}
