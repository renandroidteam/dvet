﻿using System;

namespace DataModel.Master
{
    public class Citizencharter
    {

        public int PKID_User { get; set; }
        public int FKID_InstituteType { get; set; }

        public int FKID_InstituteName { get; set; }
        public int FKID_Department { get; set; }
        public int FKID_Employee { get; set; }
        public string DOB { get; set; }
        public int Gender { get; set; }
        public string CurrentPositionLocation { get; set; }

        public int CurrentDepartment { get; set; }
        public string AccessValidTill { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class CitizenCharter
    {
        public int PKID_CC { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Quater { get; set; }
        public string Region { get; set; }
        public string CreatedBy { get; set; }

    }

    public class CitizenCommentsModel
    {
        public int PKID_Comment { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int DocumentId { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }

        public string Captcha { get; set; }
        public string Comment { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }




    public class CoreGroupMembers
    {
        public int PKID_CoreGroup { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string EmailAddress { get; set; }
        public bool Action { get; set; }
        public string CCName { get; set; }
        public string DepartmentName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

    }

    public class TaskMemberForce
    {
        public int PKID_TaskForce { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string EmailAddress { get; set; }
        public bool Action { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedBy { get; set; }
        public string CCName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

    }

    public class CitizenComment
    {
        public int PKID_Comment { get; set; }
        public string CreatedDate { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Comment { get; set; }
        public string Type { get; set; }
    }


    public class CitizenCharter_UserRole
    {
        public int PKID_Role { get; set; }
        public string RoleName { get; set; }
        public bool Active { get; set; }
    }
}
