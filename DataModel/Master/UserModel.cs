﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
   public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmailID { get; set; }
        public string Contact { get; set; }
        public string Password { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string CountryName { get; set; }
        //public string CountryCode { get; set; }
        public Nullable<int> UserTypeId { get; set; }
        public string CountryCode { get; set; }
        public string SocialType { get; set; }
        public string SocialId { get; set; }
        public string QRCode { get; set; }
        public String UserType { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public byte[] ProfileImage { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsOTPVarified { get; set; }
        public int RoleId { get; set; }
        public string RolesName { get; set; }
        public int MapId { get; set; }

    }
}
