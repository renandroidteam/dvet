﻿using System;

namespace DataModel.Master
{
    public class DecisionCaseModel
    {
        public int PKID_Case { get; set; }
        public string CaseId { get; set; }
        public string Subject { get; set; }
        public DateTime AddDate { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string ApprovalRequired { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int FKID_StatusId { get; set; }
        public string CreatedBy { get; set; }
        public string Emailid { get; set; }
        public string ModifiedBy { get; set; }


    }

    public class Decision_CaseFlowModel
    {
        public int FKID_Case { get; set; }

        public string CaseID { get; set; }
        public int DecisionCaseID { get; set; }
        public string ForwardTo { get; set; }
        public string ApprovalRequired { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string EmailAddress { get; set; }
        public string Department { get; set; }
        public string SendBackRemarks { get; set; }
        public string HoldRemarks { get; set; }
        public int PKID_CaseFlow { get; set; }
        public int FKID_StatusId { get; set; }
        public string CaseType { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class DecisionSearchCase
    {
        public int Status { get; set; }
        public int CaseId { get; set; }
        public string Subject { get; set; }
        public DateTime Date { get; set; }
    }

    public class Decision_CaseDetails
    {
        public int CaseId { get; set; }

        public int StageId { get; set; }

        public string CourtDecision { get; set; }

        public string PreviousDecisions { get; set; }

        public string DepartmentSuggestion { get; set; }

        public string DecisionImplication { get; set; }

        public string GRNumbers { get; set; }

        public string WebsiteName { get; set; }

        public string WebsiteURL { get; set; }



    }

    public class CaseOfficerDetails
    {

        public string Name { get; set; }

        public string Designation { get; set; }

        public string Email { get; set; }

        public string Department { get; set; }

        public string InstituteName { get; set; }

        public string InstituteEmail { get; set; }


    }

   
}
