﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class TaskRequestModel
    {

        public int WFInstanceId { get; set; }
        public string LoggedInUser { get; set; }
        public string StatusCode { get; set; }
    }
}
