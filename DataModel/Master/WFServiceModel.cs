﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class WFServiceModel
    {
        public int WorkflowInstanceId { get; set; }
        public int RequestID { get; set; }
        public string Comments { get; set; }
        public string CreatedBY { get; set; }
        public string ActionCode { get; set; }
        public int ProcessID { get; set; }
        public string WFCode { get; set; }
        public int StateID { get; set; }
        public string AssignedTo { get; set; }
        public string Description { get; set; }
        public int UTID { get; set; }
        public int PIPID { get; set; }
        public string Component { get; set; }
        public string PullType { get; set; }
        public string RoleName { get; set; }
        public int ParentTaskId { get; set; }
        public string ParentEmail { get; set; }
    }

    public class WFCommentsModel
    {
        public string AssignedTo { get; set; }
        public string Comments { get; set; }
        public string CreatedDate { get; set; }
    }

    public class WFinitiationModel
    {

        public string WFCode { get; set; }
        public string WFSubCode { get; set; }
        public int RequestId { get; set; }
        public string CreatedBy { get; set; }
        public string RequestNo { get; set; }
    }

    public class WorkflowInstanceDetailsModel
    {
        public int WFInstanceID { get; set; }
        public int RequestID { get; set; }
        public string AssignedTo { get; set; }
        public string WFStatus { get; set; }
        public string CreatedDate { get; set; }
        public string Comments { get; set; }
        public int WorkFlowId { get; set; }
        public int ProcessId { get; set; }
        public int ActionId { get; set; }
        public int TransitionId { get; set; }
        public int StateId { get; set; }
        public string CreatedBy { get; set; }
        public int WDId { get; set; }
        public string RequestNo { get; set; }
        public string WFCode { get; set; }
    }
}
