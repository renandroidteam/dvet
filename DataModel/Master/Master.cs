﻿using System;
namespace DataModel.Master
{
    public class Master_CourtType
    {
        public int PKID_CourtType { get; set; }
        public string CourtType { get; set; }
    }

    public class Master_AidType
    {
        public int PKID_AidType { get; set; }
        public string AidType { get; set; }
    }

    public class Master_CaseType
    {
        public int PKID_CaseType { get; set; }
        public string CaseType { get; set; }
    }

    public class Master_Region
    {
        public int PKID_Region { get; set; }
        public string RegionName { get; set; }
    }

    public class Master_CaseClassification
    {
        public int PKID_CaseClassification { get; set; }
        public string CaseClassificationName { get; set; }
    }

    public class Master_LegalStatus
    {
        public int PKID_LegalStatus { get; set; }
        public string LegalStatusName { get; set; }
    }

    public class Master_ParticipantType
    {
        public int PKID_ParticipantType { get; set; }
        public string ParticipantType { get; set; }
    }

    public class Master_Institute
    {
        public int FKID_InstituteType { get; set; }
        public int PKID_InstituteName { get; set; }
        public string InstituteName { get; set; }
        public string Type { get; set; }
    }

    public class Master_InstituteType
    {
        public int PKID_InstituteType { get; set; }
        public string InstituteType { get; set; }
    }

    public class Master_Department
    {
        public int PKID_Department { get; set; }
        public string DepartmentName { get; set; }
    }

    public class Master_Citizen_UserRoles
    {
        public int PKID_Role { get; set; }
        public string RoleName { get; set; }
    }


    public class Master_Employee
    {
        public int PKID_Employee { get; set; }
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string EmailId { get; set; }
        public string Designation { get; set; }
        public string Grade { get; set; }
        public int DepartmentId { get; set; }
        public string ManagerEmployeeId { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Position { get; set; } 
        public string DOB { get; set; }
        public string Posting { get; set; }
        public string Mobilenum { get; set; }
        public string Decision { get; set; }
      //  public string EmployeeName { get; set; }

    }
    public class SMSService
    {
        public string mobilenumber { get; set; }

    }
}
