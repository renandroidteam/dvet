﻿using System;
using System.Collections.Generic;

namespace DataModel.Master
{
    public class DashboardCaseCount
    {
        public int ClosedCaseCount { get; set; }
        public int PendingCaseCount { get; set; }
        public int AllCaseCount { get; set; }

        public int HOCountThisWeek { get; set; }
        public int ROCountThisWeek { get; set; }

        public int HOCountThisMonth { get; set; }
        public int ROCountThisMonth { get; set; }

        public int HOCountNextMonth { get; set; }
        public int ROCountNextMonth { get; set; }

        public int HOCountUpdatePending { get; set; }
        public int ROCountUpdatePending { get; set; }

        public int TotalHOCount { get; set; }
        public int TotalROCount { get; set; }

        public int HOCountPending { get; set; }
        public int ROCountPending { get; set; }

        public int HOCountThisYear { get; set; }
        public int ROCountThisYear { get; set; }

        public int HOCountClosedThisYear { get; set; }
        public int ROCountClosedThisYear { get; set; }

        public int count { get; set; }
        public string Scheduled_Hearing { get; set; }
    }

    public class CaseDetails
    {
        public int PKID_Case { get; set; }
        public string CaseID { get; set; }
        public string CourtType { get; set; }
        public string CaseNoInCourt { get; set; }
        public string Region { get; set; }
        public string AidType { get; set; }
        public int CaseClassification { get; set; }
        public string CaseType { get; set; }
        public string OldCaseId { get; set; }
        public string Subject { get; set; }
        public string CaseAgainst { get; set; }
        public int Status { get; set; }
        public string CaseSummary { get; set; }
        public string CaseDate { get; set; }
        public string CaseClosingDate { get; set; }
        public string HearingNumber { get; set; }
        public string HearingDate { get; set; }
        public string ApplicantName { get; set; }
        public string RespondantName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string AuthType { get; set; }
        public string AuthUser { get; set; }
        public string caseclassi { get; set; }
        public string Stat { get; set; }
    }

    public class CaseComment
    {
        public int PKID_Comment { get; set; }
        public int CasePKID { get; set; }
        public string CaseID { get; set; }
        public string CreatedBy { get; set; }
        public string Comment { get; set; }
        public string CreatedDate { get; set; }
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Designation { get; set; }
    }

    public class RecentCases
    {
        public int PKID_Case { get; set; }
        public string CaseID { get; set; }
        public string CaseAgainst { get; set; }
    }

    public class CaseHistory
    {
        public int PKID_History { get; set; }
        public string CaseID { get; set; }
        public string UserName { get; set; }
        public string Change { get; set; }
        public string ModifiedDate { get; set; }
        public string Details { get; set; }
        public string Details1 { get; set; }
        public string Details2 { get; set; }
    }

    public class CaseParticipants
    {
        public int PKID_Participant { get; set; }
        public int CasePKID { get; set; }
        public string CaseId { get; set; }
        public string Name { get; set; }
        public int ParticipantType { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string HearingNumber { get; set; }
        public string HearingDateString { get; set; }
        public DateTime hearingDate { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string userDisplayName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string HearingInvitedCount { get; set; }
        public string HearingParticipatedCount { get; set; }
        public string HearingInvitedParticipants { get; set; }
        public string HearingAttendedParticipats { get; set; }
        public string HearingNotes { get; set; }
        public string HearingLocation { get; set; }
        public string HearingCourt { get; set; }
        public bool HearingAttended { get; set; }

    }


    public class CaseParticipantByEmail
    {
        public int PKID_Participant { get; set; }
        public int CasePKID { get; set; }
        public string CaseId { get; set; }
        public string Name { get; set; }
        public int ParticipantType { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string HearingNumber { get; set; }
        public string HearingDate { get; set; }
        public string Remarks { get; set; }
    }

    public class CaseHearing
    {
        public int Type { get; set; }
        public string CaseId { get; set; }
        public int ParticipantsId { get; set; }
        public string HearingNumber { get; set; }
        public DateTime HearingDate { get; set; }
        public string HearingNotes { get; set; }
        public string ParticipantsInvited { get; set; }
        public string ParticipantsAttended { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Email { get; set; }
        public bool HearingAttended { get; set; }
    }
    public class AuthEmailId
    {
        public string EmailId { get; set; }
        public string Position { get; set; }
        public string CreatedBy { get; set; }
    }

    public class Report
    {

        public string CourtType { get; set; }
        public string CaseNoInCourt { get; set; }
        public string Matter { get; set; }
        public string Affidavit { get; set; }
        public string Interim { get; set; }
        public string NextDate { get; set; }
        public string Officerattendinghearing { get; set; }
        public string Remarks { get; set; }
        public string ApplicantName { get; set; }
        public string RespondantName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Region { get; set; }
        public string AidType { get; set; }
        public string CaseType { get; set; }
        public string AuthType { get; set; }
        public string AuthUser { get; set; }
        public string Year { get; set; }
        public string status { get; set; }
        public string Subject { get; set; }

    }
    public class CaseApplicantsAndRespondents
    {
        public int PKID_ApplicantAndRespondent { get; set; }
        public int CasePKID { get; set; }
        public string Name { get; set; }
        public bool ApplicantType { get; set; }
        public string ApplicantOrRespondant { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsActive { get; set; }
    }
}