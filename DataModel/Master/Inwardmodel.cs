﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class Inwardmodel
    {

        public int PKID_LetterType { get; set; }
        public int Id { get; set; }
        public string CaseNumber { get; set; }
        public int LetterType { get; set; }
        public string Letter_Type { get; set; }
        public int NoOfPages { get; set; }
        public string LetterRefNumber { get; set; }
        public string SenderName { get; set; }
        public string Subject { get; set; }
        public string Recivedthrough { get; set; }
        public int DeliverRecievedMode { get; set; }
        public DateTime RecievedDate { get; set; }
        public string Decision { get; set; }
        public int AttachedDocument { get; set; }
        public int DeskId { get; set; }
        public int CentralGovernmentId { get; set; }
        public string Department { get; set; }
        public string AssignedFrom { get; set; }
        public string AssignedTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LetterTypedesc { get; set; }
        public string CreatedLocation { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string FileNumber { get; set; }
        public int ForwardedTo { get; set; }
        public string Document { get; set; }

    }


    public class SearchModel
    {
        public int Id { get; set; }
        public string Case_id { get; set; }
        public DateTime Case_Created_Date { get; set; }
        public string Letter_Type { get; set; }
        public string Letter_Subject { get; set; }
        public string Status { get; set; }

    }


    public class Addfiles
    {
        public int Id { get; set; }
        public int FIId { get; set; }
        public int CaseId { get; set; }
        public string CaseNo { get; set; }
        public string FileNumber { get; set; }
        public string AttachedDocument { get; set; }
        public string FileName { get; set; }
        public string FileSubject { get; set; }
        public int FileCategory { get; set; }
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedLocation { get; set; }
    }


    public class AddfileNotes
    {
        public int Id { get; set; }
        public int FId { get; set; }
        public string FileNo { get; set; }
        public string CaseId { get; set; }
        public int CaseNo { get; set; }
        public string NoteNumber { get; set; }
        public string FileName { get; set; }
        public string Subject { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

    }


    public class AddFileDetails
    {
        public int Id { get; set; }
        public int FileNumber { get; set; }
        public string FileNo { get; set; }
        public string FileName { get; set; }
        public string FileSubject { get; set; }
        public string FileCategory { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedLocation { get; set; }
        public int CaseId { get; set; }

    }



    public class AddFileCaseMapping
    {

        public string CreatedBy { get; set; }

        public int FIId { get; set; }
        public int CaseId { get; set; }

    }
    public class LetterType
    {
        public int LetterTypeId { get; set; }
        public string LetterTypeName { get; set; }

    }

    public class DeskOfficer
    {
        public int PKID_CentralGovtOffice { get; set; }
        public string OfficeName { get; set; }
    }



    public class NoteStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }

    public class NoteStatusDecision
    {
        public int Id { get; set; }
        public int NoteId { get; set; }
        public int StatusId { get; set; }
        public int CaseNo { get; set; }
        public string Remarks { get; set; }
    }
}
