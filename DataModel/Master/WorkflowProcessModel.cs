﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
  public  class WorkflowProcessModel
    {
        public int WorkflowInstanceId { get; set; }
        public int RequestID { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public string ActionCode { get; set; }
        public int ActionId { get; set; }
       public string Remarks { get; set; }
        public int ProcessId { get; set; }
        public string WFCode { get; set; }
        public string WFDCode { get; set; }
        public int StateId { get; set; }
        public int CaseId { get; set; }
        public string CaseNumber { get; set; }

    }
}
