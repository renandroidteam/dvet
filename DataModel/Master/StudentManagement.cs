﻿namespace DataModel.Master
{
    public class StudentProfileDetails
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public string BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string FatherOccupation { get; set; }
        public string FatherAnnualIncome { get; set; }
        public string MotherOccupation { get; set; }
        public string MotherAnnualIncome { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string EmailId { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string MotherTongue { get; set; }
        public string Religion { get; set; }
        public string CasteCategory { get; set; }
        public string Caste { get; set; }
        public string NonCreamyLayer { get; set; }
        public string State { get; set; }
        public string Scolorship { get; set; }
        public string ScolorshipConcession { get; set; }
        public string ScolorshipDetails { get; set; }
        public string PlacementThrough { get; set; }
        public string TypeOfPlacement { get; set; }
        public string OrganizationName { get; set; }
        public int YearOfPlacement { get; set; }

        public int Type { get; set; }
    }

    public class StudentSemesterAttendance
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string SemesterAttendance { get; set; }
        public string AttendancePecentage { get; set; }
        public string LeaveDate { get; set; }
    }

    public class StudentSemesterLeave
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string SemesterLeave { get; set; }
        public string LeaveDate { get; set; }
    }

    public class StudentSemesterResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string ResultSemester { get; set; }
        public string ResultDate { get; set; }
        public string Result { get; set; }
        public string Reappear { get; set; }
    }

    public class StudentPenalty
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string PenaltyReason { get; set; }
        public string PenaltyDate { get; set; }
        public decimal PenaltyAmount { get; set; }
        public string PenaltyStatus { get; set; }
        public string PenaltyUpdatedDate { get; set; }
    }


    public class ScholorshipDetails
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string Details { get; set; }
        public string Scholorship { get; set; }
        public string IsScholorshipOrConcession { get; set; }
      

    }


    public class PlacementDetails
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string NameOfInstitute { get; set; }
        public int BatchYear { get; set; }
        public string TradeOrCourse { get; set; }
        public string OrganizationName { get; set; }
        public string PlacementThrough { get; set; }
        public string TypeOfPlacement { get; set; }
        public string YearOfPlacement { get; set; }

       
    }
}
