﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Master
{
    public class StackHolderAddQuestions
    {
        public string Questions { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public int QuestionId { get; set; }
        public int SurveyId { get; set; }
       public string SurveyName { get; set; }
    }

    public class StackHolderFeedback
    {
        public int PKID_SH { get; set; }
        public string ApplicantName { get; set; }
        public string UserType { get; set; }
        public string ReferenceNo { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string CreatedBy { get; set; }
    }

    public class StackHolderUserType
    {
        public int PKID_UT { get; set; }
        public string UserType { get; set; }
    }
    public class StackHolderSurvey
    {
        public string SurveyName { get; set; }
        public int SurveyId { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Remarks { get; set; }
    }


    public class StackHolderFeedbackDetails
    {
        public string QuestionName { get; set; }
        public string SurveyName { get; set; }
        public int SurveyId { get; set; }
        public int QuetionId { get; set; }
        public int Excellent { get; set; }
        public int VeryGood { get; set; }
        public int Satisfactory { get; set; }
        public int Average { get; set; }
        public int Poor { get; set; }
    }

    public class StackHolderFeedBackSurvey
    {
        public string SurveyName { get; set; }
        public int SurveyId { get; set; }
       
        public string Remarks { get; set; }
    }


    public class StackHoldersrespondantDetails
    {
        public string RespondentName { get; set; }
        public int SurveyId { get; set; }
        public int QuestionId { get; set; }
        public string Remarks { get; set; }
        public string QuestionName { get; set; }
        public string SurveyName { get; set; }
        public int QuestionType { get; set; }
    }
}
