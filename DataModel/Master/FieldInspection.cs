﻿using System;

namespace DataModel.Master
{

    public class FIRegisterDesk
    {
        public int Type { get; set; }
        public string RegisterDeskId { get; set; }
        public string LetterReferencenumber { get; set; }
        public string SenderName { get; set; }
        public string Desknumber { get; set; }
        public string SpeedpostNumber { get; set; }
        public string Subject { get; set; }
        public string Receipent { get; set; }
        public string OutwardData { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FI_AnnexureC
    {
        public int PKID_AnnexureC { get; set; }
        public string NameOfITI { get; set; }
        public string NameOfTrade { get; set; }
        public int SpacePerUnitPerShift { get; set; }
        public int PowerPerUnitPerShift { get; set; }
        public string Remarks { get; set; }
        public string RemarksByITI { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FIInspectionOrAuditType
    {
        public int PKID_InspectionOrAuditType { get; set; }
        public string InspectionOrAuditTypeName { get; set; }
        public bool IsAuditType { get; set; }
        public bool Active { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FI_EventType
    {
        public int PKID_EventType { get; set; }
        public string EventTypeName { get; set; }

        public string status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FIInspectionOrAudit
    {
        public int Type { get; set; }
        public string EventType { get; set; }
        public int InspectionOrAuditType { get; set; }
        public string InspectionNameOrAuditBy { get; set; }
        public string InspectionOrAuditId { get; set; }
        public string InspectionOrAuditFromDate { get; set; }
        public string InspectionOrAuditToDate { get; set; }
        public int PKID_InspectionOrAudit { get; set; }
        public string InspectionNameOrAuditName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class FIAuditParas
    {
        public int Type { get; set; }
        public int PKID_AuditParas { get; set; }
        public string AuditType { get; set; }
        public string AuditParasId { get; set; }
        public int AuditBy { get; set; }
        public string AuditFromDate { get; set; }
        public string AuditToDate { get; set; }
        public string Decision { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class FIAuditParasDetails
    {
        public int Type { get; set; }
        public int PKID_AuditParasDetail { get; set; }
        public string AuditParaId { get; set; }
        public string InstituteName { get; set; }
        public string InstituteInchargeIn { get; set; }
        public string InstituteInchargeEmail { get; set; }
        public string AuditParaHeading { get; set; }
        public string AuditParaDetails { get; set; }
        public string InstitueJustification { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class FIInspectionCommiteeData
    {
        public int AnnexureId { get; set; }
        public string AnnexureName { get; set; }
        public string status { get; set; }
    }
}
