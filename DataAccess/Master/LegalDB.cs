﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;


namespace DataAccess.Master
{
    public class LegalDB
    {
        public IList<DashboardCaseCount> GetDashboardData()
        {
            IList<DashboardCaseCount> lstentity = new List<DashboardCaseCount>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetDashboardData]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DashboardCaseCount entitymodel = new DashboardCaseCount();

                            entitymodel.ClosedCaseCount = Convert.ToInt16(reader["ClosedCaseCount"]);
                            entitymodel.PendingCaseCount = Convert.ToInt16(reader["PendingCaseCount"]);
                            entitymodel.AllCaseCount = Convert.ToInt16(reader["AllCaseCount"]);

                            entitymodel.HOCountThisWeek = Convert.ToInt16(reader["HOCountThisWeek"]);
                            entitymodel.ROCountThisWeek = Convert.ToInt16(reader["ROCountThisWeek"]);

                            entitymodel.HOCountThisMonth = Convert.ToInt16(reader["HOCountThisMonth"]);
                            entitymodel.HOCountThisMonth = Convert.ToInt16(reader["ROCountThisMonth"]);

                            entitymodel.HOCountNextMonth = Convert.ToInt16(reader["HOCountNextMonth"]);
                            entitymodel.ROCountNextMonth = Convert.ToInt16(reader["ROCountNextMonth"]);

                            entitymodel.HOCountUpdatePending = Convert.ToInt16(reader["HOCountUpdatePending"]);
                            entitymodel.ROCountUpdatePending = Convert.ToInt16(reader["ROCountUpdatePending"]);

                            entitymodel.TotalHOCount = Convert.ToInt16(reader["TotalHOCount"]);
                            entitymodel.TotalROCount = Convert.ToInt16(reader["TotalROCount"]);

                            entitymodel.HOCountPending = Convert.ToInt16(reader["HOCountPending"]);
                            entitymodel.ROCountPending = Convert.ToInt16(reader["ROCountPending"]);

                            entitymodel.HOCountThisYear = Convert.ToInt16(reader["HOCountThisYear"]);
                            entitymodel.ROCountThisYear = Convert.ToInt16(reader["ROCountThisYear"]);

                            entitymodel.HOCountClosedThisYear = Convert.ToInt16(reader["HOCountClosedThisYear"]);
                            entitymodel.ROCountClosedThisYear = Convert.ToInt16(reader["ROCountClosedThisYear"]);

                            lstentity.Add(entitymodel);
                        }
                        return lstentity;
                    }
                }
            }
        }


        public IList<DashboardCaseCount> GetDashboard(string EmailId,string CreatedBy)
        {
            IList<DashboardCaseCount> lstentity = new List<DashboardCaseCount>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Usp_GetUserWiseDashBoard";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LoginEmail", EmailId);
                  //  cmd.Parameters.AddWithValue("@LoginEmail", CreatedBy);
                    using (SqlDataReader reader = cmd.ExecuteReader())


                    {
                        while (reader.Read())
                        {
                            DashboardCaseCount entitymodel = new DashboardCaseCount();

                            entitymodel.ClosedCaseCount = Convert.ToInt16(reader["TotalClosed"]);
                            entitymodel.PendingCaseCount = Convert.ToInt16(reader["TotalPending"]);
                            entitymodel.AllCaseCount = Convert.ToInt16(reader["TotalCase"]);
                            lstentity.Add(entitymodel);
                        }
                        return lstentity;
                    }
                }
            }
        }
        public IList<DashboardCaseCount> GetHearingDashboard(string EmailId, string CreatedBy)
        {
            IList<DashboardCaseCount> lstentity = new List<DashboardCaseCount>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Usp_RptRegionWiseHearing";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LoggedInEmail", EmailId);
                    //  cmd.Parameters.AddWithValue("@LoginEmail", CreatedBy);
                    using (SqlDataReader reader = cmd.ExecuteReader())


                    {
                        while (reader.Read())
                        {
                            DashboardCaseCount entitymodel = new DashboardCaseCount();

                            entitymodel.count = Convert.ToInt16(reader["count"]);
                            entitymodel.Scheduled_Hearing = Convert.ToString(reader["Scheduled_Hearing"]);
                           
                           
                            lstentity.Add(entitymodel);
                        }
                        return lstentity;
                    }
                }
            }
        }

        public IList<CaseDetails> GetCaseData(CaseDetails obj)
        {
            IList<CaseDetails> lstCaseDetails = new List<CaseDetails>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseData]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseId", obj.CaseID);
                    cmd.Parameters.AddWithValue("@CourtType", obj.CourtType);
                    cmd.Parameters.AddWithValue("@CaseNoInCourt", obj.CaseNoInCourt);
                    cmd.Parameters.AddWithValue("@Region", obj.Region);
                    cmd.Parameters.AddWithValue("@AidType", obj.AidType);
                    cmd.Parameters.AddWithValue("@CaseClassification", obj.CaseClassification);
                    cmd.Parameters.AddWithValue("@CaseType", obj.CaseType);
                    cmd.Parameters.AddWithValue("@OldCaseId", obj.OldCaseId);
                    cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                    cmd.Parameters.AddWithValue("@CaseAgainst", obj.CaseAgainst);
                    cmd.Parameters.AddWithValue("@Status", obj.Status);
                    cmd.Parameters.AddWithValue("@CaseSummary", obj.CaseSummary);
                    cmd.Parameters.AddWithValue("@CaseDate", obj.CaseDate);
                    cmd.Parameters.AddWithValue("@CaseCLosingDate", obj.CaseClosingDate);
                    cmd.Parameters.AddWithValue("@ApplicantName", obj.ApplicantName);
                    cmd.Parameters.AddWithValue("@RespondantName", obj.RespondantName);
                   // cmd.Parameters.AddWithValue("@AuthType", obj.AuthType);
                    //cmd.Parameters.AddWithValue("@AuthUser", obj.AuthUser);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseDetails entitymodel = new CaseDetails();

                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.CaseID = Convert.ToString(reader["CaseID"]);
                            entitymodel.CourtType = Convert.ToString(reader["CourtType"]);
                            entitymodel.CaseNoInCourt = reader["CaseNoInCourt"] == DBNull.Value ? "" : Convert.ToString(reader["CaseNoInCourt"]);
                            entitymodel.Region = reader["Region"] == DBNull.Value ? "" : Convert.ToString(reader["Region"]);
                            entitymodel.AidType = reader["AidType"] == DBNull.Value ? "" : Convert.ToString(reader["AidType"]);
                            entitymodel.CaseSummary = reader["CaseSummary"] == DBNull.Value ? "" : Convert.ToString(reader["CaseSummary"]);
                            entitymodel.CaseClassification = reader["CaseClassification"] == DBNull.Value ? 0 : Convert.ToInt16(reader["CaseClassification"]);
                            entitymodel.CaseType = reader["CaseType"] == DBNull.Value ? "" : Convert.ToString(reader["CaseType"]);
                            entitymodel.OldCaseId = reader["OldCaseId"] == DBNull.Value ? "" : Convert.ToString(reader["OldCaseId"]);
                            entitymodel.Subject = reader["Subject"] == DBNull.Value ? "" : Convert.ToString(reader["Subject"]);
                            entitymodel.CaseAgainst = reader["CaseAgainst"] == DBNull.Value ? "" : Convert.ToString(reader["CaseAgainst"]);
                            entitymodel.Status = reader["Status"] == DBNull.Value ? 0 : Convert.ToInt16(reader["Status"]);
                            entitymodel.CaseDate = reader["CaseDate"] == DBNull.Value ? "" : Convert.ToString(reader["CaseDate"]);
                            entitymodel.HearingNumber = reader["HearingNumber"] == DBNull.Value ? "" : Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDate = reader["NextHearingDate"] == DBNull.Value ? "" : Convert.ToString(reader["NextHearingDate"]);
                            entitymodel.ApplicantName = reader["ApplicantName"] == DBNull.Value ? "" : Convert.ToString(reader["ApplicantName"]);
                            entitymodel.RespondantName = reader["RespondantName"] == DBNull.Value ? "" : Convert.ToString(reader["RespondantName"]);
                            entitymodel.CreatedBy = reader["CreatedBy"] == DBNull.Value ? "" : Convert.ToString(reader["CreatedBy"]);
                            entitymodel.AuthType = reader["AuthType"] == DBNull.Value ? "" : Convert.ToString(reader["AuthType"]);
                            entitymodel.AuthUser = reader["AuthUser"] == DBNull.Value ? "" : Convert.ToString(reader["AuthUser"]);

                            lstCaseDetails.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseDetails;
            }
        }
        public IList<CaseDetails> SearchCaseData(CaseDetails obj)
        {
            IList<CaseDetails> lstCaseDetails = new List<CaseDetails>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_SearchData]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseId", obj.CaseID);
                    cmd.Parameters.AddWithValue("@CourtType", obj.CourtType);
                    cmd.Parameters.AddWithValue("@CaseNoInCourt", obj.CaseNoInCourt);
                    cmd.Parameters.AddWithValue("@Region", obj.Region);
                    cmd.Parameters.AddWithValue("@AidType", obj.AidType);
                    cmd.Parameters.AddWithValue("@CaseClassification", obj.caseclassi);
                    cmd.Parameters.AddWithValue("@CaseType", obj.CaseType);
                    cmd.Parameters.AddWithValue("@OldCaseId", obj.OldCaseId);
                    cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                    cmd.Parameters.AddWithValue("@CaseAgainst", obj.CaseAgainst);
                    cmd.Parameters.AddWithValue("@Status", obj.Stat);
                    cmd.Parameters.AddWithValue("@CaseSummary", obj.CaseSummary);
                    cmd.Parameters.AddWithValue("@CaseDate", obj.CaseDate);
                    cmd.Parameters.AddWithValue("@CaseCLosingDate", obj.CaseClosingDate);
                    cmd.Parameters.AddWithValue("@ApplicantName", obj.ApplicantName);
                    cmd.Parameters.AddWithValue("@RespondantName", obj.RespondantName);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseDetails entitymodel = new CaseDetails();

                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.CaseID = Convert.ToString(reader["CaseID"]);
                            entitymodel.CourtType = Convert.ToString(reader["CourtType"]);
                            entitymodel.CaseNoInCourt = Convert.ToString(reader["CaseNoInCourt"]);
                            entitymodel.Region = Convert.ToString(reader["Region"]);
                            entitymodel.AidType = Convert.ToString(reader["AidType"]);
                            entitymodel.CaseSummary = Convert.ToString(reader["CaseSummary"]);
                            entitymodel.CaseClassification = Convert.ToInt16(reader["CaseClassification"]);
                            entitymodel.CaseType = Convert.ToString(reader["CaseType"]);
                            entitymodel.OldCaseId = Convert.ToString(reader["OldCaseId"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.CaseAgainst = Convert.ToString(reader["CaseAgainst"]);
                            entitymodel.Stat = Convert.ToString(reader["Status"]);
                            entitymodel.CaseDate = Convert.ToString(reader["CaseDate"]);
                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDate = Convert.ToString(reader["NextHearingDate"]);
                            entitymodel.ApplicantName = Convert.ToString(reader["ApplicantName"]);
                            entitymodel.RespondantName = Convert.ToString(reader["RespondantName"]);
                            entitymodel.AuthType = Convert.ToString(reader["AuthType"]);
                            entitymodel.AuthUser = Convert.ToString(reader["AuthUser"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);

                            lstCaseDetails.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseDetails;
            }
        }


        public IList<Report> Report(Report obj)
        {
            IList<Report> lstCaseDetails = new List<Report>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_Report]";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Region", obj.Region);
                    cmd.Parameters.AddWithValue("@AidType", obj.AidType);
                    cmd.Parameters.AddWithValue("@CaseType", obj.CaseType);
                    cmd.Parameters.AddWithValue("@Status", obj.status);
                    cmd.Parameters.AddWithValue("@Year", obj.Year);
                    cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Report entitymodel = new Report();

                            entitymodel.CourtType = Convert.ToString(reader["CourtType"]);
                            entitymodel.CaseNoInCourt = Convert.ToString(reader["CaseNoInCourt"]);
                            entitymodel.Region = Convert.ToString(reader["Region"]);
                            entitymodel.AidType = Convert.ToString(reader["AidType"]);
                            entitymodel.Matter = Convert.ToString(reader["Matter"]);
                            entitymodel.Affidavit = Convert.ToString(reader["Affidavit"]);
                            entitymodel.CaseType = Convert.ToString(reader["CaseType"]);
                            entitymodel.Interim = Convert.ToString(reader["Interim"]);
                            entitymodel.NextDate = Convert.ToString(reader["NextDate"]);
                            entitymodel.Officerattendinghearing = Convert.ToString(reader["OfficerAttendingHearing"]);
                            entitymodel.Remarks = Convert.ToString(reader["Remarks"]);
                            entitymodel.ApplicantName = Convert.ToString(reader["ApplicantName"]);
                            entitymodel.RespondantName = Convert.ToString(reader["RespondantName"]);
                            entitymodel.AuthType = Convert.ToString(reader["AuthType"]);
                            entitymodel.AuthUser = Convert.ToString(reader["AuthUser"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);

                            lstCaseDetails.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseDetails;
            }
        }


        public string AddCase(CaseDetails obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetCaseData]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewCaseId = new SqlParameter("@NewCaseId", SqlDbType.NVarChar, 50);
                        NewCaseId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewCaseId);

                        cmd.Parameters.AddWithValue("@Type", 1);
                        cmd.Parameters.AddWithValue("@CourtType", Convert.ToInt16(obj.CourtType));
                        cmd.Parameters.AddWithValue("@CaseNoInCourt", obj.CaseNoInCourt);
                        cmd.Parameters.AddWithValue("@Region", Convert.ToInt16(obj.Region));
                        cmd.Parameters.AddWithValue("@AidType", obj.AidType);
                        cmd.Parameters.AddWithValue("@CaseClassification", obj.CaseClassification);
                        cmd.Parameters.AddWithValue("@CaseType", Convert.ToInt16(obj.CaseType));
                        cmd.Parameters.AddWithValue("@OldCaseId", obj.OldCaseId);
                        cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                        cmd.Parameters.AddWithValue("@CaseAgainst", obj.CaseAgainst);
                        cmd.Parameters.AddWithValue("@Status", obj.Status);
                        cmd.Parameters.AddWithValue("@CaseSummary", obj.CaseSummary);
                        cmd.Parameters.AddWithValue("@AuthType", obj.AuthType);
                        cmd.Parameters.AddWithValue("@AuthUser", obj.AuthUser);
                        cmd.Parameters.AddWithValue("@CaseDate", obj.CaseDate);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewCaseId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string UpdateCase(CaseDetails obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetCaseData]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewCaseId = new SqlParameter("@NewCaseId", SqlDbType.NVarChar, 50);
                        NewCaseId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewCaseId);

                        cmd.Parameters.AddWithValue("@Type", 2);
                        cmd.Parameters.AddWithValue("@CourtType", Convert.ToInt16(obj.CourtType));
                        cmd.Parameters.AddWithValue("@CaseId", obj.CaseID);
                        cmd.Parameters.AddWithValue("@CaseNoInCourt", obj.CaseNoInCourt);
                        cmd.Parameters.AddWithValue("@Region", obj.Region);
                        cmd.Parameters.AddWithValue("@AidType", obj.AidType);
                        cmd.Parameters.AddWithValue("@CaseClassification", obj.CaseClassification);
                        cmd.Parameters.AddWithValue("@CaseType", Convert.ToInt16(obj.CaseType));
                        cmd.Parameters.AddWithValue("@OldCaseId", obj.OldCaseId);
                        cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                        cmd.Parameters.AddWithValue("@CaseAgainst", obj.CaseAgainst);
                        cmd.Parameters.AddWithValue("@Status", obj.Status);
                        cmd.Parameters.AddWithValue("@CaseSummary", obj.CaseSummary);
                        cmd.Parameters.AddWithValue("@AuthType", obj.AuthType);
                        cmd.Parameters.AddWithValue("@AuthUser", obj.AuthUser);
                        cmd.Parameters.AddWithValue("@CaseDate", obj.CaseDate);
                        cmd.Parameters.AddWithValue("@ModifiedBy", obj.ModifiedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewCaseId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while updating case:" + ex.Message;
            }
        }

        public string AddCaseComment(CaseComment obj)
        {
            // CaseComment lstCaseComment = new CaseComment();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("sp_Legal_InsertComments", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseID", obj.CaseID);
                    cmd.Parameters.AddWithValue("@DocumentId", obj.DocumentId);
                    cmd.Parameters.AddWithValue("@DocumentName", obj.DocumentName);
                    cmd.Parameters.AddWithValue("@Comments", obj.Comment);
                    cmd.Parameters.AddWithValue("@Designation",obj.Designation);
                    cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return Convert.ToString(obj.CaseID);
            }
        }

        public IList<CaseComment> GetCaseComment(CaseComment obj)
        {
            IList<CaseComment> lstCaseComment = new List<CaseComment>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseComment]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseId", obj.CaseID);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseComment entitymodel = new CaseComment();

                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.Comment = Convert.ToString(reader["Comments"]);
                            entitymodel.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);

                            lstCaseComment.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseComment;
            }
        }

        public string AddCaseHistory(CaseHistory obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetCaseHistory]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.AddWithValue("@CaseId", obj.CaseID);
                        cmd.Parameters.AddWithValue("@UserName", obj.UserName);
                        cmd.Parameters.AddWithValue("@Change", obj.Change);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(obj.CaseID);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public IList<CaseHistory> GetCaseHistory(CaseHistory obj)
        {
            IList<CaseHistory> lstCaseHistory = new List<CaseHistory>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseHistory]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseID", obj.CaseID);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseHistory entitymodel = new CaseHistory();

                            entitymodel.UserName = Convert.ToString(reader["UserName"]);
                            entitymodel.Change = Convert.ToString(reader["Change"]);
                            entitymodel.ModifiedDate = Convert.ToString(reader["ModifiedDate"]);

                            lstCaseHistory.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseHistory;
            }
        }

        public bool UpdateCaseHearing(CaseHearing obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetHearing]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter respSuccessOrError = new SqlParameter("@CasePKIDOutput", SqlDbType.Int);
                        respSuccessOrError.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(respSuccessOrError);

                        cmd.Parameters.AddWithValue("@Type", 2);
                        cmd.Parameters.AddWithValue("@Participantid", obj.ParticipantsId);

                        DateTime oDate = Convert.ToDateTime(obj.HearingDate);
                        cmd.Parameters.AddWithValue("@HearingDate", oDate);
                        cmd.Parameters.AddWithValue("@HearingNumber", obj.HearingNumber);
                        cmd.Parameters.AddWithValue("@CasePKID", obj.CaseId);
                        cmd.Parameters.AddWithValue("@HearingNotes", obj.HearingNotes);
                        cmd.Parameters.AddWithValue("@InvitedParticipants", obj.ParticipantsInvited);
                        cmd.Parameters.AddWithValue("@ParticipantsAttended", obj.ParticipantsAttended);
                        cmd.Parameters.AddWithValue("@CreatedOrModifiedBy", obj.CreatedBy);



                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IList<RecentCases> GetRecentCases(RecentCases obj)
        {
            IList<RecentCases> lstRecentCases = new List<RecentCases>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetRecentCases]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseAgainst", obj.CaseAgainst);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RecentCases entitymodel = new RecentCases();

                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.CaseID = Convert.ToString(reader["CaseID"]);
                            lstRecentCases.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstRecentCases;
            }
        }

        public IList<CaseParticipants> GetCaseParticipants(CaseParticipants obj)
        {
            IList<CaseParticipants> lstParticipantDetails = new List<CaseParticipants>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetParticipants]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    cmd.Parameters.AddWithValue("@ParticipantType", obj.ParticipantType);
                    // cmd.Parameters.AddWithValue("@Id", obj.PKID_Participant);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseParticipants entitymodel = new CaseParticipants();

                            entitymodel.CasePKID = Convert.ToInt16(reader["CasePKID"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.ParticipantType = Convert.ToInt16(reader["ParticipantType"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.Remarks = Convert.ToString(reader["Remarks"]);
                            entitymodel.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            entitymodel.HearingInvitedCount = Convert.ToString(reader["HearingInvitedCount"]);
                            entitymodel.HearingParticipatedCount = Convert.ToString(reader["HearingParticipatedCount"]);
                            entitymodel.PKID_Participant = Convert.ToInt16(reader["PKID_Participant"]);

                            lstParticipantDetails.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstParticipantDetails;
            }
        }

        public IList<CaseParticipantByEmail> GetCaseParticipantsByEmailId(CaseParticipantByEmail obj)
        {
            IList<CaseParticipantByEmail> lstCaseParticipantsByEmailId = new List<CaseParticipantByEmail>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseParticipantsByEmailId]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    cmd.Parameters.AddWithValue("@EmailId", obj.Email);
                    conn.Open();
                    
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseParticipantByEmail entitymodel = new CaseParticipantByEmail();

                            entitymodel.CasePKID = Convert.ToInt16(reader["CasePKID"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.ParticipantType = Convert.ToInt16(reader["ParticipantType"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.Remarks = Convert.ToString(reader["Remarks"]);
                            lstCaseParticipantsByEmailId.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCaseParticipantsByEmailId;
            }
        }

        public IList<CaseParticipants> GetHearingParticipants(CaseParticipants obj)
        {
            IList<CaseParticipants> lstHearingParticipants = new List<CaseParticipants>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetHearingParticipants]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseParticipants entitymodel = new CaseParticipants();

                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDateString = Convert.ToString(reader["HearingDate"]);
                            entitymodel.HearingInvitedParticipants = Convert.ToString(reader["InvitedParticipants"]);
                            entitymodel.HearingAttendedParticipats = Convert.ToString(reader["ParticipantsAttended"]);

                            lstHearingParticipants.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstHearingParticipants;
            }
        }

        public int AddCaseParicipants(CaseParticipants obj)
        {
            try
            {

                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetParticipants]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewParticipantId = new SqlParameter("@NewParticipantID", SqlDbType.Int);
                        NewParticipantId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewParticipantId);

                        cmd.Parameters.AddWithValue("@Type", 1);
                        cmd.Parameters.AddWithValue("@CasePKID", Convert.ToInt16(obj.CasePKID));
                        cmd.Parameters.AddWithValue("@ParticipantType", obj.ParticipantType);
                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        cmd.Parameters.AddWithValue("@Email", obj.Email);
                        cmd.Parameters.AddWithValue("@Designation", obj.Designation);
                        cmd.Parameters.AddWithValue("@HearingNumber", obj.HearingNumber);
                        cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);

                        //cmd.Parameters.Add("@NewParticipantID", SqlDbType.Int);
                        //cmd.Parameters["@NewParticipantID"].Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result = Convert.ToInt16(cmd.Parameters["@NewParticipantID"].Value);
                        conn.Close();

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateCaseParicipants(CaseParticipants obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetParticipants]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewParticipantId = new SqlParameter("@NewParticipantID", SqlDbType.Int);
                        NewParticipantId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewParticipantId);

                        cmd.Parameters.AddWithValue("@Type", 2);
                        cmd.Parameters.AddWithValue("@CasePKID", Convert.ToInt16(obj.CasePKID));
                        cmd.Parameters.AddWithValue("@PKID_Participant", Convert.ToInt16(obj.PKID_Participant));
                        cmd.Parameters.AddWithValue("@ParticipantType", obj.ParticipantType);
                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        cmd.Parameters.AddWithValue("@Email", obj.Email);
                        cmd.Parameters.AddWithValue("@Designation", obj.Designation);
                        cmd.Parameters.AddWithValue("@HearingNumber", obj.HearingNumber);
                        cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewParticipantId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string AddHearingParicipants(CaseParticipants obj)
        {
            try
            {
                var result = "";
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetHearing]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter respSuccessOrError = new SqlParameter("@CasePKIDOutput", SqlDbType.Int);
                        respSuccessOrError.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(respSuccessOrError);

                        cmd.Parameters.AddWithValue("@Type", 1);
                        //cmd.Parameters.AddWithValue("@HearingDate", Convert.ToString(obj.hearingDate));
                        cmd.Parameters.AddWithValue("@HearingDate", DateTime.Now);
                    
                        cmd.Parameters.AddWithValue("@HearingNumber", obj.HearingNumber);
                        cmd.Parameters.AddWithValue("@HearingLocation",obj.HearingLocation);
                        cmd.Parameters.AddWithValue("@HearingCourt",obj.HearingCourt);
                        cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                        cmd.Parameters.AddWithValue("@HearingNotes", obj.HearingNotes);
                        cmd.Parameters.AddWithValue("@InvitedParticipants", obj.HearingInvitedParticipants);
                        cmd.Parameters.AddWithValue("@CreatedOrModifiedBy", obj.CreatedBy);

                      



                        conn.Open();
                        cmd.ExecuteNonQuery();


                      
                        conn.Close();
                        return   Convert.ToString(respSuccessOrError.Value); ;

                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding hearing participants:" + ex.Message;
            }
        }

        public string UpdateHearingParicipants(CaseParticipants obj)
        {
            try
            {


                var result = "";
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetHearing]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter respSuccessOrError = new SqlParameter("@CasePKIDOutput", SqlDbType.NVarChar,50);
                        respSuccessOrError.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(respSuccessOrError);
                        //DateTime parsedDate = DateTime.Parse(obj.HearingDateString);

                        cmd.Parameters.AddWithValue("@Type", 2);
                        cmd.Parameters.AddWithValue("@HearingDate", obj.hearingDate);
                        cmd.Parameters.AddWithValue("@HearingNumber", obj.HearingNumber);
                        cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                        cmd.Parameters.AddWithValue("@HearingAttended", obj.HearingAttended);
                        cmd.Parameters.AddWithValue("@Participantid", obj.PKID_Participant);
                        cmd.Parameters.AddWithValue("@HearingNotes", obj.HearingNotes);
                        cmd.Parameters.AddWithValue("@InvitedParticipants", obj.HearingInvitedParticipants);
                        cmd.Parameters.AddWithValue("@ParticipantsAttended", obj.HearingAttendedParticipats);
                        cmd.Parameters.AddWithValue("@CreatedOrModifiedBy", obj.ModifiedBy);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(respSuccessOrError.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while updating hearing participants:" + ex.Message;
            }
        }

        public IList<CaseHearing> GetCaseHearing(CaseHearing obj)
        {
            IList<CaseHearing> lstCaseHearing = new List<CaseHearing>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetHearing]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseId", obj.CaseId);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseHearing entitymodel = new CaseHearing();
                            entitymodel.ParticipantsId = Convert.ToInt16(reader["ParticipantId"]);
                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDate = Convert.ToDateTime(reader["HearingDate"]);
                            entitymodel.HearingAttended = Convert.ToBoolean(reader["HearingAtended"]);
                            entitymodel.ParticipantsInvited = Convert.ToString(reader["ParticipantsInvited"]);
                            entitymodel.ParticipantsAttended = Convert.ToString(reader["ParticipantsAttended"]);
                            entitymodel.HearingNotes = Convert.ToString(reader["HearingNotes"]);
                            lstCaseHearing.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseHearing;
            }
        }

        public IList<CaseHearing> GetCaseHearingNumbers(CaseHearing obj)
        {
            IList<CaseHearing> lstCaseHearing = new List<CaseHearing>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseHearingNumbers]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseID", obj.CaseId);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseHearing entitymodel = new CaseHearing();

                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            lstCaseHearing.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCaseHearing;
            }
        }

        public IList<Master_Employee> GetEmployeeByEmail(string Emailid)
        {
            List<Master_Employee> lstEmployees = new List<Master_Employee>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Employee_GetEmployeeByEmail]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmailId", Emailid);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Employee entitymodel = new Master_Employee();
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                            entitymodel.PKID_Employee = Convert.ToInt16(reader["PKID_Employee"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.DOB = Convert.ToString(reader["DOB"]);
                            entitymodel.Mobilenum = Convert.ToString(reader["MobileNum"]);
                            entitymodel.Posting = Convert.ToString(reader["PostingLocation"]);
                            entitymodel.Position = Convert.ToString(reader["Position"]);


                            lstEmployees.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstEmployees;
            }
        }

        public string AddApplicantAndRespondent(CaseApplicantsAndRespondents obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetApplicantsAndRespondents]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter respSuccessOrError = new SqlParameter("@NewID", SqlDbType.Int);
                        respSuccessOrError.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(respSuccessOrError);

                        cmd.Parameters.AddWithValue("@Type", 1);
                        cmd.Parameters.AddWithValue("@IsRespondent", Convert.ToBoolean(obj.ApplicantType));
                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        cmd.Parameters.AddWithValue("@Email", obj.Email);
                        cmd.Parameters.AddWithValue("@Address", obj.Address);
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(respSuccessOrError.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding applicant or respondent:" + ex.Message;
            }
        }

        public string UpdateApplicantAndRespondent(CaseApplicantsAndRespondents obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Legal_SetApplicantsAndRespondents]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter respSuccessOrError = new SqlParameter("@NewID", SqlDbType.Int);
                        respSuccessOrError.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(respSuccessOrError);

                        cmd.Parameters.AddWithValue("@Type", 2);
                        cmd.Parameters.AddWithValue("@IsRespondent", Convert.ToBoolean(obj.ApplicantType));
                        cmd.Parameters.AddWithValue("@PKID_ApplicantAndRespondent", Convert.ToInt16(obj.PKID_ApplicantAndRespondent));
                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        cmd.Parameters.AddWithValue("@Email", obj.Email);
                        cmd.Parameters.AddWithValue("@Address", obj.Address);
                        cmd.Parameters.AddWithValue("@IsActive", obj.IsActive);

                        cmd.Parameters.AddWithValue("@ModifiedBy", obj.ModifiedBy);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(respSuccessOrError.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while updating applicant or respondent:" + ex.Message;
            }
        }

        public IList<CaseApplicantsAndRespondents> GetApplicantsAndRespondents(CaseApplicantsAndRespondents obj)
        {
            IList<CaseApplicantsAndRespondents> lstCaseApplicantsAndRespondents = new List<CaseApplicantsAndRespondents>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[sp_Legal_GetApplicantsAndRespondents]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsRespondent", Convert.ToBoolean(obj.ApplicantType));
                    cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    cmd.Parameters.AddWithValue("@PKID_ApplicantAndRespondent", obj.PKID_ApplicantAndRespondent);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseApplicantsAndRespondents entitymodel = new CaseApplicantsAndRespondents();

                            entitymodel.PKID_ApplicantAndRespondent = Convert.ToInt16(reader["PKID_ApplicantAndRespondent"]);
                            entitymodel.CasePKID = Convert.ToInt16(reader["CasePKID"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.ApplicantType = Convert.ToBoolean(reader["IsRespondent"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);
                            entitymodel.Address = Convert.ToString(reader["Address"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            entitymodel.ModifiedBy = Convert.ToString(reader["ModifiedBy"]);
                            entitymodel.ModifiedDate = Convert.ToString(reader["ModifiedDate"]);
                            entitymodel.IsActive = reader["IsActive"] == DBNull.Value ? true : Convert.ToBoolean(reader["IsActive"]);
                            lstCaseApplicantsAndRespondents.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCaseApplicantsAndRespondents;
            }
        }

        public string SendEmailReminderToHearingParicipants()
        {
            IList<CaseParticipants> lstHearingParticipantsForReminder = new List<CaseParticipants>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetHearingParicipantsForReminder]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseParticipants entitymodel = new CaseParticipants();

                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDateString = Convert.ToString(reader["HearingDate"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseID"]);
                            entitymodel.CasePKID = Convert.ToInt16(reader["CasePKID"]);
                            entitymodel.Name = Convert.ToString(reader["AttendeeName"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);

                            using (LegalEmailReminder emailReminder = new LegalEmailReminder())
                            {
                                emailReminder.ComposeMail(entitymodel);
                            }
                            lstHearingParticipantsForReminder.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return "Success";
            }
        }

        public string SendSMSReminderToHearingParicipants()
        {
            IList<CaseParticipants> lstHearingParticipantsForReminder = new List<CaseParticipants>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetHearingParicipantsForReminder]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@CasePKID", obj.CasePKID);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseParticipants entitymodel = new CaseParticipants();

                            entitymodel.HearingNumber = Convert.ToString(reader["HearingNumber"]);
                            entitymodel.HearingDateString = Convert.ToString(reader["HearingDate"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseID"]);
                            entitymodel.CasePKID = Convert.ToInt16(reader["CasePKID"]);
                            entitymodel.Name = Convert.ToString(reader["AttendeeName"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);

                            using (LegalSMSReminder emailReminder = new LegalSMSReminder())
                            {
                                emailReminder.ComposeMail(entitymodel);
                            }
                            lstHearingParticipantsForReminder.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return "Success";
            }
        }
        
        public class LegalEmailReminder : IDisposable
        {
            private MailMessage msgMail = new MailMessage();

            public bool ComposeMail(CaseParticipants mo)
            {
                try
                {
                    msgMail.To.Add(mo.Email);
                    //msgMail.Priority = mo.MailPriority;
                    msgMail.Subject = "Legal case hearing reminder";
                    msgMail.Body = "Dear Sir/Madam, This is to inform you that legal case hearing for hearing number " + mo.HearingNumber + " is due on " + mo.HearingDateString + " for case id " + mo.CaseId;
                    msgMail.IsBodyHtml = true;
                    msgMail.BodyEncoding = System.Text.Encoding.UTF8;

                    SmtpClient client = new SmtpClient();
                    client.Send(msgMail);
                    return true;
                }

                catch (Exception ex)
                {
                    //return ex.Message;
                    return false;
                }
            }

            #region IDisposable Members

            public void Dispose()
            {
                msgMail.Dispose();
            }

            #endregion IDisposable Members
        }

        public class LegalSMSReminder : IDisposable
        {
            private MailMessage msgMail = new MailMessage();

            public bool ComposeMail(CaseParticipants mo)
            {
                try
                {
                    msgMail.To.Add(mo.Email);
                    //msgMail.Priority = mo.MailPriority;
                    msgMail.Subject = "Legal case hearing reminder";
                    msgMail.Body = "This is to inform you that legal case hearing " + mo.HearingNumber + " is due on " + mo.HearingDateString + "for case id " + mo.CaseId;
                    msgMail.IsBodyHtml = true;
                    msgMail.BodyEncoding = System.Text.Encoding.UTF8;

                    SmtpClient client = new SmtpClient();
                    client.Send(msgMail);
                    return true;
                }

                catch (Exception ex)
                {
                    //return ex.Message;
                    return false;
                }
            }

            #region IDisposable Members

            public void Dispose()
            {
                msgMail.Dispose();
            }

            #endregion IDisposable Members
        }

        public IList<CaseComment> SetCaseComment(CaseComment obj)
        {
            IList<CaseComment> lstCaseComment = new List<CaseComment>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetCaseComment]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DocumentId", obj.DocumentId);
                    cmd.Parameters.AddWithValue("@CaseID", obj.CaseID);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CaseComment entitymodel = new CaseComment();

                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.Comment = Convert.ToString(reader["Comments"]);
                            entitymodel.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);

                            lstCaseComment.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseComment;
            }
        }


        public IList<AuthEmailId> GetAuthUsers(AuthEmailId obj)
        {
            IList<AuthEmailId> lstemail = new List<AuthEmailId>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetUsersByAuth]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Position", obj.Position);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AuthEmailId entitymodel = new AuthEmailId();

                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            lstemail.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstemail;
            }
        }


        public IList<AuthEmailId> GetAuthTypes(AuthEmailId obj)
        {
            IList<AuthEmailId> lstemail = new List<AuthEmailId>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Legal_GetAuthTypes]";
                    cmd.CommandType = CommandType.StoredProcedure;
                   // cmd.Parameters.AddWithValue("@Position", obj.Position);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AuthEmailId entitymodel = new AuthEmailId();

                            entitymodel.Position = Convert.ToString(reader["AuthType"]);
                            lstemail.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstemail;
            }
        }
    }
}
