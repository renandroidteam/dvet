﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Master
{
    public class InwardDB
    {
        public int IoAddCase(Inwardmodel obj)
        {
            try
            {
                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].sp_InsertInwardDetails", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        // cmd.Parameters.AddWithValue("@CaseNumber", Convert.ToInt16(obj.CaseNumber));
                        //  cmd.Parameters.AddWithValue("@Id", Convert.ToInt16(obj.Id));
                        cmd.Parameters.AddWithValue("@LetterType", Convert.ToInt16(obj.LetterType));
                        cmd.Parameters.AddWithValue("@LetterRefNumber", Convert.ToString(obj.LetterRefNumber));
                        cmd.Parameters.AddWithValue("@SenderName", Convert.ToString(obj.SenderName));
                        cmd.Parameters.AddWithValue("@Subject", Convert.ToString(obj.Subject));
                        cmd.Parameters.AddWithValue("@Recivedthrough", Convert.ToString(obj.Recivedthrough));
                        cmd.Parameters.AddWithValue("@DeliverRecievedMode", Convert.ToInt16(obj.DeliverRecievedMode));
                        cmd.Parameters.AddWithValue("@RecievedDate", Convert.ToDateTime(obj.RecievedDate));
                        cmd.Parameters.AddWithValue("@AttachedDocument", Convert.ToInt16(obj.AttachedDocument));
                        cmd.Parameters.AddWithValue("@DeskId", Convert.ToInt16(obj.DeskId));
                        cmd.Parameters.AddWithValue("@CentralGovernmentId", Convert.ToInt16(obj.CentralGovernmentId));
                        cmd.Parameters.AddWithValue("@Department", Convert.ToString(obj.Department));
                        cmd.Parameters.AddWithValue("@AssignedFrom", Convert.ToString(obj.AssignedFrom));
                        cmd.Parameters.AddWithValue("@AssignedTo", Convert.ToString(obj.AssignedTo));
                        cmd.Parameters.AddWithValue("@NoOfPages", Convert.ToInt16(obj.NoOfPages));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(obj.CreatedBy));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@CreatedLocation", Convert.ToString(obj.CreatedLocation));
                        cmd.Parameters.AddWithValue("@Document", Convert.ToString(obj.Document));
                        cmd.Parameters.AddWithValue("@ForwardedTo", Convert.ToInt16(obj.ForwardedTo));
                        //cmd.Parameters.AddWithValue("@CaseId", Convert.ToString(obj.c));

                        cmd.Parameters.Add("@CaseId", SqlDbType.Int);
                        cmd.Parameters["@CaseId"].Direction = ParameterDirection.Output;



                        conn.Open();
                        cmd.ExecuteNonQuery();


                        result = Convert.ToInt16(cmd.Parameters["@CaseId"].Value);
                        conn.Close();
                        return result;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public IList<Inwardmodel> GetInwardData(int Id)
        {

            IList<Inwardmodel> lstentitymodel = new List<Inwardmodel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_Inward_GetCaseDetails]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", Id);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Inwardmodel entitymodel = new Inwardmodel();

                            entitymodel.Id = Convert.ToInt16(reader["Id"]);
                            entitymodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                            entitymodel.LetterType = Convert.ToInt16(reader["LetterType"]);
                            entitymodel.LetterRefNumber = Convert.ToString(reader["LetterRefNumber"]);
                            entitymodel.SenderName = Convert.ToString(reader["SenderName"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.Recivedthrough = Convert.ToString(reader["RecievedThrough"]);
                            entitymodel.DeliverRecievedMode = Convert.ToInt16(reader["DeliverRecieveMode"]);
                            entitymodel.RecievedDate = Convert.ToDateTime(reader["RecievedDate"]);
                            entitymodel.AttachedDocument = Convert.ToInt16(reader["AttachedDocument"]);
                            entitymodel.DeskId = Convert.ToInt16(reader["PKID_Desk"]);
                            entitymodel.CentralGovernmentId = Convert.ToInt16(reader["CentralGovernmentId"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);
                            entitymodel.AssignedFrom = Convert.ToString(reader["AssignedFrom"]);
                            entitymodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            entitymodel.CreatedLocation = Convert.ToString(reader["CreatedLocation"]);
                            entitymodel.Document = Convert.ToString(reader["Document"]);

                            lstentitymodel.Add(entitymodel);
                        }
                        return lstentitymodel;
                    }
                }

            }


        }



        public IList<Inwardmodel> SearchCase(Inwardmodel model)
        {
            IList<Inwardmodel> lstsearchmodel = new List<Inwardmodel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_SearchCase";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseNumber", model.CaseNumber);
                    cmd.Parameters.AddWithValue("@LetterType", model.LetterType);
                    cmd.Parameters.AddWithValue("@Subject", model.Subject);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Inwardmodel entitymodel = new Inwardmodel();

                            entitymodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                            entitymodel.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            entitymodel.LetterType = Convert.ToInt32(reader["LetterType"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.LetterTypedesc = Convert.ToString(reader["LetterTypedesc"]);
                            entitymodel.Id = Convert.ToInt32(reader["Id"]);
                            lstsearchmodel.Add(entitymodel);
                        }
                        return lstsearchmodel;
                    }
                }
            }

        }



        public Inwardmodel GetInwardDatabycaseno(String CaseNumber)
        {
            try
            {
                Inwardmodel lstmodel = new Inwardmodel();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "usp_getId";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                lstmodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                                lstmodel.Id = Convert.ToInt32(reader["Id"]);
                                lstmodel.NoOfPages = Convert.ToInt32(reader["NoOfPages"]);
                                lstmodel.ForwardedTo = reader["ForwardedTo"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ForwardedTo"]); 
                              
                                lstmodel.LetterTypedesc = Convert.ToString(reader["LetterTypedesc"]);
                                lstmodel.LetterRefNumber = Convert.ToString(reader["LetterRefNumber"]);
                                lstmodel.SenderName = Convert.ToString(reader["SenderName"]);
                                lstmodel.Subject = Convert.ToString(reader["Subject"]);
                                lstmodel.Recivedthrough = Convert.ToString(reader["RecievedThrough"]);
                                lstmodel.RecievedDate = Convert.ToDateTime(reader["RecievedDate"]);
                                lstmodel.Document = Convert.ToString(reader["Document"]);
                            }
                            return lstmodel;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Inwardmodel GetInwardDatabycasenumber(String CaseNumber)
        {
            try
            {
                Inwardmodel lstmodel = new Inwardmodel();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "usp_getDecisionBasedDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                lstmodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                                lstmodel.Id = Convert.ToInt32(reader["Id"]);
                                lstmodel.NoOfPages = Convert.ToInt32(reader["NoOfPages"]);
                                lstmodel.ForwardedTo = Convert.ToInt32(reader["ForwardedTo"]);
                                lstmodel.LetterTypedesc = Convert.ToString(reader["LetterTypedesc"]);
                                lstmodel.LetterRefNumber = Convert.ToString(reader["LetterRefNumber"]);
                                lstmodel.SenderName = Convert.ToString(reader["SenderName"]);
                                lstmodel.Subject = Convert.ToString(reader["Subject"]);
                                lstmodel.Recivedthrough = Convert.ToString(reader["RecievedThrough"]);
                                lstmodel.RecievedDate = Convert.ToDateTime(reader["RecievedDate"]);
                                lstmodel.Decision = Convert.ToString(reader["Status"]);
                            }
                            return lstmodel;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string AddFiles(Addfiles obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_Inward_InsertFilecategory", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.AddWithValue("@FileNumber", obj.FileNumber);
                        cmd.Parameters.AddWithValue("@CaseId", obj.CaseNo);
                        cmd.Parameters.AddWithValue("@FileName", obj.FileName);
                        cmd.Parameters.AddWithValue("@DocumentAttached", obj.AttachedDocument);
                        cmd.Parameters.AddWithValue("@FileSubject", obj.FileSubject);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        cmd.Parameters.AddWithValue("@CreatedOn", System.DateTime.Now);
                        cmd.Parameters.AddWithValue("@FileCategory", obj.FileCategory);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return "Success";


                    }
                }

            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }




        public string AddDeskOfficerDetails(DeskOfficerResponseModel obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Inward_InsertDeskOfficerResponseDetails]", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@CaseId", Convert.ToInt32(obj.CaseNo));
                        cmd.Parameters.AddWithValue("@AssignedTo", Convert.ToString(obj.AssignedTo));
                        cmd.Parameters.AddWithValue("@SetDueDate", Convert.ToDateTime(obj.SetDueDate));
                        cmd.Parameters.AddWithValue("@Priority", Convert.ToString(obj.Priority));
                        cmd.Parameters.AddWithValue("@Remarks", Convert.ToString(obj.Remarks));
                        cmd.Parameters.AddWithValue("@Comments", Convert.ToString(obj.Comments));
                        cmd.Parameters.AddWithValue("@DocumentPath", Convert.ToString(obj.DocumentPath));
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return "Success";

                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }




        public IList<Inwardmodel> GetInwardMYPendingTask(String AssignedTo)
        {
            try
            {
                IList<Inwardmodel> obj = new List<Inwardmodel>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_InwardMYPendingTask]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AssignedTo", AssignedTo);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inwardmodel lstmodel = new Inwardmodel();
                                lstmodel.Id = Convert.ToInt16(reader["Id"]);
                                lstmodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                                lstmodel.PKID_LetterType = Convert.ToInt16(reader["PKID_LetterType"]);
                                lstmodel.Letter_Type = Convert.ToString(reader["LetterType"]);
                                lstmodel.LetterRefNumber = Convert.ToString(reader["LetterRefNumber"]);
                                lstmodel.SenderName = Convert.ToString(reader["SenderName"]);
                                lstmodel.Subject = Convert.ToString(reader["Subject"]);
                                lstmodel.Recivedthrough = Convert.ToString(reader["RecievedThrough"]);
                                lstmodel.DeliverRecievedMode = Convert.ToInt16(reader["DeliverRecieveMode"]);
                                lstmodel.RecievedDate = Convert.ToDateTime(reader["RecievedDate"]);
                                lstmodel.AttachedDocument = Convert.ToInt16(reader["AttachedDocument"]);
                                lstmodel.DeskId = Convert.ToInt16(reader["DeskId"]);
                                lstmodel.CentralGovernmentId = Convert.ToInt16(reader["CentralGovernmentId"]);
                                lstmodel.Department = Convert.ToString(reader["Department"]);
                                lstmodel.AssignedFrom = Convert.ToString(reader["AssignedFrom"]);
                                lstmodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                                lstmodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                                lstmodel.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                                lstmodel.CreatedLocation = Convert.ToString(reader["CreatedLocation"]);
                                //  lstmodel.NoOfPages = Convert.ToInt16(reader["NoOfPages"]);
                                lstmodel.Status = Convert.ToString(reader["Status"]);
                                lstmodel.StatusId = Convert.ToInt16(reader["StatusID"]);
                                obj.Add(lstmodel);
                            }
                            return obj;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IList<DeskOfficerResponseModel> GetDeskOfficerResponseDetails(int CaseId)
        {
            try
            {
                IList<DeskOfficerResponseModel> obj = new List<DeskOfficerResponseModel>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_Inward_GetDeskOfficerResponseDetails]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseId", CaseId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DeskOfficerResponseModel lstmodel = new DeskOfficerResponseModel();
                                lstmodel.CaseId = Convert.ToInt16(reader["CaseId"]);
                                lstmodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                                lstmodel.Comments = Convert.ToString(reader["Comments"]);
                                lstmodel.Remarks = Convert.ToString(reader["Remarks"]);
                                lstmodel.Priority = Convert.ToString(reader["Priority"]);
                                lstmodel.DocumentPath = Convert.ToString(reader["DocumentPath"]);
                                lstmodel.SetDueDate = Convert.ToDateTime(reader["SetDueDate"]);

                                obj.Add(lstmodel);

                            }
                            return obj;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string AddFileNotes(AddfileNotes obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Inward_InsertFileNotes]", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@CaseId", obj.CaseNo);
                        cmd.Parameters.AddWithValue("@FId", obj.FileNo);
                        cmd.Parameters.AddWithValue("@NoteNumber", obj.NoteNumber);
                        cmd.Parameters.AddWithValue("@Subject", obj.Subject);
                        cmd.Parameters.AddWithValue("@Status", obj.Status);
                        cmd.Parameters.AddWithValue("@Notes", obj.Notes);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                        cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return "Success";

                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }


        public IList<AddfileNotes> GetFileNotesDetails(AddfileNotes addfile)
        {
            try
            {
                IList<AddfileNotes> obj = new List<AddfileNotes>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_Inward_GetFileNotes]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FId", addfile.FileNo);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                AddfileNotes lstmodel = new AddfileNotes();
                                lstmodel.Id = Convert.ToInt16(reader["Id"]);
                                lstmodel.CaseNo = Convert.ToInt16(reader["CaseID"]);
                                lstmodel.FileNo = Convert.ToString(reader["FId"]);
                                lstmodel.NoteNumber = Convert.ToString(reader["NoteNumber"]);
                                lstmodel.Subject = Convert.ToString(reader["Subject"]);
                                lstmodel.Notes = Convert.ToString(reader["Notes"]);
                                lstmodel.Status = Convert.ToString(reader["Status"]);
                                lstmodel.CreatedBy = Convert.ToInt16(reader["CreatedBy"]);
                                lstmodel.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
                                lstmodel.Remarks = Convert.ToString(reader["Remarks"]);
                                obj.Add(lstmodel);

                            }
                            return obj;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string AddFileDetails(AddFileDetails obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Inward_InsertFileCaseDetails]", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@FileNumber", Convert.ToInt16(obj.FileNumber));
                        cmd.Parameters.AddWithValue("@FileName", Convert.ToString(obj.FileName));
                        cmd.Parameters.AddWithValue("@FileSubject", Convert.ToString(obj.FileSubject));
                        cmd.Parameters.AddWithValue("@FileCategory", Convert.ToString(obj.FileCategory));
                        cmd.Parameters.AddWithValue("@CreatedLocation", Convert.ToString(obj.CreatedLocation));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt16(obj.CreatedBy));
                        cmd.Parameters.AddWithValue("@CreatedDate", Convert.ToDateTime(obj.CreatedDate));
                        cmd.Parameters.AddWithValue("@CaseId", Convert.ToString(obj.CaseId));

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return "Success";

                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }

        }

        public string AddFileCaseMapping(AddFileCaseMapping obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Inward_InsertFileCaseMapping]", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt16(obj.CreatedBy));
                        cmd.Parameters.AddWithValue("@FIId", Convert.ToInt16(obj.FIId));
                        cmd.Parameters.AddWithValue("@CaseId", Convert.ToString(obj.CaseId));

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return "Success";

                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }


        public IList<AddFileDetails> GetFileDetails(int Caseid)
        {

            IList<AddFileDetails> lstentitymodel = new List<AddFileDetails>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_GetInwardFiles";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseId", Caseid);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AddFileDetails entitymodel = new AddFileDetails();

                            entitymodel.Id = Convert.ToInt16(reader["FIId"]);
                            entitymodel.FileNo = Convert.ToString(reader["FileNumber"]);
                            entitymodel.FileName = Convert.ToString(reader["FileName"]);
                            entitymodel.FileSubject = Convert.ToString(reader["FileSubject"]);
                            entitymodel.CaseId = Convert.ToInt16(reader["CaseId"]);


                            lstentitymodel.Add(entitymodel);
                        }
                        return lstentitymodel;
                    }
                }
            }
        }

        public IList<Addfiles> Viewfiles(Addfiles model)
        {
            try
            {
                IList<Addfiles> lstsearchViewmodel = new List<Addfiles>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_SearchViewCase]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileNumber", model.FileNumber);
                        cmd.Parameters.AddWithValue("@FileName", model.FileName);
                        cmd.Parameters.AddWithValue("@FileSubject", model.FileSubject);
                        cmd.Parameters.AddWithValue("@FileCategory", model.FileCategory);
                        cmd.Parameters.AddWithValue("@CreatedDate", model.CreatedDate);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Addfiles entitymodel = new Addfiles();

                                entitymodel.FileNumber = Convert.ToString(reader["FileNumber"]);
                                entitymodel.FileName = Convert.ToString(reader["FileName"]);
                                entitymodel.FileSubject = Convert.ToString(reader["FileSubject"]);
                                entitymodel.FileCategory = Convert.ToInt16(reader["FileCategory"]);

                                entitymodel.FIId = Convert.ToInt16(reader["FIId"]);

                                lstsearchViewmodel.Add(entitymodel);
                            }
                            return lstsearchViewmodel;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IList<Inwardmodel> Listofcases(Inwardmodel model)
        {
            IList<Inwardmodel> lstListofcasesmodel = new List<Inwardmodel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_GetListCases]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FileNumber", model.FileNumber);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Inwardmodel entitymodel = new Inwardmodel();

                            entitymodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                            entitymodel.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);


                            lstListofcasesmodel.Add(entitymodel);
                        }
                        return lstListofcasesmodel;
                    }



                }
            }
        }


        public IList<Desk> GetDesks()
        {
            IList<Desk> listDesk = new List<Desk>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "HR_GetDeskdetails";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Desk deskView = new Desk();

                            deskView.DeskId = Convert.ToInt32(reader["PKID_Desk"]);
                            deskView.DeskName = Convert.ToString(reader["DeskName"]);


                            listDesk.Add(deskView);
                        }
                    }
                }
            }
            return listDesk;
        }




        public IList<Department> GetDepartments()
        {
            IList<Department> listDepartment = new List<Department>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "HR_GetDepartmentdetails";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Department departmentView = new Department();

                            departmentView.DepartmentId = Convert.ToInt32(reader["PKID_Department"]);
                            departmentView.DepartmentName = Convert.ToString(reader["DepartmentName"]);


                            listDepartment.Add(departmentView);
                        }
                    }
                }
            }
            return listDepartment;
        }





        public IList<Employee> GetEmployees(Employee model)
        {
            try
            {
                IList<Employee> listEmp = new List<Employee>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "HR_GetForwardedTo";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DepartmentId", model.DepartmentId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Employee empView = new Employee();

                                empView.EmployeeId = Convert.ToInt32(reader["PKID_Employee"]);
                                empView.EmployeeName = Convert.ToString(reader["FirstName"]);
                                empView.DepartmentId = Convert.ToInt32(reader["DepartmentId"]);
                                empView.Emailid = Convert.ToString(reader["EmailId"]);
                                listEmp.Add(empView);
                            }

                            return listEmp;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IList<LetterType> GetLetterTypes()
        {
            try
            {
                IList<LetterType> listType = new List<LetterType>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "HR_GetLetterTypes";
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LetterType typeView = new LetterType();

                                typeView.LetterTypeId = Convert.ToInt32(reader["PKID_LetterType"]);
                                typeView.LetterTypeName = Convert.ToString(reader["LetterType"]);


                                listType.Add(typeView);
                            }
                            return listType;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public IList<DeskOfficer> GetCentralGovernmentOffices()
        {
            try
            {
                IList<DeskOfficer> listOffice = new List<DeskOfficer>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "HR_GetCentralGovernmentOffices";
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DeskOfficer officeView = new DeskOfficer();

                                officeView.PKID_CentralGovtOffice = Convert.ToInt32(reader["PKID_CentralGovtOffice"]);
                                officeView.OfficeName = Convert.ToString(reader["OfficeName"]);


                                listOffice.Add(officeView);
                            }
                            return listOffice;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Status> GetStatusDetails()
        {
            IList<Status> listStatus = new List<Status>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_Inward_GetStatusDetails";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Status statusModel = new Status();

                            statusModel.Id = Convert.ToInt32(reader["Id"]);
                            statusModel.StatusCode = Convert.ToString(reader["StateCode"]);
                            statusModel.StatusDesc = Convert.ToString(reader["Statusdesc"]);
                            statusModel.IsActive = Convert.ToBoolean(reader["IsActive"]);
                            listStatus.Add(statusModel);
                        }
                    }
                }
            }
            return listStatus;
        }



        public IList<NoteStatus> GetNoteStatusDetails()
        {
            IList<NoteStatus> listStatus = new List<NoteStatus>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetNoteStatusDetails";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            NoteStatus statusModel = new NoteStatus();

                            statusModel.Id = Convert.ToInt32(reader["Id"]);
                            statusModel.Status = Convert.ToString(reader["Status"]);

                            listStatus.Add(statusModel);
                        }
                    }
                }
            }
            return listStatus;
        }

        public int IinsertNotesStatus(NoteStatusDecision obj)
        {

            int result = 0;

            try
            {

               
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Sp_InsertDecisionNotesDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        
                        
                        cmd.Parameters.AddWithValue("@NoteId", obj.NoteId);
                        cmd.Parameters.AddWithValue("@StatusId",obj.StatusId);
                        cmd.Parameters.AddWithValue("@CaseId", obj.CaseNo);
                        cmd.Parameters.AddWithValue("@Remarks",obj.Remarks);
                        cmd.ExecuteNonQuery();


                      
                        conn.Close();
                        

                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string UpdateWorkFlowStatus(WorkflowProcessModel workflow)
        {
            string res = "";
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_UpdateWorkflowStatus";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFInstanceID", workflow.WorkflowInstanceId);
                        cmd.Parameters.AddWithValue("@RequestId", workflow.CaseId); 
                        
                        cmd.ExecuteNonQuery();
                        res ="";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }



        public string InsertWorkflowRemarks(WorkflowProcessModel workflow)
        {
            string res = "";
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_InsertWorkflowRemarks";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WorkFlowInstance", workflow.WorkflowInstanceId);
                        cmd.Parameters.AddWithValue("@CaseId", workflow.CaseId);
                        cmd.Parameters.AddWithValue("@Remarks", workflow.Remarks);
                        cmd.ExecuteNonQuery();
                        res = "";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }



        public int UpdateWorkFlowApprovedStatus(NoteStatusDecision note)
        {
            int res = 0;
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Sp_ApprovedNotesDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@NoteId", note.NoteId);
                        //cmd.Parameters.AddWithValue("@StatusId", note.StatusId);
                        cmd.Parameters.AddWithValue("@CaseId", note.CaseNo);

                        cmd.ExecuteNonQuery();
                        res = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }



        public int UpdateWorkFlowRejectedStatus(NoteStatusDecision note)
        {
            int res = 0;
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Sp_RejectedNotesDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@NoteId", note.NoteId);
                        //cmd.Parameters.AddWithValue("@StatusId", note.StatusId);
                        cmd.Parameters.AddWithValue("@CaseId", note.CaseNo);

                        cmd.ExecuteNonQuery();
                        res = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }


        public IList<Inwardmodel> GetDocumentDetials(String CaseNumber)
        {
            try
            {
                IList<Inwardmodel> lstmodels = new List<Inwardmodel>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Sp_DocumentDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inwardmodel lstmodel= new Inwardmodel();
                                lstmodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                                lstmodel.Id = Convert.ToInt32(reader["Id"]);
                                
                                lstmodel.Document = Convert.ToString(reader["DocumentName"]);
                                lstmodels.Add(lstmodel);
                            }
                            return lstmodels;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}















