﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
    public class StudentManagementDB
    {

        public int  studentProfileDetails(StudentProfileDetails cd)
        {
            try {
                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
                    {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                        {
                    cmd.CommandText = "Sp_AddStudentGeneralDetails";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", Convert.ToString(cd.FirstName));
                    cmd.Parameters.AddWithValue("@FatherName", Convert.ToString(cd.FatherName));
                    cmd.Parameters.AddWithValue("@MotherName", Convert.ToString(cd.MotherName));
                    cmd.Parameters.AddWithValue("@LastName", Convert.ToString(cd.LastName));
                    cmd.Parameters.AddWithValue("@FatherOccupation", Convert.ToString(cd.FatherOccupation));
                    cmd.Parameters.AddWithValue("@FatherAnnualIncome", Convert.ToString(cd.FatherAnnualIncome));
                    cmd.Parameters.AddWithValue("@MotherOccupation", Convert.ToString(cd.MotherOccupation));
                    cmd.Parameters.AddWithValue("@MotherAnnualIncome", Convert.ToString(cd.MotherAnnualIncome));
                    cmd.Parameters.AddWithValue("@PrimaryPhoneNumber", Convert.ToString(cd.PrimaryPhone));
                    cmd.Parameters.AddWithValue("@SecondaryPhoneNumber", Convert.ToString(cd.SecondaryPhone));
                    cmd.Parameters.AddWithValue("@RegistrationNumber", Convert.ToString(cd.RegistrationNumber));
                    cmd.Parameters.AddWithValue("@InstituteName", Convert.ToString(cd.NameOfInstitute));
                    cmd.Parameters.AddWithValue("@YearOfTheBatch", Convert.ToString(cd.YearOfPlacement));
                    cmd.Parameters.AddWithValue("@Course", Convert.ToString(cd.TradeOrCourse));
                    cmd.Parameters.AddWithValue("@EmailId", Convert.ToString(cd.EmailId));
                    cmd.Parameters.AddWithValue("@DOB", Convert.ToString(cd.DOB));
                    cmd.Parameters.AddWithValue("@Gender", Convert.ToString(cd.Gender));
                    cmd.Parameters.AddWithValue("@Nationality", Convert.ToString(cd.Nationality));
                    cmd.Parameters.AddWithValue("@MotherTounge", Convert.ToString(cd.MotherTongue));
                    cmd.Parameters.AddWithValue("@Religion", Convert.ToString(cd.Religion));
                    cmd.Parameters.AddWithValue("@CasteCategory", Convert.ToString(cd.CasteCategory));
                    cmd.Parameters.AddWithValue("@Caste", Convert.ToString(cd.Caste));
                    cmd.Parameters.AddWithValue("@NonCreamyLayer", Convert.ToString(cd.NonCreamyLayer));
                    cmd.Parameters.AddWithValue("@State", Convert.ToString(cd.State));


                        cmd.Parameters.Add("@StudentId", SqlDbType.Int);
                        cmd.Parameters["@StudentId"].Direction = ParameterDirection.Output;

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        result = Convert.ToInt16(cmd.Parameters["@StudentId"].Value);
                        conn.Close();
                        return result;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public StudentProfileDetails GetStudentManagementProfileDetails(StudentProfileDetails cd)
        {
            StudentProfileDetails lstStudentDetails = new StudentProfileDetails();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 1);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StudentProfileDetails entitymodel = new StudentProfileDetails();
                            entitymodel.StudentId = Convert.ToInt16(reader["StudentId"]);
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.RegistrationNumber = Convert.ToString(reader["RegistrationNumber"]);
                            entitymodel.NameOfInstitute = Convert.ToString(reader["InstituteName"]);
                         
                            entitymodel.TradeOrCourse = Convert.ToString(reader["Course"]);
                            entitymodel.FatherName = Convert.ToString(reader["FatherName"]);
                            entitymodel.MotherName = Convert.ToString(reader["MotherName"]);
                            entitymodel.FatherOccupation = Convert.ToString(reader["FatherOccupation"]);
                            entitymodel.FatherAnnualIncome = Convert.ToString(reader["FatherAnnualIncome"]);
                            entitymodel.MotherOccupation = Convert.ToString(reader["MotherOccupation"]);
                            entitymodel.MotherAnnualIncome = Convert.ToString(reader["MotherAnnualIncome"]);
                            entitymodel.PrimaryPhone = Convert.ToString(reader["PrimaryPhoneNumber"]);
                            entitymodel.SecondaryPhone = Convert.ToString(reader["SecondaryPhoneNumber"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.DOB = Convert.ToString(reader["DOB"]);
                            entitymodel.Gender = Convert.ToString(reader["Gender"]);
                            entitymodel.Nationality = Convert.ToString(reader["Nationality"]);
                            entitymodel.MotherTongue = Convert.ToString(reader["MotherTounge"]);
                            entitymodel.Religion = Convert.ToString(reader["Religion"]);
                            entitymodel.CasteCategory = Convert.ToString(reader["CasteCategory"]);
                            entitymodel.Caste = Convert.ToString(reader["Caste"]);
                            entitymodel.NonCreamyLayer = Convert.ToString(reader["NonCreamyLayer"]);
                            entitymodel.State = Convert.ToString(reader["State"]);
                           
                            entitymodel.BatchYear = Convert.ToString(reader["YearOfTheBatch"]);

                            lstStudentDetails=entitymodel;
                        }
                        return lstStudentDetails;
                    }
                }
            }
        }



        public IList<StudentSemesterAttendance> GetStudentAttendenceDetails(StudentSemesterAttendance cd)
        {
            IList<StudentSemesterAttendance> lstStudentSemesterAttendance = new List<StudentSemesterAttendance>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentAcademics()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 2);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StudentSemesterAttendance entitymodelSemesterAttendance = new StudentSemesterAttendance();

                            entitymodelSemesterAttendance.SemesterAttendance = Convert.ToString(reader["Semester"]);
                            entitymodelSemesterAttendance.AttendancePecentage = Convert.ToString(reader["Attendence"]);
                            lstStudentSemesterAttendance.Add(entitymodelSemesterAttendance);
                        }
                        return lstStudentSemesterAttendance;
                    }
                }
            }
        }



        public IList<StudentSemesterLeave> GetStudentLeaveDetails(StudentSemesterLeave cd)
        {
            IList<StudentSemesterLeave> lstStudentSemesterLeave = new List<StudentSemesterLeave>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentAcademics()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[Sp_GetStudentManagementRecords]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 3);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StudentSemesterLeave entitymodelSemesterLeave = new StudentSemesterLeave();

                            entitymodelSemesterLeave.SemesterLeave = Convert.ToString(reader["SemesterLeave"]);
                            entitymodelSemesterLeave.LeaveDate = Convert.ToString(reader["LeaveDate"]);
                            lstStudentSemesterLeave.Add(entitymodelSemesterLeave);
                        }
                        return lstStudentSemesterLeave;
                    }
                }
            }
        }


        public IList<StudentSemesterResult> GetStudentResultDetailsS(StudentSemesterResult cd)
        {
            IList<StudentSemesterResult> lstStudentSemesterLeave = new List<StudentSemesterResult>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentAcademics()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 4);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StudentSemesterResult entitymodelSemesterResult = new StudentSemesterResult();

                            entitymodelSemesterResult.ResultSemester = Convert.ToString(reader["Semester"]);
                            entitymodelSemesterResult.ResultDate = Convert.ToString(reader["ResultDate"]);
                            entitymodelSemesterResult.Result = Convert.ToString(reader["Results"]);
                            entitymodelSemesterResult.Reappear = Convert.ToString(reader["Reappear"]);
                            lstStudentSemesterLeave.Add(entitymodelSemesterResult);
                        }
                        return lstStudentSemesterLeave;
                    }
                }
            }
        }




        public ScholorshipDetails GetStudentScholorshipDetails(ScholorshipDetails cd)
        {
            ScholorshipDetails lstStudentDetails = new ScholorshipDetails();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 7);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ScholorshipDetails entitymodel = new ScholorshipDetails();
                            entitymodel.Scholorship = Convert.ToString(reader["Scholarship"]);
                            entitymodel.IsScholorshipOrConcession = Convert.ToString(reader["ScholorshipOrConcession"]);
                            entitymodel.Details = Convert.ToString(reader["Details"]);
                          

                            lstStudentDetails = entitymodel;
                        }
                        return lstStudentDetails;
                    }
                }
            }
        }



        public PlacementDetails GetStudentPlacementDetails(PlacementDetails cd)
        {
            PlacementDetails lstStudentDetails = new PlacementDetails();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 5);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PlacementDetails entitymodel = new PlacementDetails();
                            entitymodel.OrganizationName = Convert.ToString(reader["OrganizationName"]);
                            entitymodel.TypeOfPlacement = Convert.ToString(reader["TypeOfPlacement"]);
                            entitymodel.PlacementThrough = Convert.ToString(reader["PlacementThrough"]);
                            entitymodel.YearOfPlacement = Convert.ToString(reader["YearOfPlacement"]);

                            lstStudentDetails = entitymodel;
                        }
                        return lstStudentDetails;
                    }
                }
            }
        }

        public IList<StudentPenalty> GetStudentPenaltyDetils(StudentPenalty cd)
        {
            IList<StudentPenalty> lstStudentPenalty = new List<StudentPenalty>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentAcademics()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStudentManagementRecords";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", cd.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", cd.LastName);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", cd.RegistrationNumber);
                    cmd.Parameters.AddWithValue("@NameOfInstitute", cd.NameOfInstitute);
                    cmd.Parameters.AddWithValue("@BatchYear", cd.BatchYear);
                    cmd.Parameters.AddWithValue("@TradeOrCourse", cd.TradeOrCourse);
                    cmd.Parameters.AddWithValue("@Type", 6);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StudentPenalty entitymodelStudentPenalty = new StudentPenalty();

                            entitymodelStudentPenalty.PenaltyReason = Convert.ToString(reader["Reason"]);
                            entitymodelStudentPenalty.PenaltyDate = Convert.ToString(reader["PenaltyDate"]);
                            entitymodelStudentPenalty.PenaltyAmount = Convert.ToDecimal(reader["Amount"]);
                            entitymodelStudentPenalty.PenaltyStatus = Convert.ToString(reader["Status"]);
                            entitymodelStudentPenalty.PenaltyUpdatedDate = Convert.ToString(reader["UpdateDate"]);
                            lstStudentPenalty.Add(entitymodelStudentPenalty);
                        }
                        return lstStudentPenalty;
                    }
                }
            }
        }
    }
}
