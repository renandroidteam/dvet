﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
    public class CitizenCharterDB
    {
        public bool AddUSer(Citizencharter obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_InsertUser]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@InstituteTypeId", obj.FKID_InstituteType);
                        cmd.Parameters.AddWithValue("@InstituteId", obj.FKID_InstituteName);
                        cmd.Parameters.AddWithValue("@DepartmentId", obj.FKID_Department);
                        cmd.Parameters.AddWithValue("@EmployeeId", obj.FKID_Employee);
                        cmd.Parameters.AddWithValue("@DOB", obj.DOB);
                        cmd.Parameters.AddWithValue("@Gender", obj.Gender);
                        cmd.Parameters.AddWithValue("@CurrentPositionLocation", obj.CurrentPositionLocation);
                        cmd.Parameters.AddWithValue("@CurrentDepartment", obj.CurrentDepartment);
                        cmd.Parameters.AddWithValue("@AccessValidTill", obj.AccessValidTill);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return true;
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeactivateCitizenUser(Citizencharter citizencharterUser)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_DeacticvateUser]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", citizencharterUser.PKID_User);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                };
            }
        }

        public string AddCitizenCharter(CitizenCharter obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenChareter_InsertCC]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@Year", obj.Year);
                        cmd.Parameters.AddWithValue("@Region", obj.Region);
                        cmd.Parameters.AddWithValue("@Quater", obj.Quater);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        //return true;
                        return "success";
                    };
                }
            }
            catch (Exception ex)
            {
                return "Error occured while updating case:" + ex.Message;
            }
        }


        public IList<CitizenCharter> GetCitizenCharter()
        {
            IList<CitizenCharter> lstyear = new List<CitizenCharter>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_CitizenCharter_GetCC]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CitizenCharter entitymodel = new CitizenCharter();

                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.PKID_CC = Convert.ToInt16(reader["PKID_CC"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);

                            //// entitymodel.Action = Convert.ToBoolean(reader["Action"]);
                            lstyear.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstyear;
            }
        }

        public IList<CitizenCharter> GetYear()
        {
            IList<CitizenCharter> lstyear = new List<CitizenCharter>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_GetYearList]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CitizenCharter entitymodel = new CitizenCharter();

                            entitymodel.Year = Convert.ToString(reader["YearNumber"]);

                            //// entitymodel.Action = Convert.ToBoolean(reader["Action"]);
                            lstyear.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstyear;
            }
        }

        public bool AssignCitizenUserRole(int UserId, string roles)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    //using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_DeleteRoles]", conn))
                    //{
                    //    cmd.CommandType = CommandType.StoredProcedure;
                    //    cmd.Parameters.AddWithValue("@UserId", UserId);

                    //    conn.Open();
                    //    cmd.ExecuteNonQuery();

                    //};

                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_AssignRoles]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        string[] userroles = roles.Split(';');

                        foreach (string role in userroles)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            cmd.Parameters.AddWithValue("@Role", role);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool UnAssignCitizenUserRole(int UserId, string roles)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_DeleteRoles]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        string[] userroles = roles.Split(';');

                        foreach (string role in userroles)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            cmd.Parameters.AddWithValue("@Role", role);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public string AddCitizenComments(CitizenCommentsModel obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_InsertComments]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@Type", obj.Type);
                        cmd.Parameters.AddWithValue("@DocumentId", obj.DocumentId);
                        cmd.Parameters.AddWithValue("@Name", obj.Name);
                        cmd.Parameters.AddWithValue("@Email", obj.Email);

                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        cmd.Parameters.AddWithValue("@Captcha", obj.Captcha);

                        cmd.Parameters.AddWithValue("@Comment", obj.Comment);
                        cmd.Parameters.AddWithValue("@CreatedBy", obj.CreatedBy);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        //return true;
                        return "success";
                    };
                }
            }
            catch (Exception ex)
            {
                return "Error occured while updating case:" + ex.Message;
            }
        }

        public IList<CoreGroupMembers> GetCoreGroupMembers()
        {
            IList<CoreGroupMembers> lstCoreGroupMembers = new List<CoreGroupMembers>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_CitizenCharter_GetCoreGroupMember]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CoreGroupMembers entitymodel = new CoreGroupMembers();

                         //   entitymodel.Date = Convert.ToDateTime(reader["Date"]);
                            entitymodel.CCName = Convert.ToString(reader["CCName"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);
                            entitymodel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            entitymodel.PKID_CoreGroup = Convert.ToInt16(reader["PKID_CoreGroup"]);
                            entitymodel.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                            //// entitymodel.Action = Convert.ToBoolean(reader["Action"]);
                            lstCoreGroupMembers.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCoreGroupMembers;
            }
        }
        
        public IList<TaskMemberForce> GetTaskMemberForce()
        {
            IList<TaskMemberForce> lstTaskMemberForce = new List<TaskMemberForce>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_CitizenCharter_GetTaskMemberForce]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TaskMemberForce entitymodel = new TaskMemberForce();
                            //entitymodel.Date = Convert.ToDateTime(reader["Date"]);
                            entitymodel.CCName = Convert.ToString(reader["CitizenName"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);
                            entitymodel.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                            entitymodel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            entitymodel.PKID_TaskForce = Convert.ToInt16(reader["PKID_TaskForce"]);
                            lstTaskMemberForce.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstTaskMemberForce;
            }
        }
        
        public IList<CitizenComment> GetCitizenComment(CitizenCommentsModel citizencommentsmodel)
        {
            IList<CitizenComment> lstCitizenComment = new List<CitizenComment>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_CitizenCharter_GetComments]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", citizencommentsmodel.Type);
                    cmd.Parameters.AddWithValue("@docid", citizencommentsmodel.DocumentId);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CitizenComment entitymodel = new CitizenComment();

                            entitymodel.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);
                            entitymodel.MobileNumber = Convert.ToString(reader["MobileNumber"]);
                            entitymodel.Comment = Convert.ToString(reader["Comment"]);
                            lstCitizenComment.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCitizenComment;
            }
        }
        
        public IList<Master_Employee> GetEmployeeDetails(int pKID_Employee)
        {
            List<Master_Employee> lstEmployees = new List<Master_Employee>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Employee_GetEmployee]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmployeeId", pKID_Employee);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Employee entitymodel = new Master_Employee();
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                            entitymodel.PKID_Employee = Convert.ToInt16(reader["PKID_Employee"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);

                            entitymodel.Age = Convert.ToInt16(reader["Age"]);
                            entitymodel.DepartmentId = Convert.ToInt16(reader["DepartmentId"]);
                            entitymodel.Gender = Convert.ToString(reader["Gender"]);

                            lstEmployees.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstEmployees;

            }
        }

        public void DeleteTaskForceMember(int pKID_TaskForce)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_TaskForce_DeleteMember]", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_TaskForce", pKID_TaskForce);
                    cmd.ExecuteNonQuery();
                    conn.Close();

                };
            }
        }

        public IList<Master_Employee> GetEmployeeByDepartment(int departmentId)
        {
            List<Master_Employee> lstEmployees = new List<Master_Employee>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_GetEmployeeByDepartment]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DepartmentId", departmentId);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Employee entitymodel = new Master_Employee();
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                            entitymodel.PKID_Employee = Convert.ToInt16(reader["PKID_Employee"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);

                            entitymodel.DOB = Convert.ToString(reader["DOB"]);
                            entitymodel.Posting = Convert.ToString(reader["PostingLocation"]);

                            entitymodel.Age = reader["Age"] == DBNull.Value ? 0 : Convert.ToInt16(reader["Age"]);
                            entitymodel.DepartmentId = reader["DepartmentId"] == DBNull.Value ? 0 : Convert.ToInt16(reader["DepartmentId"]);
                            entitymodel.Gender = Convert.ToString(reader["Gender"]);
                           

                            lstEmployees.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstEmployees;
            }
        }

       

        public IList<Master_Institute> GetInstituteNames(string type)
        {
            List<Master_Institute> lstEmployees = new List<Master_Institute>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_GetInstituteNames]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", type);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Institute entitymodel = new Master_Institute();
                            entitymodel.InstituteName = Convert.ToString(reader["InstituteName"]);
                            entitymodel.PKID_InstituteName = Convert.ToInt16(reader["PKID_InstituteName"]);
                            lstEmployees.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstEmployees;
            }
        }

        public IList<CitizenCharter_UserRole> GetCitizenCharterUsersRoleByUserId(int pKID_User)
        {
            List<CitizenCharter_UserRole> lstRoles = new List<CitizenCharter_UserRole>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_GetUSerRoles]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_User", pKID_User);
                    cmd.Parameters.AddWithValue("@Type", 1);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CitizenCharter_UserRole entitymodel = new CitizenCharter_UserRole();
                            //entitymodel.Active = Convert.ToDateTime(reader["AccessValidTill"]);// reader["AccessValidTill"] == DBNull.Value ? (DateTime)null : Convert.ToDateTime(reader["AccessValidTill"]);
                            entitymodel.PKID_Role = Convert.ToInt16(reader["PKID_Role"]);
                            entitymodel.RoleName = Convert.ToString(reader["RoleName"]);
                            lstRoles.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstRoles;
            }
        }

        public IList<CitizenCharter_UserRole> GetCitizenRoles()
        {
            List<CitizenCharter_UserRole> lstRoles = new List<CitizenCharter_UserRole>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_GetUSerRoles]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_User", 0);
                    cmd.Parameters.AddWithValue("@Type", 2);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CitizenCharter_UserRole entitymodel = new CitizenCharter_UserRole();

                            entitymodel.PKID_Role = Convert.ToInt16(reader["PKID_Role"]);
                            entitymodel.RoleName = Convert.ToString(reader["RoleName"]);
                            entitymodel.Active = Convert.ToBoolean(reader["Active"]);
                            lstRoles.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstRoles;
            }
        }

        public List<Citizencharter> GetCitizenCharterUsers(int DepartmentId)
        {
            List<Citizencharter> lstCitizenCharterUsers = new List<Citizencharter>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CitizenCharter_GetUSer]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DepartmentId", DepartmentId);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Citizencharter entitymodel = new Citizencharter();
                            entitymodel.AccessValidTill = Convert.ToString(reader["AccessValidTill"]);// reader["AccessValidTill"] == DBNull.Value ? (DateTime)null : Convert.ToDateTime(reader["AccessValidTill"]);
                            entitymodel.DOB = Convert.ToString(reader["DOB"]);
                            entitymodel.FKID_Department = Convert.ToInt16(reader["FKID_Department"]);
                            entitymodel.FKID_Employee = Convert.ToInt16(reader["FKID_Employee"]);
                            entitymodel.FKID_InstituteName = Convert.ToInt16(reader["FKID_InstituteName"]);
                            entitymodel.FKID_InstituteType = Convert.ToInt16(reader["FKID_InstituteType"]);

                            entitymodel.Gender = reader["Gender"] == DBNull.Value ? 0 : Convert.ToInt16(reader["Gender"]);
                            entitymodel.PKID_User = reader["PKID_User"] == DBNull.Value ? 0 : Convert.ToInt16(reader["PKID_User"]);
                            // entitymodel. = Convert.ToString(reader["Gender"]);
                            //entitymodel.EmployeeName = Convert.ToString(reader["EmployeeName"]);
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            lstCitizenCharterUsers.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstCitizenCharterUsers;
            }
        }

        public void DeleteCoreGroupMember(int pKID_CoreGroup)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CoreGroup_DeleteMember]", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_CoreGroup", pKID_CoreGroup);
                    cmd.ExecuteNonQuery();
                    conn.Close();

                };
            }
        }

        public void UpdateTaskForceMember(TaskMemberForce member)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_TaskForce_UpdateMember]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_TaskForce", member.PKID_TaskForce);
                   // cmd.Parameters.AddWithValue("@Date", member.Date);
                    cmd.Parameters.AddWithValue("@Name", member.Name);
                    cmd.Parameters.AddWithValue("@CCName", member.CCName);
                    cmd.Parameters.AddWithValue("@Designation", member.Designation);
                    cmd.Parameters.AddWithValue("@Department", member.Department);
                    cmd.Parameters.AddWithValue("@EmailAddress", member.EmailAddress);
                    cmd.Parameters.AddWithValue("@ModifiedBy", member.CreatedBy);
                    cmd.Parameters.AddWithValue("@ModifiedDate", DateTime.Now);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    // return true;
                };
            }
        }

        public bool AddTaskForceMember(TaskMemberForce member)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_TaskForce_InsertMember]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //   cmd.Parameters.AddWithValue("@PKID_TaskForce", member.PKID_TaskForce);
                   // cmd.Parameters.AddWithValue("@Date", member.Date);
                    cmd.Parameters.AddWithValue("@CCName", member.CCName);
                    cmd.Parameters.AddWithValue("@Name", member.Name);
                    cmd.Parameters.AddWithValue("@Designation", member.Designation);
                    cmd.Parameters.AddWithValue("@Department", member.Department);
                    cmd.Parameters.AddWithValue("@EmailAddress", member.EmailAddress);
                    cmd.Parameters.AddWithValue("@CreatedBy", member.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                };
            }
        }

        public void UpdateCoreGroupMember(CoreGroupMembers member)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CoreMember_UpdateMember]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_CoreGroup", member.PKID_CoreGroup);
                 //   cmd.Parameters.AddWithValue("@Date", member.Date);
                    cmd.Parameters.AddWithValue("@Name", member.Name);
                    cmd.Parameters.AddWithValue("@CCName", member.CCName);
                    cmd.Parameters.AddWithValue("@Designation", member.Designation);
                    cmd.Parameters.AddWithValue("@Department", member.Department);
                    cmd.Parameters.AddWithValue("@EmailAddress", member.EmailAddress);
                    cmd.Parameters.AddWithValue("@ModifiedBy", member.CreatedBy);
                    cmd.Parameters.AddWithValue("@ModifiedDate", DateTime.Now);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    // return true;
                };
            }
        }

        public bool AddCoreGroupMember(CoreGroupMembers member)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_CoreGroup_InsertMember]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                    cmd.Parameters.AddWithValue("@CCName", member.CCName);
                    cmd.Parameters.AddWithValue("@Name", member.Name);
                    cmd.Parameters.AddWithValue("@Designation", member.Designation);
                    cmd.Parameters.AddWithValue("@Department", member.Department);
                    cmd.Parameters.AddWithValue("@EmailAddress", member.EmailAddress);
                    cmd.Parameters.AddWithValue("@CreatedBy", member.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                };
            }
        }
    }
}
