﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
    public class FieldInspectionDB
    {
        public IList<FIInspectionOrAuditType> GetInspectionOrAuditTypes(FIInspectionOrAuditType obj)
        {
            IList<FIInspectionOrAuditType> lstInspectionOrAuditType = new List<FIInspectionOrAuditType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_FI_GetInspectionOrAuditType]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@InspectionOrAudit", obj.IsAuditType);

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FIInspectionOrAuditType entitymodel = new FIInspectionOrAuditType();

                            entitymodel.PKID_InspectionOrAuditType = Convert.ToInt16(reader["PKID_InspectionOrAuditType"]);
                            entitymodel.InspectionOrAuditTypeName = Convert.ToString(reader["InspectionOrAuditTypeName"]);
                            entitymodel.IsAuditType = Convert.ToBoolean(reader["IsAuditType"]);
                            entitymodel.Active = Convert.ToBoolean(reader["Active"]);

                            lstInspectionOrAuditType.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstInspectionOrAuditType;
            }
        }

        public IList<FI_EventType> GetEventType()
        {
            IList<FI_EventType> lstentity = new List<FI_EventType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_FI_GetEventType]";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FI_EventType entitymodel = new FI_EventType();

                            entitymodel.PKID_EventType = Convert.ToInt16(reader["PKID_EventType"]);
                            entitymodel.EventTypeName = Convert.ToString(reader["EventTypeName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public string AddOrUpdateInspection(FIInspectionOrAudit cd)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_FI_SetInspection]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                        NewFIId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewFIId);

                        cmd.Parameters.AddWithValue("@Type", cd.Type);
                        cmd.Parameters.AddWithValue("@EventType", cd.EventType);
                        cmd.Parameters.AddWithValue("@InspectionType", cd.InspectionOrAuditType);
                        cmd.Parameters.AddWithValue("@InspectionId", cd.InspectionOrAuditId);
                        cmd.Parameters.AddWithValue("@InspectionFromDate", Convert.ToDateTime(cd.InspectionOrAuditFromDate));
                        cmd.Parameters.AddWithValue("@InspectionToDate", Convert.ToDateTime(cd.InspectionOrAuditToDate));
                        //cmd.Parameters.AddWithValue("@PKID_Inspection", cd.PKID_InspectionOrAudit);
                        cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewFIId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding/updating Inspection:" + ex.Message;
            }
        }

        public string AddOrUpdateAudit(FIInspectionOrAudit cd)
        {
            try
            {
                IList<FIInspectionOrAudit> objPay = new List<FIInspectionOrAudit>();

                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_FI_SetAudit]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                        NewFIId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewFIId);

                        cmd.Parameters.AddWithValue("@Type", cd.Type);
                        cmd.Parameters.AddWithValue("@EventType", cd.EventType);
                        cmd.Parameters.AddWithValue("@AuditType", cd.InspectionOrAuditType);
                        cmd.Parameters.AddWithValue("@AuditId", cd.InspectionOrAuditId);
                        cmd.Parameters.AddWithValue("@AuditFromDate", cd.InspectionOrAuditFromDate);
                        cmd.Parameters.AddWithValue("@AuditToDate", cd.InspectionOrAuditToDate);
                        //cmd.Parameters.AddWithValue("@PKID_Audit", cd.PKID_InspectionOrAudit);
                        cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return Convert.ToString(NewFIId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding/updating Audit:" + ex.Message;
            }
        }

        /* public string UpdateInspection(FIInspection cd)
         {
             //int res = 0;
             try
             {
                 using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                 {
                     conn.Open();
                     using (var cmd = conn.CreateCommand())
                     {
                         cmd.CommandText = "[dbo].[sp_FI_SetInspection]";
                         cmd.CommandType = CommandType.StoredProcedure;
                         SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                         NewFIId.Direction = ParameterDirection.Output;

                         cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 2;
                         cmd.Parameters.Add("@EventType", SqlDbType.NVarChar).Value = cd.EventType;
                         cmd.Parameters.Add("@InspectionType", SqlDbType.NVarChar).Value = cd.InspectionType;
                         cmd.Parameters.Add("@InspectionName", SqlDbType.NVarChar).Value = cd.InspectionName;
                         cmd.Parameters.Add("@InspectionFromDate", SqlDbType.DateTime).Value = cd.InspectionFromDate;
                         cmd.Parameters.Add("@InspectionToDate", SqlDbType.DateTime).Value = cd.InspectionToDate;
                         cmd.Parameters.Add("@PKID_AddNewInspection", SqlDbType.Int).Value = cd.PKID_AddNewInspection;

                         cmd.ExecuteNonQuery();
                         return Convert.ToString(NewFIId.Value);
                     }
                 }
             }
             catch (Exception ex)
             {
                 return "Error occured while updating case:" + ex.Message;
             }
         }*/

        public string AddOrUpdateRegisterdesk(FIRegisterDesk cd)
        {
            IList<FIRegisterDesk> objPay = new List<FIRegisterDesk>();
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[sp_FI_SetRegisterDesk]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                        NewFIId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewFIId);

                        cmd.Parameters.AddWithValue("@Type", cd.Type);
                        cmd.Parameters.AddWithValue("@LetterReferencenumber", cd.LetterReferencenumber);
                        cmd.Parameters.AddWithValue("@SenderName", cd.SenderName);
                        cmd.Parameters.AddWithValue("@Desknumber", cd.Desknumber);
                        cmd.Parameters.AddWithValue("@SpeedpostNumber", cd.SpeedpostNumber);
                        cmd.Parameters.AddWithValue("@Subject", cd.Subject);
                        cmd.Parameters.AddWithValue("@Receipent", cd.Receipent);
                        cmd.Parameters.AddWithValue("@OutwardData", cd.OutwardData);
                        cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                        cmd.Parameters.AddWithValue("@RegisterDeskId", cd.RegisterDeskId);

                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return Convert.ToString(NewFIId.Value);

                    }

                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding/updating Register Desk:" + ex.Message;
            }
        }

        /* public string UpdateRegisterdesk(FIRegisterDesk cd)
         {
             try
             {
                 using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                 {
                     conn.Open();
                     using (var cmd = conn.CreateCommand())
                     {
                         cmd.CommandText = "[dbo].[sp_FI_SetRegisterDesk]";
                         cmd.CommandType = CommandType.StoredProcedure;
                         SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                         NewFIId.Direction = ParameterDirection.Output;
                         cmd.Parameters.Add(NewFIId);

                         cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 2;
                         cmd.Parameters.Add("@LetterReferencenumber", SqlDbType.Int).Value = cd.LetterReferencenumber;
                         cmd.Parameters.Add("@SenderName", SqlDbType.NVarChar).Value = cd.SenderName;
                         cmd.Parameters.Add("@Desknumber", SqlDbType.Int).Value = cd.Desknumber;
                         cmd.Parameters.Add("@SpeedpostNumber", SqlDbType.Int).Value = cd.SpeedpostNumber;
                         cmd.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = cd.Subject;
                         cmd.Parameters.Add("@Receipent", SqlDbType.NVarChar).Value = cd.Receipent;
                         cmd.Parameters.Add("@OutwardData", SqlDbType.NVarChar).Value = cd.OutwardData;

                         cmd.ExecuteNonQuery();
                         conn.Close();
                         return Convert.ToString(NewFIId.Value);
                     }
                 }
             }
             catch (Exception ex)
             {
                 return "Error occured while adding case:" + ex.Message;
             }
         }
         */

        public IList<FI_AnnexureC> InsertAnnexureC(FI_AnnexureC cd)
        {
            IList<FI_AnnexureC> objPay = new List<FI_AnnexureC>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_FI_AnnexureC_Details]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PKID_AnnexureC", Convert.ToInt16(cd.PKID_AnnexureC));
                    cmd.Parameters.AddWithValue("@NameOfITI", cd.NameOfITI);
                    cmd.Parameters.AddWithValue("@NameOfTrade", cd.NameOfTrade);
                    cmd.Parameters.AddWithValue("@SpacePerUnitPerShift", Convert.ToInt16(cd.SpacePerUnitPerShift));
                    cmd.Parameters.AddWithValue("@PowerPerUnitPerShift", Convert.ToInt16(cd.PowerPerUnitPerShift));
                    cmd.Parameters.AddWithValue("@Remarks", cd.Remarks);
                    cmd.Parameters.AddWithValue("@RemarksByITI", cd.RemarksByITI);
                    cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                    //cmd.Parameters.AddWithValue("@CreatedDate", Convert.ToDateTime(cd.CreatedDate));
                    cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                    //cmd.Parameters.AddWithValue("@ModifiedDate", Convert.ToDateTime(cd.ModifiedDate));
                    cmd.ExecuteNonQuery();
                }
            }
            return objPay;
        }
        
        public IList<FIInspectionOrAudit> GetSearchDetails(FIInspectionOrAudit obj)
        {
            IList<FIInspectionOrAudit> lstentity = new List<FIInspectionOrAudit>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[sp_FI_GetSearchDetails]";
                    cmd.Parameters.AddWithValue("@Type", obj.Type);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FIInspectionOrAudit entitymodel = new FIInspectionOrAudit();
                            entitymodel.PKID_InspectionOrAudit = Convert.ToInt16(reader["PKID"]);
                            entitymodel.InspectionOrAuditId = Convert.ToString(reader["InspectionOrAuditId"]);
                            entitymodel.EventType = Convert.ToString(reader["EventType"]);
                            entitymodel.InspectionOrAuditType = Convert.ToInt16(reader["InspectionOrAuditType"]);
                            entitymodel.InspectionNameOrAuditName = Convert.ToString(reader["InspectionNameOrAuditBy"]);
                            entitymodel.InspectionOrAuditFromDate = Convert.ToString(reader["InspectionOrAuditFromDate"]);
                            entitymodel.InspectionOrAuditToDate = Convert.ToString(reader["InspectionOrAuditToDate"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
                return lstentity;
            }
        }

        public string AddOrUpdateAuditParas(FIAuditParas cd)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_FI_SetAuditParas]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                        NewFIId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewFIId);

                        cmd.Parameters.AddWithValue("@Type", cd.Type);
                        cmd.Parameters.AddWithValue("@AuditType", cd.AuditType);
                        cmd.Parameters.AddWithValue("@AuditParasId", cd.AuditParasId);
                        cmd.Parameters.AddWithValue("@AuditBy", cd.AuditBy);
                        cmd.Parameters.AddWithValue("@AuditFromDate", cd.AuditFromDate);
                        cmd.Parameters.AddWithValue("@AuditToDate", cd.AuditToDate);
                        cmd.Parameters.AddWithValue("@Decision", cd.Decision);
                        cmd.Parameters.AddWithValue("@Remarks", cd.Remarks);
                        cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewFIId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding audit paras:" + ex.Message;
            }
        }
        
        public string AddOrUpdateAuditParasDetails(FIAuditParasDetails cd)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_FI_SetAuditParasDetails]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter NewFIId = new SqlParameter("@NewFIId", SqlDbType.NVarChar, 50);
                        NewFIId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(NewFIId);

                        cmd.Parameters.AddWithValue("@Type", cd.Type);
                        cmd.Parameters.AddWithValue("@AuditParaId", cd.AuditParaId);
                        cmd.Parameters.AddWithValue("@InstituteName", cd.InstituteName);
                        cmd.Parameters.AddWithValue("@InstituteInchargeIn", cd.InstituteInchargeIn);
                        cmd.Parameters.AddWithValue("@InstituteInchargeEmail", cd.InstituteInchargeEmail);
                        cmd.Parameters.AddWithValue("@AuditParaHeading", cd.AuditParaHeading);
                        cmd.Parameters.AddWithValue("@AuditParaDetails", cd.AuditParaDetails);
                        cmd.Parameters.AddWithValue("@InstitueJustification", cd.InstitueJustification);
                        cmd.Parameters.AddWithValue("@Remarks", cd.Remarks);
                        cmd.Parameters.AddWithValue("@CreatedBy", cd.CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", cd.ModifiedBy);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(NewFIId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error occured while adding audit para details:" + ex.Message;
            }
        }

        public IList<FIInspectionCommiteeData> GetInspectionCommiteeData()
        {
            IList<FIInspectionCommiteeData> lstentity = new List<FIInspectionCommiteeData>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_FI_GetInspectionCommiteeData]";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FIInspectionCommiteeData entitymodel = new FIInspectionCommiteeData();

                            entitymodel.AnnexureId = Convert.ToInt16(reader["AnnexureId"]);
                            entitymodel.AnnexureName = Convert.ToString(reader["AnnexureName"]);
                            entitymodel.status = Convert.ToString(reader["status"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }
    }
}
