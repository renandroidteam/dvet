﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Master
{
    public interface IWorkflowDL<TEntity, in TPrimaryKey> where TEntity : class
    {
        void WFIntiation(WFinitiationModel WFmodel);
        void WFAction(WFServiceModel WFmodel);
        WorkflowInstanceDetailsModel GetWorkflowDetails(WorkflowInstanceDetailsModel model);
    }
    public interface ITaskDL<TEntity, in TPrimaryKey> where TEntity : class
    {
        IList<TaskResponseModel> GetTaskDetails(TaskRequestModel taskmodel);
    }
    public interface IWorkFlowComments<TEntity, in TPrimaryKey> where TEntity : class
    {
        IList<WFCommentsModel> WFComments(int RequestId);
    }
}
