﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Master
{
    public class StackHolderDB
    {
        public string AddQuestions(StackHolderAddQuestions obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_CreateQuestion]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                        cmd.Parameters.AddWithValue("@Questions", Convert.ToString(obj.Questions));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(obj.CreatedBy));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    }
                }
                return Convert.ToString(obj.Questions);
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string AddSurvey(StackHolderSurvey obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_CreateSurveyForm]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter oldsurvey = new SqlParameter("@oldsurvey", SqlDbType.NVarChar, 50);
                        oldsurvey.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(oldsurvey);
                        cmd.Parameters.AddWithValue("@SurveyName", Convert.ToString(obj.SurveyName));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(obj.CreatedBy));
                        if (obj.StartDate == DateTime.Parse("1/1/0001 12:00:00 AM"))
                        {
                            DateTime? dt = null;
                            cmd.Parameters.AddWithValue("@StartDate", dt);
                        }
                        else
                            cmd.Parameters.AddWithValue("@StartDate", Convert.ToDateTime(obj.StartDate));

                        if (obj.StartDate == DateTime.Parse("1/1/0001 12:00:00 AM"))
                        {
                            DateTime? dt = null;
                            cmd.Parameters.AddWithValue("@Endate", dt);
                        }
                        else
                            cmd.Parameters.AddWithValue("@Endate", Convert.ToDateTime(obj.EndDate));

                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        return Convert.ToString(oldsurvey.Value);
                    }
                }

            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string AddStackHolder(StackHolderFeedback shf)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_Feedback]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@ApplicantName", Convert.ToString(shf.ApplicantName));
                        cmd.Parameters.AddWithValue("@UserType", Convert.ToString(shf.UserType));
                        cmd.Parameters.AddWithValue("@EmailId", Convert.ToString(shf.EmailId));
                        cmd.Parameters.AddWithValue("@MobileNo", Convert.ToString(shf.MobileNo));
                        cmd.Parameters.AddWithValue("@ReferenceNo", Convert.ToString(shf.ReferenceNo));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(shf.CreatedBy));
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    }
                }
                return Convert.ToString(shf.ApplicantName);
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string AddMultiQuestions(StackHolderAddQuestions item)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_CreateQuestion]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.AddWithValue("@Questions", Convert.ToString(item.Questions));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(item.CreatedBy));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    }
                }
                return Convert.ToString(item.Questions);
            }
            catch (Exception ex)
            {
                return "Error occured while adding case:" + ex.Message;
            }
        }

        public string UpdateQuestions(StackHolderAddQuestions obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_CreateQuestion]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                        cmd.Parameters.AddWithValue("@QuestionId", Convert.ToString(obj.QuestionId));
                        cmd.Parameters.AddWithValue("@Questions", Convert.ToString(obj.Questions));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(obj.CreatedBy));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    }
                }
                return Convert.ToString(obj.Questions);
            }
            catch (Exception ex)
            {
                return "Error occured while updating case:" + ex.Message;
            }
        }
        public string DeleteQuestions(StackHolderAddQuestions obj)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_StackHolder_DeleteQuestion]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@QuestionId", Convert.ToString(obj.QuestionId));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(obj.CreatedBy));
                        //cmd.Parameters.AddWithValue("@IsActive", 0);
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    }
                }
                return Convert.ToString(obj.Questions);
            }
            catch (Exception ex)
            {
                return "Error occured while updating case:" + ex.Message;
            }
        }
        public IList<StackHolderAddQuestions> GetQuestions(int SurveyId)
        {
            IList<StackHolderAddQuestions> lstCaseComment = new List<StackHolderAddQuestions>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "USP_StackHolder_GetQuestions";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SurveyId", SurveyId);
                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderAddQuestions entitymodel = new StackHolderAddQuestions();
                            entitymodel.SurveyName = Convert.ToString(reader["SurveyName"]);
                            entitymodel.Questions = Convert.ToString(reader["Question"]);
                            entitymodel.QuestionId = Convert.ToInt16(reader["QuestionId"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.IsActive = Convert.ToBoolean(reader["IsActive"]);

                            lstCaseComment.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseComment;
            }
        }

        public IList<StackHolderSurvey> GetSurvey()
        {
            IList<StackHolderSurvey> lstsurvey = new List<StackHolderSurvey>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "USP_StackHolder_GetSurvey";
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderSurvey entitymodel = new StackHolderSurvey();

                            entitymodel.SurveyName = Convert.ToString(reader["SurveyName"]);
                            entitymodel.SurveyId = Convert.ToInt16(reader["Id"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            entitymodel.IsActive = Convert.ToBoolean(reader["IsActive"]);

                            lstsurvey.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstsurvey;
            }
        }

        public IList<StackHolderUserType> GetUserType()
        {
            IList<StackHolderUserType> lstCaseComment = new List<StackHolderUserType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo.sp_StackHolder_GetUserType]";
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderUserType entitymodel = new StackHolderUserType();

                            entitymodel.PKID_UT = Convert.ToInt16(reader["PKID_UT"]);
                            entitymodel.UserType = Convert.ToString(reader["UserType"]);
                            lstCaseComment.Add(entitymodel);
                        }

                    }
                    conn.Close();
                }
                return lstCaseComment;
            }
        }



        public List<StackHolderSurvey> GetStakeHoldersSurveyDetails()
        {
            List<StackHolderSurvey> lstentity = new List<StackHolderSurvey>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_StakeHoldersSurveyDetails";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderSurvey entityModel = new StackHolderSurvey();

                            entityModel.SurveyName = Convert.ToString(reader["SurveyName"]);
                            entityModel.SurveyId = Convert.ToInt32(reader["SurveyId"]);
                           // entityModel.Remarks = Convert.ToString(reader["Remarks"]);


                            lstentity.Add(entityModel);
                        }
                    }
                    return lstentity;
                }
            }

        }



        public List<StackHolderFeedbackDetails> GetStackHolderFeedbackDetails(StackHolderFeedbackDetails obj)
        {
            List<StackHolderFeedbackDetails> lstentity = new List<StackHolderFeedbackDetails>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStakeHoldersFeedBackDetails";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderFeedbackDetails entityModel = new StackHolderFeedbackDetails();

                            entityModel.SurveyId = Convert.ToInt32(reader["SurveyId"]);
                            entityModel.QuetionId = Convert.ToInt32(reader["QuestionId"]);
                            entityModel.SurveyName = Convert.ToString(reader["SurveyName"]);
                            entityModel.QuestionName = Convert.ToString(reader["Question"]);
                            entityModel.Excellent = Convert.ToInt32(reader["Excellent"]);
                            entityModel.VeryGood = Convert.ToInt32(reader["VeryGood"]);
                            entityModel.Satisfactory = Convert.ToInt32(reader["Satisfactory"]);
                            entityModel.Average = Convert.ToInt32(reader["Average"]);
                            entityModel.Poor = Convert.ToInt32(reader["Poor"]);
                            lstentity.Add(entityModel);
                        }
                    }
                    return lstentity;
                }
            }

        }


        public List<StackHolderFeedBackSurvey> GetStackHolderSurveykDetails(StackHolderFeedbackDetails obj)
        {
            List<StackHolderFeedBackSurvey> lstentity = new List<StackHolderFeedBackSurvey>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "Sp_GetStake_Holders_Survey_Details";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StackHolderFeedBackSurvey entityModel = new StackHolderFeedBackSurvey();


                            entityModel.SurveyId = Convert.ToInt32(reader["SurveyId"]);
                            entityModel.SurveyName = Convert.ToString(reader["SurveyName"]);

                            lstentity.Add(entityModel);
                        }
                    }
                    return lstentity;
                }
            }

        }




        public List<StackHoldersrespondantDetails> GetStackHolderRespondantDetails(StackHoldersrespondantDetails obj)
        {
            List<StackHoldersrespondantDetails> lstentity = new List<StackHoldersrespondantDetails>();
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_GetRespondantDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                        cmd.Parameters.AddWithValue("@QuestionId", obj.QuestionId);
                        cmd.Parameters.AddWithValue("@QuestionType",obj.QuestionType);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                StackHoldersrespondantDetails entityModel = new StackHoldersrespondantDetails();
                                entityModel.SurveyId = Convert.ToInt32(reader["SurveyId"]);
                              //  entityModel.QuestionId = Convert.ToInt32(reader["QuestionId"]);
                                entityModel.QuestionName = Convert.ToString(reader["Question"]);
                                entityModel.RespondentName = Convert.ToString(reader["RespondentName"]);
                                entityModel.Remarks = Convert.ToString(reader["Remarks"]);
                                lstentity.Add(entityModel);
                            }
                        }
                        return lstentity;
                    }
                }

            }
             

            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<StackHoldersrespondantDetails> GetStackHolderRemarksDetails(StackHoldersrespondantDetails obj)
        {
            List<StackHoldersrespondantDetails> lstentity = new List<StackHoldersrespondantDetails>();
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Sp_GetSurveyRemarksDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SurveyId", obj.SurveyId);
                       
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                StackHoldersrespondantDetails entityModel = new StackHoldersrespondantDetails();
                                entityModel.SurveyId = Convert.ToInt32(reader["SurveyId"]);
                                entityModel.SurveyName = Convert.ToString(reader["SurveyName"]);
                                entityModel.RespondentName = Convert.ToString(reader["RespondantName"]);
                                entityModel.Remarks = Convert.ToString(reader["Remarks"]);
                                lstentity.Add(entityModel);
                            }
                        }
                        return lstentity;
                    }
                }

            }


            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
