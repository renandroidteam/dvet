﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Master
{
    public class WorkFlowComments : IWorkFlowComments<WFCommentsModel, int>
    {

        public IList<WFCommentsModel> WFComments(int RequestId)
        {
            IList<WFCommentsModel> lstEntity = new List<WFCommentsModel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd1 = conn.CreateCommand())
                {
                    cmd1.CommandText = "usp_GetWFComments";
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@RequestId", RequestId);


                    SqlDataReader reader1 = cmd1.ExecuteReader();
                    while (reader1.Read())
                    {
                        WFCommentsModel objSubEntity = new WFCommentsModel();

                        objSubEntity.AssignedTo = Convert.ToString(reader1["AssignedTo"]);
                        objSubEntity.Comments = Convert.ToString(reader1["Comments"]);
                        objSubEntity.CreatedDate = Convert.ToString(reader1["CreatedDate"]);
                        lstEntity.Add(objSubEntity);
                    }
                    reader1.Close();


                }
            }
            return lstEntity;
        }
    }
    public class TaskDL : ITaskDL<TaskResponseModel, int>
    {
        public IList<TaskResponseModel> GetTaskDetails(TaskRequestModel taskmodel)
        {
            IList<TaskResponseModel> lstTask = new List<TaskResponseModel>();
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "GetWorkflowEFtasklist";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFID", Convert.ToString(taskmodel.WFInstanceId));
                        cmd.Parameters.AddWithValue("@AssignedTo", Convert.ToString(taskmodel.LoggedInUser));
                        cmd.Parameters.AddWithValue("@StatusCode", Convert.ToString(taskmodel.StatusCode));

                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            TaskResponseModel objEntity = new TaskResponseModel();
                            objEntity.WFInstanceID = Convert.ToInt16(reader["WFInstanceID"]);
                            objEntity.RequestID = Convert.ToInt16(reader["RequestID"]);
                            objEntity.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            objEntity.Subject = Convert.ToString(reader["Subject"]);
                            objEntity.CApplication = Convert.ToString(reader["CApplication"]);
                            objEntity.WorkflowStatus = Convert.ToString(reader["WorkflowStatus"]);
                            objEntity.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            objEntity.SubWFCode = Convert.ToString(reader["SubWFCode"]);
                            objEntity.WFCode = Convert.ToString(reader["WFCode"]);
                            objEntity.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                            lstTask.Add(objEntity);
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstTask;
        }
    }
    public class WorkflowDL : IWorkflowDL<WFinitiationModel, int>
    {
        public void WFIntiation(WFinitiationModel WFmodel)
        {
            try
            {
                int hdId = 0;
                var resltCpId = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();

                    string reslt = "";
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "WorkflowEFGeneralInitiation";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToString(WFmodel.RequestId));
                        cmd.Parameters.AddWithValue("@WorkflowCode", Convert.ToString(WFmodel.WFCode));
                        cmd.Parameters.AddWithValue("@WorkflowDCode", Convert.ToString(WFmodel.WFSubCode));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(WFmodel.CreatedBy));
                        cmd.Parameters.AddWithValue("@RequestNo", Convert.ToString(WFmodel.RequestNo));
                        cmd.ExecuteNonQuery();


                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void WFAction(WFServiceModel WFmodel)
        {
            try
            {

                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();


                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "proc_WFEFProcess";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToInt16(WFmodel.RequestID));
                        cmd.Parameters.AddWithValue("@WorkflowInstanceId", Convert.ToInt16(WFmodel.WorkflowInstanceId));
                        cmd.Parameters.AddWithValue("@Comments", Convert.ToString(WFmodel.Comments));
                        cmd.Parameters.AddWithValue("@CreatedBY", Convert.ToString(WFmodel.CreatedBY));
                        cmd.Parameters.AddWithValue("@ActionCode", Convert.ToString(WFmodel.ActionCode));
                        cmd.Parameters.AddWithValue("@ProcessID", Convert.ToInt16(WFmodel.ProcessID));
                        cmd.Parameters.AddWithValue("@WFCode", Convert.ToString(WFmodel.WFCode));
                        cmd.Parameters.AddWithValue("@StateID", Convert.ToInt16(WFmodel.StateID));
                        cmd.ExecuteNonQuery();


                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public WorkflowInstanceDetailsModel GetWorkflowDetails(WorkflowInstanceDetailsModel model)
        {
            WorkflowInstanceDetailsModel obj = new WorkflowInstanceDetailsModel();
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "GetWorkFlowDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFInstanceId ", Convert.ToString(model.WFInstanceID));

                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            obj.WFInstanceID = Convert.ToInt32(reader["WFInstanceID"]);
                            obj.RequestID = Convert.ToInt32(reader["RequestID"]);
                            //obj.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                            //obj.WFStatus = Convert.ToString(reader["WFStatus"]);
                            //obj.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                            //obj.Comments = Convert.ToString(reader["Comments"]);
                            obj.WFCode = Convert.ToString(reader["WFCode"]);
                            obj.ProcessId = Convert.ToInt32(reader["ProcessId"]);
                            obj.ActionId = Convert.ToInt32(reader["ActionId"]);
                            //obj.TransitionId = Convert.ToInt32(reader["TransitionId"]);
                            obj.StateId = Convert.ToInt32(reader["StateId"]);
                            //obj.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            //obj.WDId = Convert.ToInt32(reader["WDId"]);
                            //obj.RequestNo = Convert.ToString(reader["RequestNo"]);

                        }
                        reader.Close();


                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return obj;
        }
    }
}