﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
    public class eOfficeDB
    {
        public IList<eOffice> GetDocumentType()
        {
            IList<eOffice> lstentity = new List<eOffice>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_EOffice_DocumentType] ORDER BY DocumentTypeName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            eOffice entitymodel = new eOffice();

                            entitymodel.DocumentType = Convert.ToString(reader["DocumentTypeName"]);
                            entitymodel.PKID_DocumentType = Convert.ToInt16(reader["PKID_DocumentType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }

            return lstentity;

        }
        public IList<eOfficeYears> GetYear()
        {
            IList<eOfficeYears> lstentity = new List<eOfficeYears>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[sp_GetYearList]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            eOfficeYears entitymodel = new eOfficeYears();

                            entitymodel.Year = Convert.ToString(reader["YearNumber"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }

            return lstentity;

        }

        public int eOfficeTaskDetails(eOfficeTaskDetailsModel cd)
        {
            try
            {
                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
                {
               
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_AddTaskDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UniqueCode", Convert.ToString(cd.UniqueCode));
                        cmd.Parameters.AddWithValue("@Subject", Convert.ToString(cd.Subject));
               
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(cd.CreatedBy));
                  
                        //cmd.Parameters.AddWithValue("@TaskDate", Convert.ToDateTime(cd.TaskDate));
                        cmd.Parameters.Add("@TaskId", SqlDbType.Int);
                        cmd.Parameters["@TaskId"].Direction = ParameterDirection.Output;

                        conn.Open();
                        cmd.ExecuteNonQuery();

                        result = Convert.ToInt16(cmd.Parameters["@TaskId"].Value);
                        conn.Close();
                        return result;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<eOfficeTaskDetailsModel> GetTaskDetails()
        {
            List<eOfficeTaskDetailsModel> lstentity = new List<eOfficeTaskDetailsModel> ();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_GetTaskDetails";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            eOfficeTaskDetailsModel entitymodel = new eOfficeTaskDetailsModel();
                            entitymodel.TaskId = Convert.ToInt16(reader["TaskId"]);
                            entitymodel.UniqueCode = Convert.ToString(reader["UniqueCode"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.CreatedOn = Convert.ToDateTime(reader["TaskDate"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }

            return lstentity;

        }


        public int eOfficeGRDetails(GRDetails cd)
        {
            try
            {
                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_StudentProfile()))
                {

                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_Add_E_Office_GR_Details";
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@TaskId", Convert.ToInt16(cd.TaskId));
                        cmd.Parameters.AddWithValue("@Subject", Convert.ToString(cd.Subject));
                        cmd.Parameters.AddWithValue("@Year", Convert.ToString(cd.year));
                        cmd.Parameters.AddWithValue("@DocumentType", Convert.ToString(cd.DocumentType));
                        cmd.Parameters.AddWithValue("@UniqueCode", Convert.ToString(cd.UniqueCode));
                        cmd.Parameters.AddWithValue("@Desk", Convert.ToString(cd.Desk));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(cd.CreatedBy));
                        cmd.Parameters.AddWithValue("@Document", Convert.ToString(cd.Document));
                        if (cd.CreatedOn == DateTime.Parse("1/1/0001 12:00:00 AM"))
                        {
                            DateTime? dt = null;
                            cmd.Parameters.AddWithValue("@CreatedOn", dt);
                        }
                        else
           
                        cmd.Parameters.AddWithValue("@CreatedOn", Convert.ToDateTime(cd.CreatedOn));
                        cmd.Parameters.AddWithValue("@DocumentDate", Convert.ToDateTime(cd.DocumentDate));
                        cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(cd.IsActive));



                        conn.Open();
                        cmd.ExecuteNonQuery();

                       
                        conn.Close();
                        return result;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<GRDetails> GetGRDetails(GRDetails gr)
        {
            List<GRDetails> lstentity = new List<GRDetails>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_getGrDetails";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Year", gr.year);
                    cmd.Parameters.AddWithValue("@DcumentType", gr.DocumentType);
                    cmd.Parameters.AddWithValue("@DocumentDate", gr.DocumentDate);
                    cmd.Parameters.AddWithValue("@Subject", gr.Subject);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GRDetails entitymodel = new GRDetails();
                            entitymodel.GRId = Convert.ToInt16(reader["GRId"]);
                            entitymodel.UniqueCode = Convert.ToString(reader["UniqueCode"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.DocumentDate = Convert.ToDateTime(reader["DocumentDate"]);
                            entitymodel.DocumentType = Convert.ToString(reader["DocumentType"]);
                            entitymodel.year = Convert.ToString(reader["Year"]);
                            entitymodel.Desk = Convert.ToString(reader["Desk"]);
                            entitymodel.Document = Convert.ToString(reader["Document"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }

            return lstentity;

        }

        public int UpdateGRDetails(GRDetails gr)
        {
            int res = 0;
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_UpdateGRDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Year", gr.year);
                        cmd.Parameters.AddWithValue("@UniqueCode", gr.UniqueCode);
                        cmd.Parameters.AddWithValue("@DocumentType", gr.DocumentType);
                        cmd.Parameters.AddWithValue("@DocumentDate", gr.DocumentDate);
                        cmd.Parameters.AddWithValue("@Subject", gr.Subject);
                        cmd.Parameters.AddWithValue("@Desk", gr.Desk);
                        cmd.Parameters.AddWithValue("@Document", gr.Document);
                        cmd.ExecuteNonQuery();
                        res = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }


        public IList<GRDetails> GetEOfficeTaskDetails(GRDetails gr)
        {
            List<GRDetails> lstentity = new List<GRDetails>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "sp_View_E_Office_Details";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@WFInstanceID", gr.WorkFlowInstanceId);
                   
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GRDetails entitymodel = new GRDetails();
                            entitymodel.RequestId = Convert.ToInt16(reader["RequestID"]);
                            entitymodel.ProcessId = Convert.ToInt16(reader["ProcessId"]);
                            entitymodel.StateId = Convert.ToInt16(reader["ProcessId"]);
                            entitymodel.UniqueCode = Convert.ToString(reader["UniqueCode"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.DocumentDate = Convert.ToDateTime(reader["DocumentDate"]);
                            entitymodel.DocumentType = Convert.ToString(reader["DocumentType"]);
                            entitymodel.year = Convert.ToString(reader["Year"]);
                            entitymodel.Desk = Convert.ToString(reader["Desk"]);
                            entitymodel.Document = Convert.ToString(reader["Document"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }

            return lstentity;

        }
    }
}
