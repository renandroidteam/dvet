﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Master;

namespace DataAccess.Master
{
  public  class WorkflowDB
    {
        public void WorkflowIntiation(WorkflowModel objViewmodel)
        {
            try
            {

                int result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("WorkflowGeneralInitiation", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToInt16(objViewmodel.RequestId));
                        cmd.Parameters.AddWithValue("@RequestNo", Convert.ToString(objViewmodel.RequestNo));
                        cmd.Parameters.AddWithValue("@WorkflowCode", Convert.ToString(objViewmodel.WorkflowCode));
                        cmd.Parameters.AddWithValue("@WorkflowDCode", Convert.ToString(objViewmodel.WorkflowDCode));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(objViewmodel.CreatedBy));
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WorkflowModel> GetWorkflowInwardtasklist(string assignedto, string WfStatus)
        {
            try
            {
                List<WorkflowModel> obj = new List<WorkflowModel>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[GetWorkflowInwardtasklist]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AssignedTo", assignedto);
                        cmd.Parameters.AddWithValue("@StatusCode", WfStatus);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                WorkflowModel lstmodel = new WorkflowModel();
                                lstmodel.WFInstanceID = Convert.ToInt16(reader["WFInstanceID"]);
                                lstmodel.RequestId = Convert.ToInt16(reader["RequestID"]);
                                lstmodel.RequestNo = Convert.ToString(reader["CaseNumber"]);
                              
                                lstmodel.LetterType = Convert.ToString(reader["LetterType"]);
                               
                                lstmodel.LetterSubject = Convert.ToString(reader["Subject"]);
                                lstmodel.WorkflowCode = Convert.ToString(reader["WFCode"]);
                                lstmodel.WorkflowDCode = Convert.ToString(reader["SubWFCode"]);
                                lstmodel.Status = Convert.ToString(reader["WorkflowStatus"]);
                                lstmodel.DeskCreatedDate = Convert.ToString(reader["CreatedDate"]);
                              
                                lstmodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                                lstmodel.CreatedBy = Convert.ToString(reader["CreatedBY"]);
                                obj.Add(lstmodel);
                            }
                            return obj;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool WorkflowProces(WorkflowProcessModel objViewmodel)
        {
            try
            {

               // bool result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("WorkflowGeneralInitiation", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@WorkflowInstanceId", Convert.ToInt16(objViewmodel.WorkflowInstanceId));
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToInt16(objViewmodel.CaseId));
                       cmd.Parameters.AddWithValue("@RequestNo", Convert.ToString(objViewmodel.CaseNumber));
                       cmd.Parameters.AddWithValue("@CreatedBY", Convert.ToString(objViewmodel.CreatedBy));
                        cmd.Parameters.AddWithValue("@WorkflowDCode", Convert.ToString(objViewmodel.WFDCode));
                        //cmd.Parameters.AddWithValue("@ProcessID", Convert.ToInt16(objViewmodel.ProcessId));
                        cmd.Parameters.AddWithValue("@WorkflowCode", Convert.ToString(objViewmodel.WFCode));
                        //cmd.Parameters.AddWithValue("@newStateID", Convert.ToInt16(objViewmodel.StateId));
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                       
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool WorkflowProcess(WorkflowProcessModel objViewmodel)
        {
            try
            {

                // bool result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("WorkflowGeneralInitiation1", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WorkflowInstanceId", Convert.ToInt16(objViewmodel.WorkflowInstanceId));
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToInt16(objViewmodel.CaseId));
                        // cmd.Parameters.AddWithValue("@Comments", Convert.ToString(objViewmodel.Comments));
                        cmd.Parameters.AddWithValue("@CreatedBY", Convert.ToString(objViewmodel.CreatedBy));
                        cmd.Parameters.AddWithValue("@ActionCode", Convert.ToString(objViewmodel.ActionCode));
                        cmd.Parameters.AddWithValue("@ProcessID", Convert.ToInt16(objViewmodel.ProcessId));
                        cmd.Parameters.AddWithValue("@WorkflowCode", Convert.ToString(objViewmodel.WFCode));
                        cmd.Parameters.AddWithValue("@StateID", Convert.ToInt16(objViewmodel.StateId));
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public WorkflowProcessModel GetAssignedDatabycasenumber(String CaseNumber)
        {
            try
            {
                WorkflowProcessModel lstmodel = new WorkflowProcessModel();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "sp_GetWorkFlowdetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RequestNumber", CaseNumber);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //WorkflowProcessModel lstmodel = new WorkflowProcessModel();
                                lstmodel.WorkflowInstanceId = Convert.ToInt32(reader["WFInstanceID"]);
                                lstmodel.RequestID = Convert.ToInt32(reader["RequestID"]);
                               // lstmodel.WFCode = Convert.ToString(reader["WFCode"]);
                               // lstmodel.CaseNumber = Convert.ToString(reader["CaseNumber"]);
                                lstmodel.ActionId = Convert.ToInt32(reader["ActionID"]);
                                lstmodel.ProcessId = Convert.ToInt32(reader["ProcessID"]);
                                lstmodel.StateId = Convert.ToInt32(reader["StateID"]);
                                
                            }
                       
                        }

                    }
                }
                return lstmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public List<WorkflowModel> GetWorkflowApprovedInwardtasklist(string assignedto, string WfStatus)
        {
            try
            {
                List<WorkflowModel> obj = new List<WorkflowModel>();
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[GetApprovedWorkflowInwardtasklist]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AssignedTo", assignedto);
                        cmd.Parameters.AddWithValue("@StatusCode", WfStatus);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                WorkflowModel lstmodel = new WorkflowModel();
                                lstmodel.WFInstanceID = Convert.ToInt16(reader["WFInstanceID"]);
                                lstmodel.RequestId = Convert.ToInt16(reader["RequestID"]);
                                lstmodel.RequestNo = Convert.ToString(reader["CaseNumber"]);

                                lstmodel.LetterType = Convert.ToString(reader["LetterType"]);

                                lstmodel.LetterSubject = Convert.ToString(reader["Subject"]);
                                lstmodel.WorkflowCode = Convert.ToString(reader["WFCode"]);
                                lstmodel.WorkflowDCode = Convert.ToString(reader["SubWFCode"]);
                                lstmodel.Status = Convert.ToString(reader["WorkflowStatus"]);
                                lstmodel.DeskCreatedDate = Convert.ToString(reader["CreatedDate"]);

                                lstmodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                                lstmodel.CreatedBy = Convert.ToString(reader["CreatedBY"]);
                                obj.Add(lstmodel);
                            }
                            return obj;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool WorkflowProcessStatus(WorkflowProcessModel objViewmodel)
        {
            try
            {

                // bool result = 0;
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    using (SqlCommand cmd = new SqlCommand("", conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WorkflowInstanceId", Convert.ToInt16(objViewmodel.WorkflowInstanceId));
                        cmd.Parameters.AddWithValue("@RequestID", Convert.ToInt16(objViewmodel.CaseId));
                        // cmd.Parameters.AddWithValue("@Comments", Convert.ToString(objViewmodel.Comments));
                        cmd.Parameters.AddWithValue("@CreatedBY", Convert.ToString(objViewmodel.CreatedBy));
                        cmd.Parameters.AddWithValue("@ActionCode", Convert.ToString(objViewmodel.ActionCode));
                        cmd.Parameters.AddWithValue("@ProcessID", Convert.ToInt16(objViewmodel.ProcessId));
                        cmd.Parameters.AddWithValue("@WorkflowCode", Convert.ToString(objViewmodel.WFCode));
                        cmd.Parameters.AddWithValue("@StateID", Convert.ToInt16(objViewmodel.StateId));
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
