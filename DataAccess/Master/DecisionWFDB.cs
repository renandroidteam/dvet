﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
   public class DecisionWFDB
    {
        public bool WFProcess(DecisionWF process)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[proc_WFProcess]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WorkflowInstanceId", process.WorkflowInstanceId);
                        cmd.Parameters.AddWithValue("@RequestID", process.RequestID);

                        cmd.Parameters.AddWithValue("@Comments", process.Comments);
                        cmd.Parameters.AddWithValue("@CreatedBY", process.CreatedBY);

                        cmd.Parameters.AddWithValue("@ActionCode", process.ActionCode);
                        cmd.Parameters.AddWithValue("@ProcessID", process.ProcessID);
                        cmd.Parameters.AddWithValue("@WFCode", process.WFCode);

                        cmd.Parameters.AddWithValue("@StateID", process.StateID);
                       
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;

        }

        public bool WFIntiation(DecisionCaseModel intiation)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[WorkflowGeneralInitiation]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        cmd.Parameters.AddWithValue("@RequestID", intiation.PKID_Case);

                        cmd.Parameters.AddWithValue("@RequestNo", intiation.CaseId);
                        cmd.Parameters.AddWithValue("@WorkflowCode", "SAWF");
                        cmd.Parameters.AddWithValue("@WorkflowDCode", "SG");
                        cmd.Parameters.AddWithValue("@CreatedBY", intiation.CreatedBy);
                        cmd.ExecuteNonQuery();
                        conn.Close();

                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;

        }


        public IList<DecisionWF> WFTasklist(DecisionWF intiation)
        {
            List<DecisionWF> lstCases = new List<DecisionWF>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[GetWorkflowtasklist]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@WFID", 4);
                    cmd.Parameters.AddWithValue("@AssignedTo", "AnandP@dvetedu.onmicrosoft.com");
                    cmd.Parameters.AddWithValue("@StatusCode", "PEND");

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            DecisionWF entitymodel = new DecisionWF();
                          
                            entitymodel.CreatedBY = Convert.ToString(reader["CreatedBY"]);
                            entitymodel.AssignedTo = Convert.ToString(reader["AssignedTo"]);
                            entitymodel.RequestNo = Convert.ToString(reader["Subject"]);
                            entitymodel.WorkflowInstanceId = Convert.ToInt16(reader["WFInstanceID"]);
                            entitymodel.StatusCode = Convert.ToString(reader["WorkflowStatus"]);
                            

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }



    }
}
