﻿using DataModel.Master;
using DVETAdministrationWebAPI.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using DVETAdministrationWebAPI;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Text;

namespace DataAccess.Master
{
    public class MasterDB
    {

        public IList<Master_Region> GetRegion()
        {
            IList<Master_Region> lstentity = new List<Master_Region>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_Region] ORDER BY RegionName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Region entitymodel = new Master_Region();

                            entitymodel.PKID_Region = Convert.ToInt16(reader["PKID_Region"]);
                            entitymodel.RegionName = Convert.ToString(reader["RegionName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_Employee> GetEmployeeByEmail(string Emailid)
        {
            List<Master_Employee> lstEmployees = new List<Master_Employee>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Employee_GetEmployeeByEmail]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmailId", Emailid);
                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Employee entitymodel = new Master_Employee();
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                            entitymodel.PKID_Employee = Convert.ToInt16(reader["PKID_Employee"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.DOB = Convert.ToString(reader["DOB"]);
                            entitymodel.Mobilenum = Convert.ToString(reader["MobileNum"]);
                            entitymodel.Decision = Convert.ToString(reader["Decision"]);
                            entitymodel.Posting = Convert.ToString(reader["PostingLocation"]);
                            entitymodel.Position = Convert.ToString(reader["Position"]);
                           


                            lstEmployees.Add(entitymodel);
                        }
                    }
                    conn.Close();
                }
                return lstEmployees;
            }
        }

        public IList<Master_CourtType> GeCourtType()
        {
            IList<Master_CourtType> lstentity = new List<Master_CourtType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_CourtType] ORDER BY CourtType";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_CourtType entitymodel = new Master_CourtType();

                            entitymodel.PKID_CourtType = Convert.ToInt16(reader["PKID_CourtType"]);
                            entitymodel.CourtType = Convert.ToString(reader["CourtType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_AidType> GetAidType()
        {
            IList<Master_AidType> lstentity = new List<Master_AidType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_AidType] ORDER BY AidType";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_AidType entitymodel = new Master_AidType();

                            entitymodel.PKID_AidType = Convert.ToInt16(reader["PKID_AidType"]);
                            entitymodel.AidType = Convert.ToString(reader["AidType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_CaseType> GetCaseType()
        {
            IList<Master_CaseType> lstentity = new List<Master_CaseType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_CaseType]";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_CaseType entitymodel = new Master_CaseType();

                            entitymodel.PKID_CaseType = Convert.ToInt16(reader["PKID_CaseType"]);
                            entitymodel.CaseType = Convert.ToString(reader["CaseType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_CaseClassification> GetCaseClassification()
        {
            IList<Master_CaseClassification> lstentity = new List<Master_CaseClassification>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_CaseClassification] ORDER BY CaseClassificationName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_CaseClassification entitymodel = new Master_CaseClassification();

                            entitymodel.PKID_CaseClassification = Convert.ToInt16(reader["PKID_CaseClassification"]);
                            entitymodel.CaseClassificationName = Convert.ToString(reader["CaseClassificationName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_LegalStatus> GetLegalStatus()
        {
            IList<Master_LegalStatus> lstentity = new List<Master_LegalStatus>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_LegalStatus] ORDER BY LegalStatusName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_LegalStatus entitymodel = new Master_LegalStatus();

                            entitymodel.PKID_LegalStatus = Convert.ToInt16(reader["PKID_LegalStatus"]);
                            entitymodel.LegalStatusName = Convert.ToString(reader["LegalStatusName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_ParticipantType> GetParticipantType()
        {
            IList<Master_ParticipantType> lstentity = new List<Master_ParticipantType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_ParticipantType] ORDER BY ParticipantType";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_ParticipantType entitymodel = new Master_ParticipantType();

                            entitymodel.PKID_ParticipantType = Convert.ToInt16(reader["PKID_ParticipantType"]);
                            entitymodel.ParticipantType = Convert.ToString(reader["ParticipantType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_Department> GetDepartments()
        {
            IList<Master_Department> lstentity = new List<Master_Department>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_Department] ORDER BY DepartmentName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Department entitymodel = new Master_Department();
                            entitymodel.PKID_Department = Convert.ToInt16(reader["PKID_Department"]);
                            entitymodel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_Institute> GetInstitutes()
        {
            IList<Master_Institute> lstentity = new List<Master_Institute>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_InstituteName] ORDER BY InstituteName";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Institute entitymodel = new Master_Institute();
                            entitymodel.PKID_InstituteName = Convert.ToInt16(reader["PKID_InstituteName"]);
                            entitymodel.InstituteName = Convert.ToString(reader["InstituteName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_InstituteType> GetInstitutesTypes()
        {
            IList<Master_InstituteType> lstentity = new List<Master_InstituteType>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_InstituteType] ORDER BY InstituteType";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_InstituteType entitymodel = new Master_InstituteType();
                            entitymodel.PKID_InstituteType = Convert.ToInt16(reader["PKID_InstituteType"]);
                            entitymodel.InstituteType = Convert.ToString(reader["InstituteType"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        public IList<Master_Citizen_UserRoles> GetCitizen_UserRoles()
        {
            IList<Master_Citizen_UserRoles> lstentity = new List<Master_Citizen_UserRoles>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Citizen_UserRole] ORDER BY Rolename";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Citizen_UserRoles entitymodel = new Master_Citizen_UserRoles();
                            entitymodel.PKID_Role = Convert.ToInt16(reader["PKID_Role"]);
                            entitymodel.RoleName = Convert.ToString(reader["RoleName"]);
                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

        /// <summary>
        /// Method for sending OTP MSG.
        /// </summary>
        /// <param name="username"> Registered user name
        /// <param name="password"> Valid login password
        /// <param name="senderid">Sender ID
        /// <param name="mobileNo"> valid single  Mobile Number
        /// <param name="message">Message Content
        /// <param name="secureKey">Department generate key by login to services portal

        // Method for sending OTP MSG.
        public string SendMobileOTPMessage(string mobileNo, string msgBody)
        {

            try
            {
                var username = ConfigurationManager.AppSettings["SMS-Username"];
                var password = ConfigurationManager.AppSettings["SMS-Password"];
                var senderId = ConfigurationManager.AppSettings["SMS-SenderID"];
                var secureKey = ConfigurationManager.AppSettings["SMS-SecureKey"];
                string _smsApiUri = ConfigurationManager.AppSettings["SMS-API-URI"];
                msgBody = msgBody.Replace('\n', ' ').Replace('\r', ' ').Replace("  ", " ");
                msgBody = msgBody.Trim(' ');
                //var output = SendSingleSms(username, password, senderId, string.Join(",", mobileNo), msgBody, secureKey);

                return null;

            }
            catch (Exception ex)
            {
                //Logger.Error("SMS Bulk", ex);
                return ex.Message;
                //throw ex;
            }

        }
               
        public IList<Master_Employee> GetEmployees()
        {
            IList<Master_Employee> lstentity = new List<Master_Employee>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_CommonMaster()))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM [dbo].[tbl_Master_Employee] ORDER BY EmployeeId";
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Master_Employee entitymodel = new Master_Employee();
                            entitymodel.FirstName = Convert.ToString(reader["FirstName"]);
                            entitymodel.LastName = Convert.ToString(reader["LastName"]);
                            entitymodel.EmployeeId = Convert.ToString(reader["EmployeeId"]);
                            entitymodel.PKID_Employee = Convert.ToInt16(reader["PKID_Employee"]);
                            entitymodel.EmailId = Convert.ToString(reader["EmailId"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);

                            entitymodel.Age = Convert.ToInt16(reader["Age"]);
                            entitymodel.DepartmentId = Convert.ToInt16(reader["DepartmentId"]);
                            entitymodel.Gender = Convert.ToString(reader["Gender"]);

                            lstentity.Add(entitymodel);
                        }
                    }
                }
            }
            return lstentity;
        }

    }
}
