﻿using DataModel.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Master
{
    public class DecisionDB
    {
        public List<DecisionCaseModel> DashBoard()
        {
            List<DecisionCaseModel> lstCases = new List<DecisionCaseModel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetDashboard]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DecisionCaseModel entitymodel = new DecisionCaseModel();
                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.FKID_StatusId = Convert.ToInt16(reader["FKID_StatusId"]);
                            //   entitymodel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            entitymodel.Description = Convert.ToString(reader["Description"]);
                            entitymodel.AddDate = Convert.ToDateTime(reader["AddDate"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseId"]);
                            entitymodel.Status = Convert.ToInt16(reader["Status"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);


                            lstCases.Add(entitymodel);
                        }
                    }
                    conn.Close();
                };
                return lstCases;
            }
        }

        public IList<DecisionCaseModel> GetCaseHistory(int fKID_Case)
        {
            return null;
            //sp_Decision_GetCaseHistory what else data need to be join there? decision status is not there in caseflow
        }

        public bool InsertCaseFlow(Decision_CaseFlowModel caseflow)
        {
            try
            {
                using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_InsertCaseFlow]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@CasePKID", caseflow.FKID_Case);
                        cmd.Parameters.AddWithValue("@ForwardTo", caseflow.ForwardTo);
                        cmd.Parameters.AddWithValue("@CaseID", caseflow.CaseID);
                        //cmd.Parameters.AddWithValue("@FKID_StatusId", caseflow.FKID_StatusId);
                        cmd.Parameters.AddWithValue("@ApprovalRequired", caseflow.ApprovalRequired);
                        cmd.Parameters.AddWithValue("@Name", caseflow.Name);

                        cmd.Parameters.AddWithValue("@Designation", caseflow.Designation);
                        cmd.Parameters.AddWithValue("@Email", caseflow.EmailAddress);
                        cmd.Parameters.AddWithValue("@Department", caseflow.Department);
                        
                        cmd.Parameters.AddWithValue("@SendBackRemarks", caseflow.SendBackRemarks);
                        cmd.Parameters.AddWithValue("@HoldRemarks", caseflow.HoldRemarks);
                        //cmd.Parameters.AddWithValue("@CreatedBy", caseflow.CreatedBy);
                   
                        cmd.ExecuteNonQuery();

                        conn.Close();

                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;

        }
        public bool InsertCaseDetails(Decision_CaseDetails casedetails)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_InsertCaseDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseID", casedetails.CaseId);
                    cmd.Parameters.AddWithValue("@CourtDecision", casedetails.CourtDecision);
                    cmd.Parameters.AddWithValue("@PreviousDecisions", casedetails.PreviousDecisions);
                    cmd.Parameters.AddWithValue("@DepartmentSuggestion", casedetails.DepartmentSuggestion);
                    cmd.Parameters.AddWithValue("@DecisionImplication", casedetails.DecisionImplication);
                    cmd.Parameters.AddWithValue("@GRNumbers", casedetails.GRNumbers);
                    cmd.Parameters.AddWithValue("@WebsiteName", casedetails.WebsiteName);
                    cmd.Parameters.AddWithValue("@WebsiteURL", casedetails.WebsiteURL);
             


                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                };
            }
        }

        public string InsertCase(DecisionCaseModel cased)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_SetCaseData]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter NewCaseId = new SqlParameter("@NewCaseId", SqlDbType.NVarChar, 50);
                    NewCaseId.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(NewCaseId);

                    cmd.Parameters.AddWithValue("@Type", 1);
                    //    cmd.Parameters.AddWithValue("@CaseID", cased.CaseId);

                    cmd.Parameters.AddWithValue("@Subject", cased.Subject);
                    cmd.Parameters.AddWithValue("@AddDate", cased.AddDate);
                    //cmd.Parameters.AddWithValue("@AddDate", cased.CreatedBy);
                    cmd.Parameters.AddWithValue("@Description", cased.Description);
                    cmd.Parameters.AddWithValue("@Status", cased.Status);
                    cmd.Parameters.AddWithValue("@ApprovalRequired", cased.ApprovalRequired);
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return Convert.ToString(NewCaseId.Value);
                };
            }
        }
        public IList<DecisionCaseModel> SearchCase(DecisionCaseModel searchcase)
        {
            List<DecisionCaseModel> lstCases = new List<DecisionCaseModel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_SearchCase]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Status", searchcase.Status);
                    cmd.Parameters.AddWithValue("@CaseID", searchcase.CaseId);
                    cmd.Parameters.AddWithValue("@Subject", searchcase.Subject);
                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            DecisionCaseModel entitymodel = new DecisionCaseModel();
                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            //  entitymodel.DepartmentID = Convert.ToInt16(reader["Department"]);
                            //  entitymodel.DepartmentName = Convert.ToString(reader["DepartmentName"]);
                            entitymodel.Description = Convert.ToString(reader["Description"]);
                            entitymodel.AddDate = Convert.ToDateTime(reader["AddDate"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseId"]);
                            entitymodel.Status = Convert.ToInt16(reader["Status"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }
        public IList<DecisionCaseModel> GetCase(DecisionCaseModel searchcase)
        {
            List<DecisionCaseModel> lstCases = new List<DecisionCaseModel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", 1);
                    cmd.Parameters.AddWithValue("@CaseID", searchcase.CaseId);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            DecisionCaseModel entitymodel = new DecisionCaseModel();
                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.Description = Convert.ToString(reader["Description"]);
                            entitymodel.AddDate = Convert.ToDateTime(reader["AddDate"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseId"]);
                            entitymodel.Status = Convert.ToInt16(reader["Status"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }
        public IList<DecisionCaseModel> GetCaseID()
        {
            List<DecisionCaseModel> lstCases = new List<DecisionCaseModel>();
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Type", 2);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            DecisionCaseModel entitymodel = new DecisionCaseModel();
                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.Description = Convert.ToString(reader["Description"]);
                            entitymodel.AddDate = Convert.ToDateTime(reader["AddDate"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseId"]);
                            entitymodel.Status = Convert.ToInt16(reader["Status"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }
        public IList<Decision_CaseDetails> GetCaseDetails(int CaseId)
        {

            List<Decision_CaseDetails> lstCases = new List<Decision_CaseDetails>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetCaseDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@FK_CaseID", CaseId);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            Decision_CaseDetails entitymodel = new Decision_CaseDetails();

                            entitymodel.CaseId = Convert.ToInt16(reader["FKID_Case"]);
                            entitymodel.CourtDecision = Convert.ToString(reader["CourtDecision"]);
                            entitymodel.PreviousDecisions = Convert.ToString(reader["PreviousDecisions"]);
                            entitymodel.DecisionImplication = Convert.ToString(reader["DecisionImplication"]);
                            entitymodel.DepartmentSuggestion = Convert.ToString(reader["DepartmentSuggestion"]);
                            entitymodel.GRNumbers = Convert.ToString(reader["GRNumbers"]);
                            entitymodel.WebsiteName = Convert.ToString(reader["WebsiteName"]);
                            entitymodel.WebsiteURL = Convert.ToString(reader["WebsiteURL"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }

        public IList<Decision_CaseFlowModel> GetCaseFlow(string CaseId)
        {

            List<Decision_CaseFlowModel> lstCases = new List<Decision_CaseFlowModel>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetCaseFlow]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Type", 1);

                    cmd.Parameters.AddWithValue("@CaseID", CaseId);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            Decision_CaseFlowModel entitymodel = new Decision_CaseFlowModel();

                            entitymodel.FKID_Case = Convert.ToInt16(reader["FKID_Case"]);
                            entitymodel.CaseID = Convert.ToString(reader["ForwardTo"]);
                            entitymodel.ForwardTo = Convert.ToString(reader["ForwardTo"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                            entitymodel.SendBackRemarks = Convert.ToString(reader["SendBackRemarks"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);
                            entitymodel.HoldRemarks = Convert.ToString(reader["HoldRemarks"]);
                            entitymodel.PKID_CaseFlow = Convert.ToInt16(reader["PKID_CaseFlow"]);
                            entitymodel.CaseType = Convert.ToString(reader["CaseType"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }
        public IList<Decision_CaseFlowModel> GetAllFlow()
        {

            List<Decision_CaseFlowModel> lstCases = new List<Decision_CaseFlowModel>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetCaseFlow]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Type", 2);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            Decision_CaseFlowModel entitymodel = new Decision_CaseFlowModel();

                            entitymodel.FKID_Case = Convert.ToInt16(reader["FKID_Case"]);
                            entitymodel.CaseID = Convert.ToString(reader["ForwardTo"]);
                            entitymodel.ForwardTo = Convert.ToString(reader["ForwardTo"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                            entitymodel.SendBackRemarks = Convert.ToString(reader["SendBackRemarks"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);
                            entitymodel.HoldRemarks = Convert.ToString(reader["HoldRemarks"]);
                            entitymodel.PKID_CaseFlow = Convert.ToInt16(reader["PKID_CaseFlow"]);
                            entitymodel.CaseType = Convert.ToString(reader["CaseType"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }

        public IList<DecisionCaseModel> GetFlow(string EmailId)
        {

            List<DecisionCaseModel> lstCases = new List<DecisionCaseModel>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetFlow]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@EmailId", EmailId);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            DecisionCaseModel entitymodel = new DecisionCaseModel();

                            entitymodel.PKID_Case = Convert.ToInt16(reader["PKID_Case"]);
                            entitymodel.FKID_StatusId = Convert.ToInt16(reader["FKID_StatusId"]);
                            entitymodel.Description = Convert.ToString(reader["Description"]);
                            entitymodel.AddDate = Convert.ToDateTime(reader["AddDate"]);
                            entitymodel.ApprovalRequired = Convert.ToString(reader["ApprovalRequired"]);
                            entitymodel.CaseId = Convert.ToString(reader["CaseId"]);
                            entitymodel.Status = Convert.ToInt16(reader["Status"]);
                            entitymodel.Subject = Convert.ToString(reader["Subject"]);
                            entitymodel.CreatedBy = Convert.ToString(reader["CreatedBy"]);

                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }


        public IList<CaseOfficerDetails> GetofficerDetails()
        {

            List<CaseOfficerDetails> lstCases = new List<CaseOfficerDetails>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetForwardDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Type", 1);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            CaseOfficerDetails entitymodel = new CaseOfficerDetails();


                            entitymodel.Name = Convert.ToString(reader["Name"]);
                            entitymodel.Email = Convert.ToString(reader["Email"]);
                            entitymodel.Designation = Convert.ToString(reader["Designation"]);
                            entitymodel.Department = Convert.ToString(reader["Department"]);


                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }

        public IList<CaseOfficerDetails> GetInstituteDetails()
        {

            List<CaseOfficerDetails> lstCases = new List<CaseOfficerDetails>();

            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_GetForwardDetails]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Type", 2);

                    // cmd.Parameters.AddWithValue("@Date", searchcase.AddDate);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            CaseOfficerDetails entitymodel = new CaseOfficerDetails();
                            entitymodel.InstituteName = Convert.ToString(reader["Institute"]);
                            entitymodel.InstituteEmail = Convert.ToString(reader["InstituteEmail"]);
                            lstCases.Add(entitymodel);

                        }
                    }

                };
                conn.Close();
                return lstCases;

            }
        }


        public bool UpdateAssignedCase(Decision_CaseFlowModel caseflow)
        {
            using (var conn = new SqlConnection(SqlHelpher.SqlConn_Administration()))
            {

                using (SqlCommand cmd = new SqlCommand("[dbo].[sp_Decision_UpdateCaseFlow]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CaseID", caseflow.FKID_Case);
                    cmd.Parameters.AddWithValue("@ForwardTo", caseflow.ForwardTo);

                    cmd.Parameters.AddWithValue("@ApprovalRequired", caseflow.ApprovalRequired);
                    cmd.Parameters.AddWithValue("@Name", caseflow.Name);

                    cmd.Parameters.AddWithValue("@Designation", caseflow.Designation);
                    cmd.Parameters.AddWithValue("@Email", caseflow.EmailAddress);
                    cmd.Parameters.AddWithValue("@Department", caseflow.Department);

                    cmd.Parameters.AddWithValue("@SendBackRemarks", caseflow.SendBackRemarks);
                    cmd.Parameters.AddWithValue("@HoldRemarks", caseflow.HoldRemarks);
                    cmd.Parameters.AddWithValue("@CaseID", caseflow.CaseID);
                    cmd.Parameters.AddWithValue("@FKID_StatusId", caseflow.FKID_StatusId);
                    cmd.Parameters.AddWithValue("@CreatedBy", caseflow.CreatedBy);
                    cmd.Parameters.AddWithValue("@ModifiedBy", caseflow.ModifiedBy);

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                };
            }
        }
    }
}
