﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DataAccess
{
    public static class SqlHelpher
    {
       
        public static string SqlConn_Administration()
        {
            return (ConfigurationManager.ConnectionStrings["Administration"].ConnectionString);
        }

        public static string SqlConn_StudentProfile()
        {
            return (ConfigurationManager.ConnectionStrings["StudentProfile"].ConnectionString);
        }

        public static string SqlConn_StudentAcademics()
        {
            return (ConfigurationManager.ConnectionStrings["StudentAcademics"].ConnectionString);
        }

        public static string SqlConn_CommonMaster()
        {
            return (ConfigurationManager.ConnectionStrings["CommonMaster"].ConnectionString);
        }

        public static string AppSettings()
        {
            return (ConfigurationManager.ConnectionStrings["SMS-Username"].ConnectionString);
        }
    }
}
